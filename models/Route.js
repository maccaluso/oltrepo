var keystone = require('keystone');
var utils = keystone.utils;
var Types = keystone.Field.Types;
var deepPopulate = require('mongoose-deep-populate')( keystone.mongoose );
var shortid = require('shortid');

/**
 * Route Model
 * =============
 */

var Route = new keystone.List('Route', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Route.schema.plugin(deepPopulate);

Route.schema.pre('save', function(next) {
    this.slugEN = utils.slug( this.nameEN );
    this.appCode = shortid.generate();
    next();
});

// Route.schema.statics.getKey = function(){
// 	return this.key;
// }

Route.add(
	{ heading: 'Info' },
	{
		name: { type: Types.Text, required: true },
		nameEN: { type: Types.Text },
		slugEN: { type: Types.Text, noedit: true },
		appCode: { type: Types.Text, noedit: true },
		description: {
			it: { type: Types.Textarea },
			en: { type: Types.Textarea }
		},

		publishedDate: { type: Date, default: Date.now },

		coverImg: {
			src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload' },
			caption: {
				it: { type: Types.Text },
				en: { type: Types.Text }
			}
		},

		isGPS: { type: Types.Boolean },
		isFeatured: { type: Types.Boolean },
		featuredOrder: { type: Types.Number }
	},

	{ heading: 'Relationships' },
	{
		arguments: { type: Types.Relationship, ref: 'Argument', many: true }
	}
);

Route.register();
