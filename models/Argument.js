var keystone = require('keystone');
var utils = keystone.utils;
var Types = keystone.Field.Types;
var deepPopulate = require('mongoose-deep-populate')( keystone.mongoose );

/**
 * Argument Model
 * =============
 */

var Argument = new keystone.List('Argument', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Argument.schema.plugin(deepPopulate);

Argument.schema.pre('save', function(next) {
    this.slugEN = utils.slug(this.nameEN);
    next();
});

Argument.add(
	{ heading: 'Info' },
	{
		name: { type: Types.Text, required: true },
		nameEN: { type: Types.Text },
		slugEN: { type: Types.Text, noedit: true },
		shortDescription: {
			it: { type: Types.Textarea },
			en: { type: Types.Textarea }
		},
		longDescription: {
			it: { type: Types.Textarea },
			en: { type: Types.Textarea }
		},
		location: { type: Types.Relationship, ref: 'GeoPosition' },
		routingStartDefault: { type: Types.Relationship, ref: 'GeoPosition', many: true },

		publishedDate: { type: Date, default: Date.now },

		coverImg: {
			src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload' },
			caption: {
				it: { type: Types.Text },
				en: { type: Types.Text }
			}
		},
		useVideoCover: { type: Types.Boolean },
		videoCover: {
			host: { type: Types.Select, options: 'youtube, vimeo', dependsOn: { useVideoCover: true } },
			id: { type: Types.Text, dependsOn: { useVideoCover: true } }
		},

		isGPS: { type: Types.Boolean },
		isFeatured: { type: Types.Boolean },
		featuredOrder: { type: Types.Number }
	},

	{ heading: 'Relationships' },
	{
		deepenings: { type: Types.Relationship, ref: 'Deepening', many: true },
		towns: { type: Types.Relationship, ref: 'Town', many: true },
		related: { type: Types.Relationship, ref: 'Argument', many: true },
		categories: { type: Types.Relationship, ref: 'Category', many: true }
	},

	{ heading: 'External Links' },
	{ editLink: { type: Types.Select, options: '1, 2, 3, 4, 5, 6, 7, 8, 9, 10' } },
	{
		externalLinks: {
			label1: {
				it: { type: Types.Text, dependsOn: { editLink: '1' } },
				en: { type: Types.Text, dependsOn: { editLink: '1' } }
			},
			url1: { type: Types.Url, dependsOn: { editLink: '1' } },

			label2: {
				it: { type: Types.Text, dependsOn: { editLink: '2' } },
				en: { type: Types.Text, dependsOn: { editLink: '2' } }
			},
			url2: { type: Types.Url, dependsOn: { editLink: '2' } },

			label3: {
				it: { type: Types.Text, dependsOn: { editLink: '3' } },
				en: { type: Types.Text, dependsOn: { editLink: '3' } }
			},
			url3: { type: Types.Url, dependsOn: { editLink: '3' } },

			label4: {
				it: { type: Types.Text, dependsOn: { editLink: '4' } },
				en: { type: Types.Text, dependsOn: { editLink: '4' } }
			},
			url4: { type: Types.Url, dependsOn: { editLink: '4' } },

			label5: {
				it: { type: Types.Text, dependsOn: { editLink: '5' } },
				en: { type: Types.Text, dependsOn: { editLink: '5' } }
			},
			url5: { type: Types.Url, dependsOn: { editLink: '5' } },

			label6: {
				it: { type: Types.Text, dependsOn: { editLink: '6' } },
				en: { type: Types.Text, dependsOn: { editLink: '6' } }
			},
			url6: { type: Types.Url, dependsOn: { editLink: '6' } },

			label7: {
				it: { type: Types.Text, dependsOn: { editLink: '7' } },
				en: { type: Types.Text, dependsOn: { editLink: '7' } }
			},
			url7: { type: Types.Url, dependsOn: { editLink: '7' } },

			label8: {
				it: { type: Types.Text, dependsOn: { editLink: '8' } },
				en: { type: Types.Text, dependsOn: { editLink: '8' } }
			},
			url8: { type: Types.Url, dependsOn: { editLink: '8' } },

			label9: {
				it: { type: Types.Text, dependsOn: { editLink: '9' } },
				en: { type: Types.Text, dependsOn: { editLink: '9' } }
			},
			url9: { type: Types.Url, dependsOn: { editLink: '9' } },

			label10: {
				it: { type: Types.Text, dependsOn: { editLink: '10' } },
				en: { type: Types.Text, dependsOn: { editLink: '10' } }
			},
			url10: { type: Types.Url, dependsOn: { editLink: '10' } }
		}
	},

	{ heading: 'Blocks' },
	{ editBlock: { type: Types.Select, options: '1, 2, 3, 4, 5' } },
	{
		block1: {
			order: { type: Types.Number, dependsOn: { editBlock: '1' } },
			text: {
				it: { type: Types.Textarea, dependsOn: { editBlock: '1' } },
				en: { type: Types.Textarea, dependsOn: { editBlock: '1' } }
			},
			img: {
				src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload', dependsOn: { editBlock: '1' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '1' } },
					en: { type: Types.Text, dependsOn: { editBlock: '1' } }
				}
			},
			gallery: {
				pics: { type: Types.CloudinaryImages, dependsOn: { editBlock: '1' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '1' } },
					en: { type: Types.Text, dependsOn: { editBlock: '1' } }
				}
			},
			video: {
				src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload', dependsOn: { editBlock: '1' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '1' } },
					en: { type: Types.Text, dependsOn: { editBlock: '1' } }
				}
			},
			externalVideo: {
				host: { type: Types.Select, options: 'youtube, vimeo', dependsOn: { editBlock: '1' } },
				id: { type: Types.Text, dependsOn: { editBlock: '1' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '1' } },
					en: { type: Types.Text, dependsOn: { editBlock: '1' } }
				}
			}
		},
		block2: {
			order: { type: Types.Number, dependsOn: { editBlock: '2' } },
			text: {
				it: { type: Types.Textarea, dependsOn: { editBlock: '2' } },
				en: { type: Types.Textarea, dependsOn: { editBlock: '2' } }
			},
			img: {
				src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload', dependsOn: { editBlock: '2' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '2' } },
					en: { type: Types.Text, dependsOn: { editBlock: '2' } }
				}
			},
			gallery: {
				pics: { type: Types.CloudinaryImages, dependsOn: { editBlock: '2' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '2' } },
					en: { type: Types.Text, dependsOn: { editBlock: '2' } }
				}
			},
			video: {
				src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload', dependsOn: { editBlock: '2' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '2' } },
					en: { type: Types.Text, dependsOn: { editBlock: '2' } }
				}
			},
			externalVideo: {
				host: { type: Types.Select, options: 'youtube, vimeo', dependsOn: { editBlock: '2' } },
				id: { type: Types.Text, dependsOn: { editBlock: '2' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '2' } },
					en: { type: Types.Text, dependsOn: { editBlock: '2' } }
				}
			}
		},
		block3: {
			order: { type: Types.Number, dependsOn: { editBlock: '3' } },
			text: {
				it: { type: Types.Textarea, dependsOn: { editBlock: '3' } },
				en: { type: Types.Textarea, dependsOn: { editBlock: '3' } }
			},
			img: {
				src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload', dependsOn: { editBlock: '3' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '3' } },
					en: { type: Types.Text, dependsOn: { editBlock: '3' } }
				}
			},
			gallery: {
				pics: { type: Types.CloudinaryImages, dependsOn: { editBlock: '3' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '3' } },
					en: { type: Types.Text, dependsOn: { editBlock: '3' } }
				}
			},
			video: {
				src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload', dependsOn: { editBlock: '3' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '3' } },
					en: { type: Types.Text, dependsOn: { editBlock: '3' } }
				}
			},
			externalVideo: {
				host: { type: Types.Select, options: 'youtube, vimeo', dependsOn: { editBlock: '3' } },
				id: { type: Types.Text, dependsOn: { editBlock: '3' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '3' } },
					en: { type: Types.Text, dependsOn: { editBlock: '3' } }
				}
			}
		},
		block4: {
			order: { type: Types.Number, dependsOn: { editBlock: '4' } },
			text: {
				it: { type: Types.Textarea, dependsOn: { editBlock: '4' } },
				en: { type: Types.Textarea, dependsOn: { editBlock: '4' } }
			},
			img: {
				src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload', dependsOn: { editBlock: '4' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '4' } },
					en: { type: Types.Text, dependsOn: { editBlock: '4' } }
				}
			},
			gallery: {
				pics: { type: Types.CloudinaryImages, dependsOn: { editBlock: '4' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '4' } },
					en: { type: Types.Text, dependsOn: { editBlock: '4' } }
				}
			},
			video: {
				src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload', dependsOn: { editBlock: '4' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '4' } },
					en: { type: Types.Text, dependsOn: { editBlock: '4' } }
				}
			},
			externalVideo: {
				host: { type: Types.Select, options: 'youtube, vimeo', dependsOn: { editBlock: '4' } },
				id: { type: Types.Text, dependsOn: { editBlock: '4' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '4' } },
					en: { type: Types.Text, dependsOn: { editBlock: '4' } }
				}
			}
		},
		block5: {
			order: { type: Types.Number, dependsOn: { editBlock: '5' } },
			text: {
				it: { type: Types.Textarea, dependsOn: { editBlock: '5' } },
				en: { type: Types.Textarea, dependsOn: { editBlock: '5' } }
			},
			img: {
				src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload', dependsOn: { editBlock: '5' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '5' } },
					en: { type: Types.Text, dependsOn: { editBlock: '5' } }
				}
			},
			gallery: {
				pics: { type: Types.CloudinaryImages, dependsOn: { editBlock: '5' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '5' } },
					en: { type: Types.Text, dependsOn: { editBlock: '5' } }
				}
			},
			video: {
				src: { type: Types.CloudinaryImage, select: true, selectPrefix: 'http://res.cloudinary.com/gianluca-macaluso/image/upload', dependsOn: { editBlock: '5' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '5' } },
					en: { type: Types.Text, dependsOn: { editBlock: '5' } }
				}
			},
			externalVideo: {
				host: { type: Types.Select, options: 'youtube, vimeo', dependsOn: { editBlock: '5' } },
				id: { type: Types.Text, dependsOn: { editBlock: '5' } },
				caption: {
					it: { type: Types.Text, dependsOn: { editBlock: '5' } },
					en: { type: Types.Text, dependsOn: { editBlock: '5' } }
				}
			}
		}
	},

	{ heading: 'Contributions' },
	{ editContribution: { type: Types.Select, options: '1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20' } },
	{
		contributions: {
			label1: {
				it: { type: Types.Text, dependsOn: { editContribution: '1' } },
				en: { type: Types.Text, dependsOn: { editContribution: '1' } }
			},
			url1: { type: Types.Url, dependsOn: { editContribution: '1' } },
			caption1: {
				it: { type: Types.Text, dependsOn: { editContribution: '1' } },
				en: { type: Types.Text, dependsOn: { editContribution: '1' } }
			},

			label2: {
				it: { type: Types.Text, dependsOn: { editContribution: '2' } },
				en: { type: Types.Text, dependsOn: { editContribution: '2' } }
			},
			url2: { type: Types.Url, dependsOn: { editContribution: '2' } },
			caption2: {
				it: { type: Types.Text, dependsOn: { editContribution: '2' } },
				en: { type: Types.Text, dependsOn: { editContribution: '2' } }
			},

			label3: {
				it: { type: Types.Text, dependsOn: { editContribution: '3' } },
				en: { type: Types.Text, dependsOn: { editContribution: '3' } }
			},
			url3: { type: Types.Url, dependsOn: { editContribution: '3' } },
			caption3: {
				it: { type: Types.Text, dependsOn: { editContribution: '3' } },
				en: { type: Types.Text, dependsOn: { editContribution: '3' } }
			},

			label4: {
				it: { type: Types.Text, dependsOn: { editContribution: '4' } },
				en: { type: Types.Text, dependsOn: { editContribution: '4' } }
			},
			url4: { type: Types.Url, dependsOn: { editContribution: '4' } },
			caption4: {
				it: { type: Types.Text, dependsOn: { editContribution: '4' } },
				en: { type: Types.Text, dependsOn: { editContribution: '4' } }
			},

			label5: {
				it: { type: Types.Text, dependsOn: { editContribution: '5' } },
				en: { type: Types.Text, dependsOn: { editContribution: '5' } }
			},
			url5: { type: Types.Url, dependsOn: { editContribution: '5' } },
			caption5: {
				it: { type: Types.Text, dependsOn: { editContribution: '5' } },
				en: { type: Types.Text, dependsOn: { editContribution: '5' } }
			},

			label6: {
				it: { type: Types.Text, dependsOn: { editContribution: '6' } },
				en: { type: Types.Text, dependsOn: { editContribution: '6' } }
			},
			url6: { type: Types.Url, dependsOn: { editContribution: '6' } },
			caption6: {
				it: { type: Types.Text, dependsOn: { editContribution: '6' } },
				en: { type: Types.Text, dependsOn: { editContribution: '6' } }
			},

			label7: {
				it: { type: Types.Text, dependsOn: { editContribution: '7' } },
				en: { type: Types.Text, dependsOn: { editContribution: '7' } }
			},
			url7: { type: Types.Url, dependsOn: { editContribution: '7' } },
			caption7: {
				it: { type: Types.Text, dependsOn: { editContribution: '7' } },
				en: { type: Types.Text, dependsOn: { editContribution: '7' } }
			},

			label8: {
				it: { type: Types.Text, dependsOn: { editContribution: '8' } },
				en: { type: Types.Text, dependsOn: { editContribution: '8' } }
			},
			url8: { type: Types.Url, dependsOn: { editContribution: '8' } },
			caption8: {
				it: { type: Types.Text, dependsOn: { editContribution: '8' } },
				en: { type: Types.Text, dependsOn: { editContribution: '8' } }
			},

			label9: {
				it: { type: Types.Text, dependsOn: { editContribution: '9' } },
				en: { type: Types.Text, dependsOn: { editContribution: '9' } }
			},
			url9: { type: Types.Url, dependsOn: { editContribution: '9' } },
			caption9: {
				it: { type: Types.Text, dependsOn: { editContribution: '9' } },
				en: { type: Types.Text, dependsOn: { editContribution: '9' } }
			},

			label10: {
				it: { type: Types.Text, dependsOn: { editContribution: '10' } },
				en: { type: Types.Text, dependsOn: { editContribution: '10' } }
			},
			url10: { type: Types.Url, dependsOn: { editContribution: '10' } },
			caption10: {
				it: { type: Types.Text, dependsOn: { editContribution: '10' } },
				en: { type: Types.Text, dependsOn: { editContribution: '10' } }
			},

			label11: {
				it: { type: Types.Text, dependsOn: { editContribution: '11' } },
				en: { type: Types.Text, dependsOn: { editContribution: '11' } }
			},
			url11: { type: Types.Url, dependsOn: { editContribution: '11' } },
			caption11: {
				it: { type: Types.Text, dependsOn: { editContribution: '11' } },
				en: { type: Types.Text, dependsOn: { editContribution: '11' } }
			},

			label12: {
				it: { type: Types.Text, dependsOn: { editContribution: '12' } },
				en: { type: Types.Text, dependsOn: { editContribution: '12' } }
			},
			url12: { type: Types.Url, dependsOn: { editContribution: '12' } },
			caption12: {
				it: { type: Types.Text, dependsOn: { editContribution: '12' } },
				en: { type: Types.Text, dependsOn: { editContribution: '12' } }
			},

			label13: {
				it: { type: Types.Text, dependsOn: { editContribution: '13' } },
				en: { type: Types.Text, dependsOn: { editContribution: '13' } }
			},
			url13: { type: Types.Url, dependsOn: { editContribution: '13' } },
			caption13: {
				it: { type: Types.Text, dependsOn: { editContribution: '13' } },
				en: { type: Types.Text, dependsOn: { editContribution: '13' } }
			},

			label14: {
				it: { type: Types.Text, dependsOn: { editContribution: '14' } },
				en: { type: Types.Text, dependsOn: { editContribution: '14' } }
			},
			url14: { type: Types.Url, dependsOn: { editContribution: '14' } },
			caption14: {
				it: { type: Types.Text, dependsOn: { editContribution: '14' } },
				en: { type: Types.Text, dependsOn: { editContribution: '14' } }
			},

			label15: {
				it: { type: Types.Text, dependsOn: { editContribution: '15' } },
				en: { type: Types.Text, dependsOn: { editContribution: '15' } }
			},
			url15: { type: Types.Url, dependsOn: { editContribution: '15' } },
			caption15: {
				it: { type: Types.Text, dependsOn: { editContribution: '15' } },
				en: { type: Types.Text, dependsOn: { editContribution: '15' } }
			},

			label16: {
				it: { type: Types.Text, dependsOn: { editContribution: '16' } },
				en: { type: Types.Text, dependsOn: { editContribution: '16' } }
			},
			url16: { type: Types.Url, dependsOn: { editContribution: '16' } },
			caption16: {
				it: { type: Types.Text, dependsOn: { editContribution: '16' } },
				en: { type: Types.Text, dependsOn: { editContribution: '16' } }
			},

			label17: {
				it: { type: Types.Text, dependsOn: { editContribution: '17' } },
				en: { type: Types.Text, dependsOn: { editContribution: '17' } }
			},
			url17: { type: Types.Url, dependsOn: { editContribution: '17' } },
			caption17: {
				it: { type: Types.Text, dependsOn: { editContribution: '17' } },
				en: { type: Types.Text, dependsOn: { editContribution: '17' } }
			},

			label18: {
				it: { type: Types.Text, dependsOn: { editContribution: '18' } },
				en: { type: Types.Text, dependsOn: { editContribution: '18' } }
			},
			url18: { type: Types.Url, dependsOn: { editContribution: '18' } },
			caption18: {
				it: { type: Types.Text, dependsOn: { editContribution: '18' } },
				en: { type: Types.Text, dependsOn: { editContribution: '18' } }
			},

			label19: {
				it: { type: Types.Text, dependsOn: { editContribution: '19' } },
				en: { type: Types.Text, dependsOn: { editContribution: '19' } }
			},
			url19: { type: Types.Url, dependsOn: { editContribution: '19' } },
			caption19: {
				it: { type: Types.Text, dependsOn: { editContribution: '19' } },
				en: { type: Types.Text, dependsOn: { editContribution: '19' } }
			},

			label20: {
				it: { type: Types.Text, dependsOn: { editContribution: '20' } },
				en: { type: Types.Text, dependsOn: { editContribution: '20' } }
			},
			url20: { type: Types.Url, dependsOn: { editContribution: '20' } },
			caption20: {
				it: { type: Types.Text, dependsOn: { editContribution: '20' } },
				en: { type: Types.Text, dependsOn: { editContribution: '20' } }
			}
		}
	}
);

Argument.register();
