var keystone = require('keystone');
var utils = keystone.utils;
var Types = keystone.Field.Types;

/**
 * Category Model
 * =============
 */

var Category = new keystone.List('Category', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Category.schema.pre('save', function(next) {
    this.slugEN = utils.slug( this.nameEN );
    next();
});

Category.add({
	name: { type: Types.Text, required: true },
	nameEN: { type: Types.Text },
	slugEN: { type: Types.Text, noedit: true },
	description: {
		it: { type: Types.Textarea },
		en: { type: Types.Textarea }
	},
	color: { type: Types.Color },
	icon: { 
		type: Types.CloudinaryImage, 
		select: true 
	}
});

Category.register();
