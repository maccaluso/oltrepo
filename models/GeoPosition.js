var keystone = require('keystone');
var utils = keystone.utils;
var Types = keystone.Field.Types;

/**
 * GeoPosition Model
 * =============
 */

var GeoPosition = new keystone.List('GeoPosition', {
	autokey: { from: 'name', path: 'key', unique: true },
});

GeoPosition.schema.pre('save', function(next) {
    this.slugEN = utils.slug(this.nameEN);
    next();
});

GeoPosition.add({
	name: { type: Types.Text, required: true },
	nameEN: { type: Types.Text },
	slugEN: { type: Types.Text, noedit: true },
	description: {
		it: { type: Types.Textarea },
		en: { type: Types.Textarea }
	},
	location: { type: Types.Location }
});

// GeoPosition.relationship({ path: 'arguments', ref: 'Argument', refPath: 'argument' });

GeoPosition.register();
