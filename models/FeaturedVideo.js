var keystone = require('keystone');
var utils = keystone.utils;
var Types = keystone.Field.Types;
var deepPopulate = require('mongoose-deep-populate')( keystone.mongoose );

/**
 * FeaturedVideo Model
 * =============
 */

var FeaturedVideo = new keystone.List('FeaturedVideo', {
	autokey: { from: 'name', path: 'key', unique: true },
});

FeaturedVideo.schema.plugin(deepPopulate);

FeaturedVideo.schema.pre('save', function(next) {
    this.slugEN = utils.slug(this.nameEN);
    next();
});

FeaturedVideo.add(

	{ heading: 'Info' },
	{
		name: { type: String, required: true },
		nameEN: { type: Types.Text },
		slugEN: { type: Types.Text, noedit: true },
		publishedDate: { type: Date, default: Date.now },
		description: {
			it: { type: Types.Textarea },
			en: { type: Types.Textarea }
		}
	},

	{ heading: 'Video data' },
	{
		videoHost: { type: Types.Select, options: 'Youtube, Vimeo' },
		videoID: { type: Types.Text },
		showInHome: { type: Types.Boolean, default: true }
	},
	
	{ heading: 'Relationships' },
	{
		categories: { type: Types.Relationship, ref: 'Category', many: true },
		towns: { type: Types.Relationship, ref: 'Town', many: true }
	}

);

FeaturedVideo.register();
