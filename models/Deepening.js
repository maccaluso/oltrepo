var keystone = require('keystone');
var utils = keystone.utils;
var Types = keystone.Field.Types;

/**
 * Deepening Model
 * =============
 */

var Deepening = new keystone.List('Deepening', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Deepening.schema.pre('save', function(next) {
    this.slugEN = utils.slug(this.nameEN);
    next();
});

Deepening.add({
	name: { type: Types.Text, required: true },
	nameEN: { type: Types.Text },
	slugEN: { type: Types.Text, noedit: true },
	caption: {
		it: { type: Types.Textarea },
		en: { type: Types.Textarea }
	},
	img: { type: Types.CloudinaryImage }
});

Deepening.register();
