var fs = require('fs');
var path = require('path');
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * PdfEnquiry Model
 * =============
 */

var PdfEnquiry = new keystone.List('PdfEnquiry', {
	nocreate: true,
	noedit: true,
});

PdfEnquiry.add({
	name: { type: Types.Name, required: true },
	email: { type: Types.Email, required: true },
	// phone: { type: String },
	// PdfenquiryType: { type: Types.Select, options: [
	// 	{ value: 'message', label: 'Just leaving a message' },
	// 	{ value: 'question', label: 'I\'ve got a question' },
	// 	{ value: 'other', label: 'Something else...' },
	// ] },
	message: { type: Types.Markdown, required: true },
	createdAt: { type: Date, default: Date.now },
});

PdfEnquiry.schema.pre('save', function (next) {
	this.wasNew = this.isNew;
	next();
});

PdfEnquiry.schema.post('save', function () {
	if (this.wasNew) {
		this.sendNotificationEmail();
	}
});

PdfEnquiry.schema.methods.sendNotificationEmail = function (callback) {
	if (typeof callback !== 'function') {
		callback = function (err) {
			if (err) {
				console.error('There was an error sending the notification email:', err);
			}
		};
	}

	if (!process.env.MAILGUN_API_KEY || !process.env.MAILGUN_DOMAIN) {
		console.log('Unable to send email - no mailgun credentials provided');
		return callback(new Error('could not find mailgun credentials'));
	}

	var pdfenquiry = this;
	var brand = keystone.get('brand');
	var file = fs.readFileSync( path.join(__dirname, '../public/pdf/le-ciclovie-delloltrepo-mantovano.pdf') );
	var file64 =  new Buffer(file).toString('base64');

	keystone.list('User').model.find().where('isAdmin', true).exec(function (err, admins) {
		if (err) return callback(err);
		new keystone.Email({
			templateName: 'enquiry-notification',
			transport: 'mailgun',
		}).send({
			to: admins,
			from: {
				name: 'Consorzio oltrepo mantovano',
				email: 'no-reply@itinerarioltrepomantovano.com',
			},
			subject: 'New PdfEnquiry for Consorzio oltrepo mantovano',
			enquiry: pdfenquiry,
			attachments:[{
				'type': 'application/pdf',
				'name': 'le-ciclovie-delloltrepo-mantovano.pdf',
				'content': file64
			}],
			brand: brand,
			layout: false,
		}, callback);
	});
};

PdfEnquiry.defaultSort = '-createdAt';
PdfEnquiry.defaultColumns = 'name, email, PdfenquiryType, createdAt';
PdfEnquiry.register();
