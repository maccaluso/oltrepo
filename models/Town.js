var keystone = require('keystone');
var Types = keystone.Field.Types;
var deepPopulate = require('mongoose-deep-populate')( keystone.mongoose );

/**
 * Town Model
 * =============
 */

var Town = new keystone.List('Town', {
	autokey: { from: 'name', path: 'key', unique: true }
});
Town.schema.plugin(deepPopulate);

Town.add(
	{
		name: { type: Types.Text, required: true },
		description: {
			it: { type: Types.Textarea },
			en: { type: Types.Textarea }
		},
		location: { type: Types.Relationship, ref: 'GeoPosition' },
		routingStartDefault: { type: Types.Relationship, ref: 'GeoPosition', many: true },

		publishedDate: { type: Date, default: Date.now },

		coverImg: {
			src: { type: Types.CloudinaryImage },
			caption: {
				it: { type: Types.Text },
				en: { type: Types.Text }
			}
		},
		gallery: { type: Types.CloudinaryImages },
		useVideoCover: { type: Types.Boolean },
		videoCover: {
			host: { type: Types.Select, options: 'youtube, vimeo', dependsOn: { useVideoCover: true } },
			id: { type: Types.Text, dependsOn: { useVideoCover: true } }
		}
		
	},
	{ heading: 'Relationships' },
	{
		arguments: { type: Types.Relationship, ref: 'Argument', many: true },
		relatedTowns: { type: Types.Relationship, ref: 'Town', many: true }
	},
	
	{ heading: 'External Links' },
	{ editLink: { type: Types.Select, options: '1, 2, 3, 4, 5, 6, 7, 8, 9, 10' } },
	{
		externalLinks: {
			label1: {
				it: { type: Types.Text, dependsOn: { editLink: '1' } },
				en: { type: Types.Text, dependsOn: { editLink: '1' } }
			},
			url1: { type: Types.Url, dependsOn: { editLink: '1' } },

			label2: {
				it: { type: Types.Text, dependsOn: { editLink: '2' } },
				en: { type: Types.Text, dependsOn: { editLink: '2' } }
			},
			url2: { type: Types.Url, dependsOn: { editLink: '2' } },

			label3: {
				it: { type: Types.Text, dependsOn: { editLink: '3' } },
				en: { type: Types.Text, dependsOn: { editLink: '3' } }
			},
			url3: { type: Types.Url, dependsOn: { editLink: '3' } },

			label4: {
				it: { type: Types.Text, dependsOn: { editLink: '4' } },
				en: { type: Types.Text, dependsOn: { editLink: '4' } }
			},
			url4: { type: Types.Url, dependsOn: { editLink: '4' } },

			label5: {
				it: { type: Types.Text, dependsOn: { editLink: '5' } },
				en: { type: Types.Text, dependsOn: { editLink: '5' } }
			},
			url5: { type: Types.Url, dependsOn: { editLink: '5' } },

			label6: {
				it: { type: Types.Text, dependsOn: { editLink: '6' } },
				en: { type: Types.Text, dependsOn: { editLink: '6' } }
			},
			url6: { type: Types.Url, dependsOn: { editLink: '6' } },

			label7: {
				it: { type: Types.Text, dependsOn: { editLink: '7' } },
				en: { type: Types.Text, dependsOn: { editLink: '7' } }
			},
			url7: { type: Types.Url, dependsOn: { editLink: '7' } },

			label8: {
				it: { type: Types.Text, dependsOn: { editLink: '8' } },
				en: { type: Types.Text, dependsOn: { editLink: '8' } }
			},
			url8: { type: Types.Url, dependsOn: { editLink: '8' } },

			label9: {
				it: { type: Types.Text, dependsOn: { editLink: '9' } },
				en: { type: Types.Text, dependsOn: { editLink: '9' } }
			},
			url9: { type: Types.Url, dependsOn: { editLink: '9' } },

			label10: {
				it: { type: Types.Text, dependsOn: { editLink: '10' } },
				en: { type: Types.Text, dependsOn: { editLink: '10' } }
			},
			url10: { type: Types.Url, dependsOn: { editLink: '10' } }
		}
	}
);

Town.register();
