// Sliders

function sliderNavigation() {
    $('body').off('click', '.slider-nav-control', sliderNavigation);

    var dir;
    $(this).hasClass('slide-right') ? dir = -1 : dir = 1;

    var cardW = $('.featured-card').width();
    var destPos = $('#featuredSlider').position().left + ( cardW * dir );

    var maxPos = cardW * ($('.featured-card').length - 3)

    $('#featuredSlider').css({ left: destPos + 'px' });

    if(destPos < 0) { $('.slide-left').show(); }
    if(destPos > -maxPos) { $('.slide-right').show(); }

    if(destPos == -maxPos) { $('.slide-right').hide(); }
    if(destPos == 0) { $('.slide-left').hide(); }
}

function videoGalleryNavigation() {
    $('body').off('click', '.video-gallery-nav-control', sliderNavigation);

    var dir;
    $(this).hasClass('video-gallery-slide-right') ? dir = -1 : dir = 1;

    var cardW = $('.video-gallery-card').width();
    var destPos = $('#videoGallerySlider').position().left + ( cardW * dir );

    var maxPos = cardW * ($('.video-gallery-card').length - 1)

    $('#videoGallerySlider').css({ left: destPos + 'px' });

    if(destPos < 0) { $('.video-gallery-slide-left').show(); }
    if(destPos > -maxPos) { $('.video-gallery-slide-right').show(); }

    if(destPos == -maxPos) { $('.video-gallery-slide-right').hide(); }
    if(destPos == 0) { $('.video-gallery-slide-left').hide(); }
}

function argumentSliderNavigation() {
    $('body').off('click', '.argument-featured-nav', argumentSliderNavigation);

    var dir;
    $(this).attr('id').indexOf('Right') > -1 ? dir = -1 : dir = 1;

    var cardW = $('.argument-featured-card').width() + 15;
    var destPos = ( $('#argumentFeaturedSlider').position().left - 15 ) + ( cardW * dir );

    var maxPos = cardW * ( $('.argument-featured-card').length - 4 );

    $('#argumentFeaturedSlider').css({ left: destPos + 'px' });

    if(destPos < 0) { $('#argumentFeaturedSliderNavLeft').show(); }
    if(destPos > -maxPos) { $('#argumentFeaturedSliderNavRight').show(); }

    if(destPos == -maxPos) { $('#argumentFeaturedSliderNavRight').hide(); }
    if(destPos == 0) { $('#argumentFeaturedSliderNavLeft').hide(); }
}

function townRelatedSliderNavigation() {
    $('body').off('click', '.town-related-nav', argumentSliderNavigation);

    var dir;
    $(this).attr('id').indexOf('Right') > -1 ? dir = -1 : dir = 1;

    var cardW = $('.town-related-card').width() + 15;
    var destPos = ( $('#townRelatedSlider').position().left - 15 ) + ( cardW * dir );

    var maxPos = cardW * ( $('.town-related-card').length - 4 );

    $('#townRelatedSlider').css({ left: destPos + 'px' });

    if(destPos < 0) { $('#townRelatedSliderNavLeft').show(); }
    if(destPos > -maxPos) { $('#townRelatedSliderNavRight').show(); }

    if(destPos == -maxPos) { $('#townRelatedSliderNavRight').hide(); }
    if(destPos == 0) { $('#townRelatedSliderNavLeft').hide(); }
}