// UI Adjustments

function setVisibleCards(type) {
    var numVisibleCards;
    var w = $(window).width();

    switch(type) {
        case 'home-featured':
            w <= 768 ? numVisibleCards = 1 : numVisibleCards = 3;
            break;
        case 'home-video-gallery':
            numVisibleCards = 1;
            break;
        case 'town-related':
            if( w <= 320 )
                numVisibleCards = 1;
            if( w > 320 && w <= 768 )
                numVisibleCards = 2;
            if( w > 768 )
                numVisibleCards = 4;
            break;
        case 'argument-featured':
            if( w <= 320 )
                numVisibleCards = 1;
            if( w > 320 && w <= 768 )
                numVisibleCards = 2;
            if( w > 768 )
                numVisibleCards = 4;
            break;
    }

    return numVisibleCards;
}

function initHomeFeaturedSlider() {
    var numVisibleCards = setVisibleCards('home-featured');

    $('#featuredContainer').css({ opacity: 0 });
    $('#featuredSlider').css({ left: 0 });
    $('.slide-left').hide();

    var containerW = $('#featuredContainer').width();
    var cardW = containerW / numVisibleCards;
    var sliderW = cardW * $('.featured-card').length;

    $('.featured-card').width( cardW );
    $('#featuredSlider').width( sliderW );

    if( $('.featured-card').length <= numVisibleCards )
        $('.slider-nav-control').hide();

    $('.slider-nav-control').css({
        top: - ( $('#featuredContainer').height() / 2 + $('.slider-nav-control').height() / 2 ) + 'px'
    })

    $('#featuredContainer').css({ opacity: 1 });
}

function initHomeVideoGallery() {
    var numVisibleCards = setVisibleCards('home-video-gallery');

    $('#videoGalleryContainer').css({ opacity: 0 });
    $('#videoGallerySlider').css({ left: 0 });
    $('.video-gallery-slide-left').hide();

    var containerW = $('#videoGalleryContainer').width();
    var cardW = containerW / numVisibleCards;
    var sliderW = cardW * $('.video-gallery-card').length;

    $('.video-gallery-card').width( cardW );
    $('#videoGallerySlider').width( sliderW );

    if( $('.video-gallery-card').length <= numVisibleCards )
        $('.video-gallery-nav-control').hide();

    $('.video-gallery-nav-control').css({
        top: - ( $('#videoGalleryContainer').height() / 2 + $('.video-gallery-nav-control').height() / 2 ) + 'px'
    })

    $('#videoGalleryContainer').css({ opacity: 1 });
}

function initTownDescription() {
    // if( $('#townDescription').hasClass('town-video-cover-description') )
    // {
    //     $('#townDescription').css({
    //         top: - $('#townDescription').outerHeight() + 'px'
    //     });
    // }
    // else
    // {
        $('#townDescription').css({
            // width: $('#townArguments').width() + 15 + 'px'
            width: $('#townArguments').width() + 'px'
        });
        $('#townDescription').css({
            marginTop: - $('#townDescription').outerHeight() + 'px',
        });
    // }
    $('#townDescription').fadeTo(500, 1);
}

function initTownArgumentInfo() {
    $('.town-argument-info').css( 'height', $('.town-argument-img').height() + 'px' );
}

function initArgumentFeaturedSlider() {
    var numVisibleCards = setVisibleCards('argument-featured');

    $('#argumentFeaturedSliderContainer').css({ opacity: 0 });
    $('#argumentFeaturedSlider').css({ left: 0 });
    $('#argumentFeaturedSliderNavLeft').hide();

    var containerW = $('#argumentFeaturedSliderContainer').width();
    var cardW = containerW / numVisibleCards - 15;
    var sliderW = cardW * $('.argument-featured-card').length + (15 * $('.argument-featured-card').length);

    $('.argument-featured-card').width( cardW );
    $('#argumentFeaturedSlider').width( sliderW );

    if( $('.argument-featured-card').length <= numVisibleCards )
        $('.argument-featured-nav').hide();

    $('#argumentFeaturedSliderContainer').css({ opacity: 1 });
}

function initTownRelatedSlider() {
    var numVisibleCards = setVisibleCards('town-related');

    $('#townRelatedSliderContainer').css({ opacity: 0 });
    $('#townRelatedSlider').css({ left: 0 });
    $('#townRelatedSliderNavLeft').hide();

    var containerW = $('#townRelatedSliderContainer').width();
    var cardW = containerW / numVisibleCards - 15;
    var sliderW = cardW * $('.town-related-card').length + (15 * $('.town-related-card').length);

    $('.town-related-card').width( cardW );
    $('#townRelatedSlider').width( sliderW );

    if( $('.town-related-card').length <= numVisibleCards )
        // console.log( $('.town-related-card').length )
        $('.town-related-nav').hide();

    $('#townRelatedSliderContainer').css({ opacity: 1 });
}

function initContribsContainers() {
    var cardW = $('.didactics-argument-card-inner').width();
    $('.didactics-argument-contribs-container').outerWidth( cardW - 30 );
}