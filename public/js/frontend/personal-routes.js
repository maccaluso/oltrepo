// Personal routes

function toggleRoutesSelector(e, container) {
    var routes = store.get('oltrepoRoutes');

    var html = '';
    html += '<div class="route-selector" style="left: ' + (e.pageX - 15) +'px; top: ' + e.pageY + 'px;">';
        
        html += '<ul class="route-selector-routes">';
            if(routes)
            {
                if(routes.length > 0)
                {
                    for(var i = 0; i < routes.length; i++)
                    {
                        var canAdd = true;
                        for(var j = 0; j < routes[i].wayPoints.length; j++)
                        {
                            if( routes[i].wayPoints[j].id == newWaypoint.id )
                                canAdd = false;
                        }

                        if (canAdd)
                            html += '<li><a class="route-selector-link" href="#">' + routes[i].name + '</a></li>';
                        else
                            html += '<li><a class="route-selector-link disabled" href="#">' + routes[i].name + '</a></li>';
                    }
                }
            }
            else
            {
                html += '<li><a href="#">' + noSavedRoutesText + '</a></li>';
            }
        html += '</ul>';
    
        html += '<a class="route-selector-new-route" href="#">' + newRouteText + '</a>';

        html += '<div class="route-selector-new-route-input-container clearfix">';
            html += '<input type="text" name="routeSelectorNewRouteInput">';
            html += '<span class="route-selector-new-route-input-submit"></span>';
        html += '</div>';

    html += '</div>';

    if( container.find('.route-selector').length == 0 )
    {
        container.append( html );
        var delay;
        delay = setTimeout(function(){
            container.find('.route-selector').addClass('expanded');
            clearTimeout( delay );
        }, 100);
    }
    else
    {
        container.find('.route-selector').remove();
    }
}

function toggleRoutesSelectorAll(e, container) {
    var routes = store.get('oltrepoRoutes');

    var html = '';
    html += '<div class="route-selector" style="left: ' + (e.pageX - 15) +'px; top: ' + e.pageY + 'px;">';
        
        html += '<ul class="route-selector-routes">';
            if(routes)
            {
                if(routes.length > 0)
                {
                    for(var i = 0; i < routes.length; i++)
                    {
                        html += '<li><a class="route-selector-link-all" href="#">' + routes[i].name + '</a></li>';
                    }
                }
            }
            else
            {
                html += '<li><a href="#">' + noSavedRoutesText + '</a></li>';
            }
        html += '</ul>';
    
        html += '<a class="route-selector-new-route" href="#">' + newRouteText + '</a>';

        html += '<div class="route-selector-new-route-input-container clearfix">';
            html += '<input type="text" name="routeSelectorNewRouteInputAll">';
            html += '<span class="route-selector-new-route-input-submit-all"></span>';
        html += '</div>';

    html += '</div>';

    if( container.find('.route-selector').length == 0 )
    {
        container.append( html );
        var delay;
        delay = setTimeout(function(){
            container.find('.route-selector').addClass('expanded');
            clearTimeout( delay );
        }, 100);
    }
    else
    {
        container.find('.route-selector').remove();
    }
}

function updateRouteSelector() {
    var routes = store.get( 'oltrepoRoutes' );
    $('ul.route-selector-routes').find('li').remove();
    for(var i = 0; i < routes.length; i++)
    {
        var canAdd = true;
        for(var j = 0; j < routes[i].wayPoints.length; j++)
        {
            if( routes[i].wayPoints[j].id == newWaypoint.id )
                canAdd = false;
        }
        var linkClass;
        canAdd ? linkClass = 'route-select-link' : linkClass = 'route-select-link disabled';

        $('ul.route-selector-routes').append( '<li><a class="' + linkClass + '" href="#">' + routes[i].name + '</a></li>' );
    }

    $('.route-selector-new-route-input-container').hide();
    $('.route-selector-new-route').show();
}

function saveNewRoute() {
    var routes = store.get( 'oltrepoRoutes' );
    if( routes )
    {
        for(var i = 0; i < routes.length; i++)
            routes[i].selected = false;

        routes.push(
            {
                name: $('input[name="routeSelectorNewRouteInput"]').val(),
                slug: 'test',
                description: '',
                selected: true,
                wayPoints: [newWaypoint]
            }
        );

        store.set( 'oltrepoRoutes', routes);
    }
    else
    {
        store.set( 'oltrepoRoutes',  [
            {
                name: $('input[name="routeSelectorNewRouteInput"]').val(),
                slug: 'test',
                description: '',
                selected: true,
                wayPoints: [newWaypoint]
            }
        ]);
    }

    updateRouteSelector();
}

function addWaypointToRoute(index) {
    var routes = store.get('oltrepoRoutes');
    
    for(var i = 0; i < routes.length; i++)
        routes[i].selected = false;
    
    routes[index].selected = true;
    
    routes[index].wayPoints.push( newWaypoint );
    $('#routeStepsCounter').text( routes[index].wayPoints.length );
    
    store.set('oltrepoRoutes', routes);
}

function setNewWaypoint(caller) {
    newWaypoint = caller.data();

    $.when( 
        $.ajax("/api/argument/" + caller.data().id) 
    )
    .then( 
        function(argument) {
            newWaypoint.name = {
                it: argument.name,
                en: argument.nameEN
            };
            newWaypoint.slug = {
                it: argument.key,
                en: argument.slugEN
            };

            newWaypoint.categories = argument.categories;
        }, 
        function(err) {
            console.log(err);
        }
    );
}

function addAllToRoute(routeIndex) {
    $('.category-add-to-route-link').each(function(i){
        var data = $(this).data();
        $.when( 
            $.ajax( "/api/argument/" + data.id ) 
        )
        .then( 
            function(argument) {
                newWaypoint = data;

                if( currentLanguage == 'it' ) 
                {
                    newWaypoint.name = argument.name;
                    newWaypoint.slug = argument.key;
                }
                else
                {
                    newWaypoint.name = argument.nameEN;
                    newWaypoint.slug = argument.slugEN;
                }

                newWaypoint.categories = argument.categories;

                addWaypointToRoute( routeIndex );
            }, 
            function(err) {
                console.log(err);
            }
        );
    });
}