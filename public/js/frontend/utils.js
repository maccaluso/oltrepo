// Utils

function getArgumentName(arg) {
    var name;
    currentLanguage == 'it' ? name = arg.name : name = arg.nameEN;
    return name;
}
function getArgumentSlug(arg) {
    var slug;
    currentLanguage == 'it' ? slug = arg.key : slug = arg.slugEN;
    return slug;
}

function getCategoryName(cat) {
    var name;
    currentLanguage == 'it' ? name = cat.name : name = cat.nameEN;
    return name;
}
function getCategorySlug(cat) {
    var slug;
    currentLanguage == 'it' ? slug = cat.key : slug = cat.slugEN;
    return slug;
}

function str_pad_left(string,pad,length) { return (new Array(length+1).join(pad)+string).slice(-length); }

function arraymove(arr, fromIndex, toIndex) {
    var element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
}