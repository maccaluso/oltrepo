function addfeaturedRouteMarkers( map ) {
	var mapContainer = $( map.getContainer() );

	var steps = mapContainer.parent().find('.featured-route-step');

	steps.each(function(i){
		// var lng = $(this).find('.featured-route-step-number').data().lng;
		// var lat = $(this).find('.featured-route-step-number').data().lat;

		var markerNumber = $(this).find('.featured-route-step-number').text();
		var location = {
            lat: $(this).find('.featured-route-step-number').data().lat,
            lng: $(this).find('.featured-route-step-number').data().lng
        }

        var el = document.createElement('div');
        el.className = 'marker featured-routes-marker';
        el.innerHTML = markerNumber;

        var marker = new mapboxgl.Marker(el, { offset: [-12.5 / 2, -12.5 / 2] });
        marker.setLngLat( location );
        marker.addTo( map );
	});
}

function initSocialShare() { 
	$('.featured-route-step-social-icon').share();
}