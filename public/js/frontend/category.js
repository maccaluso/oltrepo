// Category map Markers

function addMarkersToCategoryMap() {
    $('.category-argument-card').each(function(i) {
        var id = $(this).data().id;

        var location = {
            lat: $(this).data().lat,
            lng: $(this).data().lng
        }

        var el = document.createElement('div');
        var elInner = document.createElement('div');
        elInner.className = 'marker-inner';
        elInner.style.backgroundColor = $(this).data().color;
        
        el.append( elInner );
        el.className = 'marker argument-marker';
        el.id = 'marker' + id;

        el.addEventListener('click', function(e) {
            resetFilters();

            flyToConfig.center = location;
            flyToConfig.zoom = 14;
            categoryMap.flyTo( flyToConfig );

            filterCategoryCardsByID( id );
        });

        var DOMel = $('<div>')
            .addClass('popup-container category-popup-container')
            .text( $(this).find('h5 a').text() );

        var popup = new mapboxgl.Popup( { offset: 7.5 } );
        popup.setDOMContent( DOMel[0] );
        popups.push( popup );

        var marker = new mapboxgl.Marker(el, { offset: [-15, -15] });
        marker.setLngLat( location );
        marker.setPopup( popup );
        marker.addTo( categoryMap );
    });
}

// Category navigation

function filterByCategory(slug) {
    var currentLocation = window.location.href;
    var parts = currentLocation.split('/');

    if(slug == 'removeFilter')
    {
        parts.pop();
        window.location.href = parts.join('/')
    }
    else
    {

        if( parts.length > 5 )
            parts.pop();
        
        window.location.href = parts.join('/') + '/' + slug;
    }
}

// Category filters

function filterCategoryCards(slug) {
    $('.category-argument-card').each(function(i) {
        if( !$(this).hasClass(slug) )
            $(this).remove();
        else
            $(this).show();
    });
}

function addFilterTag(tag) {
    for (var i in filtered)
    {
        if(i == tag)
        {
            for (var j = 0; j < filtered[i].length; j++)
            {
                $('.category-argument-card[data-id="' + filtered[i][j] + '"]').addClass('filter-match filter-match-' + tag);
                $('.didactics-argument-card[data-id="' + filtered[i][j] + '"]').addClass('filter-match filter-match-' + tag);
            }
        }
    }

    var html = '';
    var color = $('#filterTags').data().color;
    if (color == '#ffffff')
        color = 'rgba(48, 48, 48, 1)';

    html += '<span class="filter-tag">';
        html += '<a class="filter-tag-link enabled" href="#" style="color: ' + color + ';" data-color="' + color + '">' + tag + '</a>';
        html += '<span class="filter-tag-icon enabled"></span>';
        html += '<span class="filter-tag-separator"></span>';
    html += '</span>';

    $('.filter-tag-link, .filter-tag-icon').css({color: '#666666'}).removeClass('enabled');
    $('#filterTags').append( html );
    $('#mainFilterInput, #didacticsFilterInput').val('');
}
function removeFilterTag(tag, target) {
    $(target).parent().fadeOut(300, function(e){
        var prevLink = $(target).parent().prev().find('.filter-tag-link');
        var prevLinkColor;
        prevLink.data() ? prevLinkColor = prevLink.data().color : prevLinkColor = '#666666';
        prevLink.css({color: prevLinkColor}).addClass('enabled');

        $(target).parent().prev().find('.filter-tag-icon').addClass('enabled');

        $(target).parent().remove();

        for (var i in filtered)
        {
            if(i == tag)
            {
                for (var j = 0; j < filtered[i].length; j++)
                {
                    $('.category-argument-card[data-id="' + filtered[i][j] + '"]').removeClass('filter-match filter-match-' + tag).fadeIn();
                    $('.didactics-argument-card[data-id="' + filtered[i][j] + '"]').removeClass('filter-match filter-match-' + tag).fadeIn();
                }
            }
        }

        toggleCategoryCards();
    });
}
function resetFilters() {
    $('.filter-tag').fadeOut(500, function(){ $(this).remove(); });

    $('.category-argument-card, .didactics-argument-card').removeClass('filter-match');
    $('.category-argument-card, .didactics-argument-card').removeClass(function (index, className) {
        return (className.match (/(^|\s)filter-match-\S+/g) || []).join(' ');
    });
    $('.category-argument-card, .didactics-argument-card').fadeIn();

    if(categoryMap)
    {
        flyToConfig.center = mapConfig.center;
        flyToConfig.zoom = mapConfig.zoom;
        categoryMap.flyTo( flyToConfig );

        for(var i = 0; i < popups.length; i++)
            popups[i].remove();

        $('.marker').fadeIn();
    }

    $('#didacticsFilterByCategorySelect').find('option').first().val('didacticsFilterByCategory').text( didacticsFilterByCategory );
    $('#didacticsFilterByCategorySelect').val('didacticsFilterByCategory');
    $('#didacticsFilterByTownSelect').find('option').first().val('didacticsFilterByTown').text( didacticsFilterByTown );
    $('#didacticsFilterByTownSelect').val('didacticsFilterByTown');
}
function resetDidacticsFilters() {
    $('.filter-tag').fadeOut(500, function(){ $(this).remove(); });

    $('.didactics-argument-card').removeClass('filter-match');
    $('.didactics-argument-card').removeClass(function (index, className) {
        return (className.match (/(^|\s)filter-match-\S+/g) || []).join(' ');
    });
}
function toggleCategoryCards() {
    var lastTag = $('.filter-tag-link').last().text();
    if(lastTag.length == 0)
    {
        $('.category-argument-card, .didactics-argument-card, .marker').fadeIn();
    }
    else
    {
        $('.category-argument-card, .didactics-argument-card').each(function(i){
            if ( $(this).hasClass('filter-match-' + lastTag) ) {
                $(this).fadeIn();
                $('#marker' + $(this).data().id).fadeIn();
            }
        });
    }
}
function filterCategoryCardsByTag(tag) {
    $('.category-argument-card, .didactics-argument-card').each(function(i){
        if ( !$(this).hasClass('filter-match-' + tag) ) {
            $(this).fadeOut();
            $('#marker' + $(this).data().id).fadeOut();
        }
    });
}
function filterCategoryCardsByID(id) {
    $('.marker').hide();
    $('#marker' + id).show();

    $('.category-argument-card').hide();
    $('.category-argument-card[data-id="' + id + '"]').fadeIn();
}

function updateSuggestions(suggestion) {
    var canAppend = true;
    var link = $('<a href="#" class="suggestion-link">' + suggestion + '</a>');
    $('.suggestion-link').each(function(i){
        if( $(this).text() == suggestion )
            canAppend = false;
    });

    if(canAppend)
        $('#suggestionsContainer').append(link);

    $('#suggestionsContainer').fadeIn();
}
function removeSuggestions(suggestion) {
    $('.suggestion-link').each(function(i){
        if( $(this).text().search(suggestion) == -1 )
            $(this).remove();
    });

    if( $('.suggestion-link').length == 0 )
        clearSuggestions();
}

function updateDidacticsSuggestions(suggestion) {
    var canAppend = true;
    var link = $('<a href="#" class="didactics-suggestion-link">' + suggestion + '</a>');
    $('.didactics-suggestion-link').each(function(i){
        if( $(this).text() == suggestion )
            canAppend = false;
    });

    if(canAppend)
        $('#suggestionsContainer').append(link);

    $('#suggestionsContainer').fadeIn();
}
function removeDidacticsSuggestions(suggestion) {
    $('.didactics-suggestion-link').each(function(i){
        if( $(this).text().search(suggestion) == -1 )
            $(this).remove();
    });

    if( $('.didactics-suggestion-link').length == 0 )
        clearSuggestions();
}

function clearSuggestions() { $('#suggestionsContainer').html('').fadeOut(); }
