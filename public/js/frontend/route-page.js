function initRoutePageSocialShare() { 
    $('.route-page-social-icon').share();
}

function addMarkersToRoutePageMap() {
    $('.route-step ').each(function(i) {
        var location = {
            lat: $(this).data().lat,
            lng: $(this).data().lng
        }

        var el = document.createElement('div');
        el.className = 'marker route-page-marker';
        el.style.backgroundColor = $(this).data().color;
        el.innerHTML = i+1;

        var marker = new mapboxgl.Marker(el, { offset: [-12.5 / 2, -12.5 / 2] });
        marker.setLngLat( location );
        marker.addTo( routePageMap );
    });
    // $('.category-argument-card').each(function(i) {
    //     var id = $(this).data().id;

    //     var location = {
    //         lat: $(this).data().lat,
    //         lng: $(this).data().lng
    //     }

    //     var el = document.createElement('div');
    //     el.className = 'marker argument-marker';
    //     el.id = 'marker' + id;
    //     el.style.backgroundColor = $(this).data().color;

    //     el.addEventListener('click', function(e) {
    //         resetFilters();

    //         flyToConfig.center = location;
    //         flyToConfig.zoom = 14;
    //         categoryMap.flyTo( flyToConfig );

    //         filterCategoryCardsByID( id );
    //     });

    //     var DOMel = $('<div>')
    //         .addClass('popup-container category-popup-container')
    //         .text( $(this).find('h5 a').text() );

    //     var popup = new mapboxgl.Popup( { offset: 7.5 } );
    //     popup.setDOMContent( DOMel[0] );
    //     popups.push( popup );

    //     var marker = new mapboxgl.Marker(el, { offset: [-12.5 / 2, -12.5 / 2] });
    //     marker.setLngLat( location );
    //     marker.setPopup( popup );
    //     marker.addTo( categoryMap );
    // });
}