// Maps initialization

function initMap() {
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/maccaluso/cj5wksxrn7jau2rmkmryvi5co',
        center: [11.07486879208193,45.00679160929593],
        zoom: 10,
        attributionControl: false
    });

    map.addControl( new mapboxgl.AttributionControl({ compact: true }));
    map.addControl( new mapboxgl.NavigationControl(), 'top-right' );

    map.on('load', function () {
        $.when( 
            $.ajax("/api/towns"), 
            $.ajax("/api/arguments") 
        )
        .then( 
            function(towns, arguments) {
                addTowns( towns[0] );
                addArgumentMarkers( arguments[0] );
            }, 
            function(err) {
                console.log(err)
            }
        );
    });

    map.on('movestart', function() { $('.route-selector').remove(); });

    map.scrollZoom.disable();
    map.setMaxBounds( mapBounds );
}

function initTownSideMap() {
    var lat = $('#townSideMap').data().lat;
    var lng = $('#townSideMap').data().lng;

    var el = document.createElement('div');
        el.className = 'marker town-marker';

    mapConfig.container = 'townSideMap';
    mapConfig.center = [lng,lat];
    mapConfig.zoom = 7;

    townSideMap = new mapboxgl.Map(mapConfig);
    currentMap = townSideMap;
    routeLayerBaseID = 'townRoute';

    townSideMap.addControl( new mapboxgl.AttributionControl({ compact: true }));
    townSideMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );
    townSideMap.scrollZoom.disable();
    // townSideMap.setMaxBounds( mapBounds );

    townSideMap.on('load', function () {
        var marker = new mapboxgl.Marker(el, { offset: [-25 / 2, -25 / 2] })
        marker.setLngLat([lng,lat]);
        marker.addTo(townSideMap);

        $('.route').each(function(i){
            var startLocation = [ $(this).data().startLng, $(this).data().startLat ];
            
            var el = document.createElement('div');
            el.className = 'marker toll-marker';

            var marker = new mapboxgl.Marker(el, { offset: [-25 / 2, -25 / 2] })
            marker.setLngLat( startLocation );
            marker.addTo(townSideMap);

            // el.addEventListener('click', function(e) {});

            var routeId = routeLayerBaseID + i;

            var containers = {
                routeDataContainer: $('#' + routeLayerBaseID + i),
                routeStepsContainer: $('#' + routeLayerBaseID + 'Steps' + i)
            }
        
            fetchRouteData(startLocation, routeId, containers);
        });
    });
}

function initArgumentSideMap() {
    var lat = $('#argumentSideMap').data().lat;
    var lng = $('#argumentSideMap').data().lng;
    var color = $('#argumentSideMap').data().color;

    var el = document.createElement('div');
    el.className = 'marker argument-marker';
    var elInner = document.createElement('div');
        elInner.className = 'marker-inner';

    if(color)
        elInner.style.backgroundColor = color;
    else
        elInner.style.backgroundColor = '#ffffff';

    el.append( elInner );

    mapConfig.container = 'argumentSideMap';
    mapConfig.center = [lng,lat];
    mapConfig.zoom = 7;

    argumentSideMap = new mapboxgl.Map(mapConfig);
    currentMap = argumentSideMap;
    routeLayerBaseID = 'argumentRoute';

    argumentSideMap.addControl( new mapboxgl.AttributionControl({ compact: true }));
    argumentSideMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );
    argumentSideMap.scrollZoom.disable();
    // argumentSideMap.setMaxBounds( mapBounds );

    argumentSideMap.on('load', function () {
        var marker = new mapboxgl.Marker(el, { offset: [-25 / 2, -25 / 2] })
        marker.setLngLat([lng,lat]);
        marker.addTo(argumentSideMap);

        $('.route').each(function(i){
            var startLocation = [ $(this).data().startLng, $(this).data().startLat ];
            
            var el = document.createElement('div');
            el.className = 'marker toll-marker';

            var marker = new mapboxgl.Marker(el, { offset: [-25 / 2, -25 / 2] })
            marker.setLngLat( startLocation );
            marker.addTo(argumentSideMap);

            // el.addEventListener('click', function(e) {});

            var routeId = routeLayerBaseID + i;

            var containers = {
                routeDataContainer: $('#' + routeLayerBaseID + i),
                routeStepsContainer: $('#' + routeLayerBaseID + 'Steps' + i)
            }
        
            fetchRouteData(startLocation, routeId, containers);
        });
    });
}

function initRouteEditorMap() {
    mapConfig.container = 'routeEditorMap';
    mapConfig.zoom = 8;

    routeEditorMap = new mapboxgl.Map(mapConfig);

    routeEditorMap.addControl( new mapboxgl.AttributionControl({ compact: true }));

    routeEditorMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );

    routeEditorMap.scrollZoom.disable();
}

function initCategoryMap() {
    mapConfig.container = 'categoryMap';
    mapConfig.zoom = 8.5;
    categoryMap = new mapboxgl.Map(mapConfig);
    currentMap = categoryMap;

    categoryMap.addControl( new mapboxgl.AttributionControl({ compact: true }));
    categoryMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );

    categoryMap.on('load', function() { addMarkersToCategoryMap(); });

    categoryMap.scrollZoom.disable();

    categoryMap.setMaxBounds( mapBounds );
}

function initRoutePageMap() {
    mapConfig.container = 'routePageMap';
    mapConfig.zoom = 9;
    routePageMap = new mapboxgl.Map(mapConfig);
    currentMap = routePageMap;
    routeLayerBaseID = 'routeInfoRoute';

    routePageMap.addControl( new mapboxgl.AttributionControl({ compact: true }));
    routePageMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );

    routePageMap.on('load', function() { addMarkersToRoutePageMap(); });

    routePageMap.scrollZoom.disable();

    // routePageMap.setMaxBounds( mapBounds );
}

function initFeaturedRoutesMaps() {
    $('.featured-route-map').each(function(i){
        mapConfig.container = $(this).attr('id');
        mapConfig.zoom = 9;

        var featuredRouteMap = new mapboxgl.Map(mapConfig);

        featuredRouteMap.addControl( new mapboxgl.AttributionControl({ compact: true }));
        featuredRouteMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );

        featuredRouteMap.on('load', function(e) { addfeaturedRouteMarkers( e.target ) });

        featuredRouteMap.scrollZoom.disable();

        featuredRouteMap.setMaxBounds( mapBounds );

        featuredRoutesMaps.push( featuredRouteMap );
    });
}