// Route Editor

function initDefaultRoute() {
    var routes = store.get('oltrepoRoutes');

    if(routes)
    {
        console.log('routes present, skipping', routes);
        if (routes.length == 1) {
            if(routes[0].default)
                $('#routeEditorDeleteRoute').addClass('disabled');
        }
    }
    else
    {
        console.log('no routes', routes);

        routes = [{
            name: yourRouteText,
            description: '',
            selected: true,
            default: true,
            wayPoints: []
        }]

        store.set('oltrepoRoutes', routes);

        $('input[name="routeName"]').val( yourRouteText + ' (default)' );
        $('#routeEditorListSelectRoute').append('<li><a class="route-editor-switch-route-link" href="#">' + yourRouteText + '</a></li>');
        $('#routeEditorDeleteRoute').addClass('disabled');

        console.log('nuovo default creato', routes);
    }

    for(var i in routes) {
        if( routes[i].selected )
            $('#routeStepsCounter').addClass('visible').text( routes[i].wayPoints.length );
    }
}

function initRouteEditor() {
    currentMap = routeEditorMap;

    $('#routeEditorListSelectRoute').html('');

    var routes = store.get('oltrepoRoutes');
    
    if(routes)
    {
        for(var i in routes)
        {
            var routeName = routes[i].name;

            if(routes[i].selected) {
                
                initWaypointsList(i);

                if( routes[i].default )
                {
                    routeName = routes[i].name + ' (default)' ;

                    $('#routeEditorDeleteRoute')
                        .addClass('disabled')
                        .tooltip({
                            title: cantDeleteTooltip
                        });
                }

                $('input[name="routeName"]').val( routeName );
            }

            $('#routeEditorListSelectRoute').append('<li><a class="route-editor-switch-route-link" href="#">' + routeName + '</a></li>');
        }

        $('#routeEditorListNoRoutes').addClass('hidden');
        $('#routeEditorListContainer, #routeEditorListAddStep a').removeClass('hidden');
    }
    else
    {
        // showNoRoutes();
    }
}

function switchActiveRoute(index) {
    var routes = store.get('oltrepoRoutes');

    for(var i in routes)
        routes[i].selected = false;

    routes[index].selected = true;

    if( routes[index].default )
    {
        $('#routeEditorDeleteRoute')
            .addClass('disabled')
            .tooltip({
                title: cantDeleteTooltip
            });
    }
    else
    {
        $('#routeEditorDeleteRoute')
            .removeClass('disabled')
            .tooltip('destroy');
    }


    $('input[name="routeName"]').val( routes[ index ].name );
    if( routes[ index ].default )
    {
        if($('input[name="routeName"]').val().indexOf('(default)') < 0)
            $('input[name="routeName"]').val( routes[ index ].name + ' (default)' );
    }

    store.set('oltrepoRoutes', routes);

    initWaypointsList(index);
}

function initWaypointsList(index) {
    var route = store.get('oltrepoRoutes')[index];
    $('#routeEditorSteps').html('');
    $('#routeEditorMap').find('.marker').remove();
    $('#routeStepsCounter').text( route.wayPoints.length );

    var counter = 1;

    for( var i in route.wayPoints )
    {
        var html = '';
        html += '<li class="route-editor-step">';
            html += '<span class="route-editor-step-tool route-editor-step-number">' + counter + '</span>';
            html += '<span class="route-editor-step-tool reorder-route-editor-step"></span>';
            html += '<span class="route-editor-step-info">';
                
                var argumentName = route.wayPoints[i].name[currentLanguage];
                var argumentSlug = route.wayPoints[i].slug[currentLanguage];

                html += '<a class="route-editor-argument-link" href="/' + currentLanguage + '/' + topicBasePath + '/' + argumentSlug + '">' + argumentName + '</a>';
                html += '<br>';
                for( var j in route.wayPoints[i].categories )
                {
                    var catName, catSlug, color;
                    currentLanguage == 'it' ? catName = route.wayPoints[i].categories[j].name : catName = route.wayPoints[i].categories[j].nameEN;
                    currentLanguage == 'it' ? catSlug = route.wayPoints[i].categories[j].key : catSlug = route.wayPoints[i].categories[j].slugEN;
                    color = route.wayPoints[i].categories[j].color;
                    html += '<a class="route-editor-category-link" style="color: ' + color + ';" href="/' + currentLanguage + '/'  + topicsBasePath + '/' + catSlug + '">';
                        html += catName;
                    html += '</a>'
                }
            html += '</span>';
            html += '<span class="route-editor-step-tool remove-route-editor-step"></span>';
        html += '</li>';

        $('#routeEditorSteps').append( html );

        var el = document.createElement('div');
        
        var elInner = document.createElement('div');
        elInner.className = 'marker-inner';
        elInner.innerHTML = counter;

        if(route.wayPoints[i].categories[0])
        {
            elInner.style.backgroundColor = route.wayPoints[i].categories[0].color;
            
            var categoryClass;
            currentLanguage == 'it' ? categoryClass = route.wayPoints[i].categories[0].key : categoryClass = route.wayPoints[i].categories[0].slugEN;
        }
        else
        {
            elInner.style.backgroundColor = '#ffffff';
            
            var categoryClass;
            currentLanguage == 'it' ? categoryClass = 'senza-categoria' : categoryClass = 'uncategorized';
        }

        el.className = 'marker argument-marker ' + categoryClass;
        el.append( elInner );

        var marker = new mapboxgl.Marker(el, { offset: [-12.5 / 2, -12.5 / 2] });
        marker.setLngLat([ route.wayPoints[i].lng, route.wayPoints[i].lat ]);
        marker.addTo(routeEditorMap);

        counter++;
    }

    if( route.wayPoints.length <= 1 )
    {
        $('#routeEditorMapInfo').fadeOut();

        var layers = currentMap.getStyle().layers;
        for(var i in layers) {
            if(layers[i].id.indexOf('Custom') > -1)
            {
                currentMap.removeLayer( layers[i].id );
                currentMap.removeSource( layers[i].source );
            }
        }

        if( route.wayPoints.length == 0 )
            flyToConfig.center = mapConfig.center;

        if( route.wayPoints.length == 1 )
            flyToConfig.center = [ route.wayPoints[0].lng, route.wayPoints[0].lat ];

        flyToConfig.zoom = mapConfig.zoom;
        routeEditorMap.flyTo( flyToConfig );
    }
    else
    {
        for(var z in currentMap.getStyle().layers)
        {
            if( currentMap.getStyle().layers[z].id.indexOf('Custom') > -1 )
                currentMap.setLayoutProperty(currentMap.getStyle().layers[z].id, 'visibility', 'none');
        }

        fetchEditorRoute(route.wayPoints, 'routeEditorCustomRoute' + index);

        var gmapEndpoint = 'https://www.google.com/maps/dir/?api=1';
            gmapEndpoint += '&origin=' + route.wayPoints[0].lat + ',' + route.wayPoints[0].lng;
            gmapEndpoint += '&destination=' + route.wayPoints[ route.wayPoints.length - 1 ].lat + ',' + route.wayPoints[ route.wayPoints.length - 1 ].lng;
            gmapEndpoint += '&travelmode=driving';
            gmapEndpoint += '&waypoints=';

            for(var h in route.wayPoints)
            {
                gmapEndpoint += route.wayPoints[h].lat + ',' + route.wayPoints[h].lng + '|';
            }

        $('#routeEditorMapInfo').find('a')
            .attr('href', gmapEndpoint)
            .attr('target', 'blank');

        $('.route-editor-personal-route-start').text( route.wayPoints[0].name[currentLanguage] );

        $('#routeEditorMapInfo').fadeIn();

        // if( !routeEditorMap.getLayer('routeEditorCustomRoute' + index) )
        // {
        //     fetchEditorRoute(route.wayPoints, 'routeEditorCustomRoute' + index);
        // }
        // else
        // {
        //     currentMap.setLayoutProperty('routeEditorCustomRoute' + index, 'visibility', 'visible');

        //     var coordinates = currentMap.getSource('routeEditorCustomRoute' + index)._data.geometry.coordinates;

        //     var bounds = coordinates.reduce(function(bounds, coord) {
        //         return bounds.extend(coord);
        //     }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

        //     currentMap.fitBounds(bounds, {
        //         padding: 20
        //     });
        // }
    }
}

function saveRouteName() {
    var routes = store.get('oltrepoRoutes');

    // var currentRouteIndex = 0;
    for(var i in routes)
    {
        if( routes[i].selected ) {
            routes[i].name = $('input[name="routeName"]').val().replace(' (default)', '');
            // currentRouteIndex = i;
        }
    }

    store.set('oltrepoRoutes', routes);
    initRouteEditor();
    // initWaypointsList( currentRouteIndex );
}

function createEmptyRoute() {
    var newRouteName = $('input[name="routeEditorNewRoute"]').val();

    if(newRouteName != '')
    {
        var routes = store.get('oltrepoRoutes');
        console.log(routes);

        var emptyRoute = {
            name: newRouteName,
            slug: 'test',
            description: '',
            selected: true,
            wayPoints: []
        }

        routes.push( emptyRoute );
        console.log(routes);

        store.set('oltrepoRoutes', routes);
        // switchActiveRoute( routes.length - 1 );
        initRouteEditor();

        $('#routeEditorNewRouteInputContainer').hide();
        $('#routeEditorCreateNewRoute').show();
    }
    else
    {
        console.log('no name no route');
    }
}

function removeWaypointFromRoute(index) {
    var routes = store.get('oltrepoRoutes');
    for(var i = 0; i < routes.length; i++)
    {
        if( routes[i].selected )
        {
            routes[i].wayPoints.splice(index, 1);
        }
    }

    store.set( 'oltrepoRoutes', routes);

    initRouteEditor();
}

function deleteActiveRoute() {
    var routes = store.get('oltrepoRoutes');
    
    var delIndex, nextIndex;
    for(var i = 0; i < routes.length; i++)
    {
        if( routes[i].selected )
            delIndex = i;
    }

    routes.splice(delIndex, 1);

    if(routes.length > 0)
    {
        store.set('oltrepoRoutes', routes);

        initRouteEditor();

        routes[0].selected = true;
        switchActiveRoute(0);
    }
    else
    {
        store.remove('oltrepoRoutes');

        $('#routeEditorMap').find('.marker').remove();

        var layers = routeEditorMap.getStyle().layers;
        for(var i in layers) {
            if(layers[i].id.indexOf('Custom') > -1)
            {
                routeEditorMap.removeLayer( layers[i].id );
                routeEditorMap.removeSource( layers[i].source );
            }
        }

        flyToConfig.center = mapConfig.center;
        flyToConfig.zoom = mapConfig.zoom;
        routeEditorMap.flyTo( flyToConfig );

        showNoRoutes();
    }
}

function hideNoRoutes() {
    $('#routeEditorListNoRoutes').addClass('hidden');
    $('#routeEditorListContainer, #routeEditorListAddStep a').removeClass('hidden');
}

function showNoRoutes() {
    $('input[name="routeEditorNewRoute"], a.route-editor-submit-new-route-name').remove();
    $('#routeEditorListNoRoutes').removeClass('hidden');
    $('#routeEditorCreateNewRoute').removeClass('disabled');
    $('#routeEditorListContainer, #routeEditorListAddStep a').addClass('hidden');
}

function initDraggables() {
    var el = document.getElementById( 'routeEditorSteps' );
    var sortable = Sortable.create(el, {
        handle: '.reorder-route-editor-step',
        onEnd: function(e){
            var routes = store.get( 'oltrepoRoutes' );

            if( routes )
            {
                for(var i in routes)
                {
                    if(routes[i].selected)
                    {
                        arraymove( routes[i].wayPoints, e.oldIndex, e.newIndex );
                        store.set( 'oltrepoRoutes', routes );
                        initWaypointsList(i);
                    }
                }
            }
        }
    });
}

function saveRouteInApp(token) {
    // console.log( token );
    $('#saveInAppModalPreloader').show();
    $('#routeEditorFeedbackContainer').addClass('hidden');

    console.log( 'send post request with route data and recaptcha token' );

    var data = { token: token }

    var routes = store.get('oltrepoRoutes');

    for(var i in routes)
    {
        if( routes[i].selected )
        {
            data.route = routes[i];
        }
    }
    
    $.ajax({
        method: "POST",
        url: "/api/new-route",
        data: data
    })
    .done(function( data ) {
        console.log( "Data Saved" );

        var routePath = '/itinerario/id/';
        if (currentLanguage == 'en')
            routePath = '/route/id/';

        var shareUrl = window.location.protocol + '//' + window.location.host + '/' + currentLanguage + routePath + data._id ;

        $('#saveInAppModalPreloader').hide();
        $('#getRouteCode').attr('disabled', 'disabled');
        grecaptcha.reset();

        initSaveInAppInfo( data );

        $('.save-in-app-social-icon').attr('data-url', shareUrl);

        $('#saveInAppModalDownloadPdf').attr('data-routeurl', '/' + currentLanguage + routePath + data._id);
        $('#saveInAppModalDownloadPdf').attr('href', '/pdf/' + data._id + '.pdf');

        $('#saveInAppModalViewRoutePage').attr('href', shareUrl).prop("disabled", false).removeAttr('disabled');

        prebuildPDF();

        $('.save-in-app-modal-col#form').hide();
        $('.save-in-app-modal-col#info').fadeIn();
    });
}

function saveInAppReCaptchaCallBack(msg) { 
    // console.log( msg );
    if( $('#getRouteCode').length > 0 )
        $('#getRouteCode').remove();
    
    var html = '';
    html += '<button type="submit" class="btn btn-default" id="getRouteCode">';
        html += getRouteCode;
    html += '</button>';

    $('#form').find('form').find('.form-group').after( html );
}

function initSaveInAppInfo( data ) {
    $('#saveInAppModalSendByEmail').attr('data-routeID', data._id);
    $('#routeNameHeading').text( data.name );
    $('#saveInAppModalRouteID').text( data.appCode );
}

// function initRouteEditorModalSocialShare() {
//     $('.save-in-app-social-icon').share();
// }

function prebuildPDF() {
    console.log( 'prebuildPDF' );
    $.get( $('#saveInAppModalDownloadPdf').data().routeurl, function( data ) {
        if(data)
            $('#saveInAppModalDownloadPdf').removeAttr('disabled');
    });
}