// Main map Markers

function addTowns(d) {

    d.forEach(function(town){
        var el = document.createElement('div');
        el.className = 'marker town-marker';

        var DOMel = $('<div>')
            .addClass('popup-container town-popup-container');

        var head = $('<div>')
            .addClass('popup-header');

        var img;
        if(town.coverImg.src)
        {
            var pid = town.coverImg.src.public_id;
            img = $.cloudinary.image(pid, { width: 300, height: 190, crop: 'fill' });
        }
        else
        {
            img = $('<img>')
                .attr('src', 'images/defaultPopupImg.svg')
                .attr('width', '300')
                .attr('height', '190');
        }

        img.addClass('img-responsive');
        head.append( img );
        head.append('<div class="popup-img-overlay"></div>');

        head.append('<h5><a href="/' + currentLanguage + '/' + townBasePath + '/' + town.key + '">' + town.name + '</a></h5>');

        DOMel.append( head );

        if(town.arguments)
        {
            var argumentList = $('<ul>')
            town.arguments.forEach(function(argument){
                var argumentElement = $('<li>')
                var argumentLink = $('<a>')
                    .attr('href', '/' + currentLanguage + '/' + topicBasePath + '/' + getArgumentSlug(argument))
                    .addClass('argument-link')
                    .text( getArgumentName(argument) );
                argumentElement.append( argumentLink );

                var categoryList = $('<span>')
                    .addClass('category-link-list');

                argument.categories.forEach(function(id){
                    var category = $('.category-link[data-id="' + id._id + '"]');
                    var catCol = category.data().color || '#ffffff';
                    var categoryLink = $('<a>')
                        .attr('href', topicsBasePath + '/' + category.data().slug)
                        .addClass('popup-category-link')
                        .css({ color: catCol })
                        .text( category.text() );
                    categoryList.append( categoryLink );
                })
                argumentElement.append( categoryList );

                argumentElement.append( '<a class="add-to-route-link" href="#" title="' + addToRouteText + '"></div>' );

                argumentElement.append( '<div style="clear: both;"></div>' );

                argumentList.append( argumentElement );
            });

            DOMel.append( argumentList );
        }

        var popup = new mapboxgl.Popup( { offset: 15 } );
        popup.setDOMContent( DOMel[0] );

        popup.on('close', function(e){
            $('.route-selector').remove();
        });

        if(town.location)
        {
            var marker = new mapboxgl.Marker(el, { offset: [-25 / 2, -40 / 2] })
            marker.setLngLat(town.location.location.geo);
            marker.setPopup( popup );
            marker.addTo(map);
        }
    });
}

function addArgumentMarkers(d) {
    d.forEach(function(arg){

        var el = document.createElement('div');
        var elInner = document.createElement('div');
        elInner.className = 'marker-inner';
        
        if(arg.categories[0])
        {
            elInner.style.backgroundColor = arg.categories[0].color;
            
            var categoryClass;
            currentLanguage == 'it' ? categoryClass = arg.categories[0].key : categoryClass = arg.categories[0].slugEN;
        }
        else
        {
            elInner.style.backgroundColor = '#ffffff';
            
            var categoryClass;
            currentLanguage == 'it' ? categoryClass = 'senza-categoria' : categoryClass = 'uncategorized';
        }

        el.append( elInner );
        el.className = 'marker argument-marker ' + categoryClass;

        var DOMel = $('<div>')
            .addClass('popup-container argument-popup-container');

        var head = $('<div>')
            .addClass('popup-header');

        if(arg.coverImg)
        {
            var img;
            if(arg.coverImg.src)
            {
                var pid = arg.coverImg.src.public_id;
                img = $.cloudinary.image(pid, { width: 300, height: 190, crop: 'fill' });
            }
            else
            {
                img = $('<img>')
                    .attr('src', '/images/defaultPopupImg.svg')
                    .attr('width', '300')
                    .attr('height', '190');
            }

            img.addClass('img-responsive');
            head.append( img );
            head.append('<div class="popup-img-overlay"></div>');
        }

        head.append('<h5><a href="/' + currentLanguage + '/' + topicBasePath + '/' + getArgumentSlug(arg) + '">' + getArgumentName(arg) + '</a></h5>');

        var categoryList = $('<div>')
            .addClass('category-link-list');

        arg.categories.forEach(function(cat){
            var slug;
            currentLanguage == 'it' ? slug = cat.key : slug = cat.slugEN;
            var categoryLink = $('<a>')
                .attr('href', topicsBasePath + '/' + slug)
                .addClass('popup-category-link')
                .css({ color: cat.color })
                .text( getCategoryName(cat) );

            categoryList.append(categoryLink);
        });

        head.append(categoryList);

        DOMel.append( head );

        var addToRoute = $('<div>')
            .addClass('add-to-route-argument-popup-link')

        var addToRouteLink = $('<a>')
            .addClass('add-to-route-link')
            .attr('href', '#')
            .attr('data-id', arg._id)
            .text( addToRouteText );

        if(arg.location && arg.location.location.geo)
        {
            addToRouteLink
                .attr('data-lng', arg.location.location.geo[0])
                .attr('data-lat', arg.location.location.geo[1])
        }

        addToRoute.append( addToRouteLink );

        DOMel.append(addToRoute);

        var popup = new mapboxgl.Popup( { offset: 15 } );
        popup.setDOMContent( DOMel[0] );

        popup.on('close', function(e){
            $('.route-selector').remove();
        });

        if(arg.location)
        {
            if(arg.location.location.geo)
            {
                var marker = new mapboxgl.Marker(el, { offset: [-15, -15] });
                marker.setLngLat(arg.location.location.geo);
                marker.setPopup( popup );
                marker.addTo(map);
            }
        }
    })
}

// Main map Filters

function filterMapMarkers() {
    $('.category-filter-container').each(function(i){
        var slug = $(this).data().slug;
        if( $(this).find('.category-filter-bullet-checked').hasClass('unchecked') )
        {
            $('.argument-marker.' + slug).addClass('faded');
        }
        else
        {
            $('.argument-marker.' + slug).removeClass('faded');
        }
    });
}