WebFont.load({
    google: {
        families: ['Montserrat:400,500,600,700,800', 'PT Serif:400,700']
    }
});

$.cloudinary.config({ cloud_name: 'itinerarioltrepomantovano'});

mapboxgl.accessToken = 'pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6IjNLLVM5a0UifQ.fQiOn9nvARyZ80kya3yfjA';
// mapboxgl.accessToken = 'pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6ImNqNXJmNWs4ejByZXMycG16dXRreW54eGoifQ.FrVMLoCLXY17aZaNSWcIXQ';

var currentLanguage, townBasePath, topicBasePath, topicsBasePath;
var confirmDeleteRoute;
var closeText, yourRouteText, addToRouteText, newRouteText, newRouteDefaultName, noSavedRoutesText;
var feedbackEmailWarningNoEmail, feedbackEmailSuccessMailSent, feedbackEmailError;
var didacticsFilterByCategory, didacticsFilterByTown, didacticsRemoveCategoryFilter, didacticsRemoveTownFilter;
var getRouteCode;

var mapConfig = {
    style: 'mapbox://styles/maccaluso/cj5wksxrn7jau2rmkmryvi5co',
    center: [11.07486879208193,45.00679160929593],
    zoom: 10,
    attributionControl: false
};
var flyToConfig = {
    bearing: 0,
    speed: 3.5,
    curve: 1,
    easing: function (t) {
        return t<.5 ? 2*t*t : -1+(4-2*t)*t;
    }
};
var mapBounds = new mapboxgl.LngLatBounds(
  new mapboxgl.LngLat( 10.40489574463723, 44.72322879639489 ),
  new mapboxgl.LngLat( 11.659509644121414, 45.29442275231085 )
);
var popups = [];

var map, routeEditorMap, townSideMap, argumentSideMap, categoryMap, routePageMap, currentMap;
var featuredRoutesMaps = [];
var routeLayerBaseID;

var filtered;

var newWaypoint;

$(document).ready(function(){
    currentLanguage = $('header').data().lang;

    switch (currentLanguage)
    {
        case 'it':
            townBasePath = 'comune';
            topicBasePath = 'argomento';
            topicsBasePath = 'argomenti';
            closeText = 'CHIUDI';
            confirmDeleteRoute = 'Sei sicuro di voler eliminare questo itinerario?';
            cantDeleteTooltip = 'Non puoi eliminare l\'itinerario di default';
            yourRouteText = 'Il tuo itinerario';
            addToRouteText = 'Aggiungi all\'itinerario';
            newRouteText = 'Crea nuovo itinerario';
            newRouteDefaultName = 'Nuovo itinerario';
            noSavedRoutesText = 'Nessun itinerario salvato';
            feedbackEmailWarningNoEmail = 'Devi inserire un indirizzo email valido';
            feedbackEmailSuccessMailSent = 'La email è stata inviata correttamente';
            feedbackEmailError = 'Ops! Qualcosa è andato storto...';
            didacticsFilterByCategory = '-- Filtra per categoria --';
            didacticsFilterByTown = '-- Filtra per comune --';
            didacticsRemoveCategoryFilter = '-- Tutte le categorie --';
            didacticsRemoveTownFilter = '-- Tutti i comuni --';
            getRouteCode = 'Ottieni il codice dell\'itinerario attivo';
            break;
        case 'en':
            townBasePath = 'town';
            topicBasePath = 'topic';
            topicsBasePath = 'topics';
            confirmDeleteRoute = 'Are you sure you want to delete this route?';
            cantDeleteTooltip = 'You can not delete the default route';
            closeText = 'CLOSE';
            yourRouteText = 'Your route';
            addToRouteText = 'Add to route';
            newRouteText = 'Create new route';
            newRouteDefaultName = 'New route';
            noSavedRoutesText = 'No saved routes';
            feedbackEmailWarningNoEmail = 'Please insert a valid email address';
            feedbackEmailSuccessMailSent = 'Email successfully sent';
            feedbackEmailError = 'Ops! Something went wrong...';
            didacticsFilterByCategory = '-- Filter by category --';
            didacticsFilterByTown = '-- Filter by town --';
            didacticsRemoveCategoryFilter = '-- All categories --';
            didacticsRemoveTownFilter = '-- All towns --';
            getRouteCode = 'Get active route code';
            break;
    }

    initDefaultRoute();
    // initRouteEditorModalSocialShare();
    $('.save-in-app-social-icon').share();

    if( $('#map').length > 0 ) { initMap(); }
    if( $('#townSideMap').length > 0 ) { initTownSideMap(); }
    if( $('#argumentSideMap').length > 0 ) { initArgumentSideMap(); }
    if( $('#routeEditorMap').length > 0 ) { initRouteEditorMap(); }
    if( $('#categoryMap').length > 0 ) { initCategoryMap(); }
    if( $('#routePageMap').length > 0 ) { 
        initRoutePageMap(); 
        initRoutePageSocialShare();
    }
    if( $('.featured-route-map').length > 0 ) { 
        initFeaturedRoutesMaps();
        initSocialShare();
    }
    if( $('#saveInAppModal').length > 0 )
    {
        $('.save-in-app-social-icon').share();
    }

    // UI Adjustments calls
    if( $('.featured-card').length > 0 ) { initHomeFeaturedSlider(); }
    if( $('.video-gallery-card').length > 0 ) { initHomeVideoGallery(); }
    if( $('#townDescription').length > 0 ) { initTownDescription(); }
    if( $('.town-argument-info').length > 0 ) { initTownArgumentInfo(); }
    if( $('.town-argument-category-link').length > 0 ) { 
        $('.town-argument-info').each(function(i){
            $(this).find('.town-argument-category-link').last().addClass('last');
        });
    }
    if( $('.town-related-card').length > 0 ) { initTownRelatedSlider(); }
    if( $('.argument-featured-card').length > 0 ) { initArgumentFeaturedSlider(); }
    if( $('.category-argument-card').length > 0 ) { 
        if ( $('#categoryArguments').hasClass('filtered') )
            filterCategoryCards( $('#categoryArguments').data().filter );
        else
            $('.category-argument-card').show();
    }
    if( $('#routeEditorSteps').length > 0 )
        initDraggables();
    if( $('.didactics-argument-contribs-container').length > 0 ) { initContribsContainers() };

    // UI Events listeners
    $('body').on('mouseenter', '.category-link', function(e){
        e.preventDefault();
        $(this).css({
            backgroundColor: $(this).data().color
        });
    });
    $('body').on('mouseleave', '.category-link', function(e){
        e.preventDefault();
        $(this).css({
            backgroundColor: 'transparent'
        });
    });

    $('body').on('click', '#routeEditorToggle a', function(e){
        e.preventDefault();
        $('#routeEditor, header').toggleClass('expanded');

        if( $('#routeEditor, header').hasClass('expanded') )
        {
            $('#routeEditorToggleText').text( closeText );
            $('#logo').addClass('mobile-logo-expanded-route-editor');
            initRouteEditor();
        }
        else
        {
            $('#routeEditorToggleText').text( yourRouteText );
            $('#logo').removeClass('mobile-logo-expanded-route-editor');

            if( $('#townSideMap').length > 0 ) { currentMap = townSideMap; }
            if( $('#argumentSideMap').length > 0 ) { currentMap = argumentSideMap; }
            if( $('#categoryMap').length > 0 ) { currentMap = categoryMap; }
        }
    });
    $('body').on('mouseleave', '#routeEditorToggle a', function(e){
        e.preventDefault();

        $('#routeEditorToggle a').blur();
    });

    $('body').on('click', '#routeEditorCloseIcon', function(e){
        $('#routeEditor, header').removeClass('expanded');
        $('#routeEditorToggleText').text( yourRouteText );
        $('#routeEditorSteps').html('');

        if( $('#townSideMap').length > 0 ) { currentMap = townSideMap; }
        if( $('#argumentSideMap').length > 0 ) { currentMap = argumentSideMap; }
        if( $('#categoryMap').length > 0 ) { currentMap = categoryMap; }
    });

    $('body').on('click', '#mobileNavToggle', function(e) {
        $('#categoryLinks').addClass('expanded');
    });
    $('body').on('click', '#closeMobileNav', function(e) {
        $('#categoryLinks').removeClass('expanded');
    });

    $('body').on('click', '.slider-nav-control', sliderNavigation);
    $('#featuredSlider').on('transitionend', function(e){
        if(e.originalEvent.propertyName == 'left')
            $('body').on('click', '.slider-nav-control', sliderNavigation);
    });

    $('body').on('click', '.video-gallery-nav-control', videoGalleryNavigation);
    $('#videoGallerySlider').on('transitionend', function(e){
        if(e.originalEvent.propertyName == 'left')
            $('body').on('click', '.video-gallery-nav-control', videoGalleryNavigation);
    });

    $('body').on('click', '#argumentTownListToggle', function(e){
        e.preventDefault();
        $('#argumentTownList').toggleClass('visible');
    });
    $('body').on('mouseleave', '#argumentTownList', function(e){ $('#argumentTownList').removeClass('visible'); });

    $('body').on('click', '.argument-featured-nav', argumentSliderNavigation);
    $('#argumentFeaturedSlider').on('transitionend', function(e){
        if(e.originalEvent.propertyName == 'left')
            $('body').on('click', '.argument-featured-nav', argumentSliderNavigation);
    });

    // App Info modal UI Events listeners
    $('body').on('click', '.app-info', function(e){
        e.preventDefault();
        $('#appInfoModal').addClass('on');
    });
    $('body').on('click', '#appInfoModal', function(e){
        if (e.target == this)
        {
            e.preventDefault();
            $('#appInfoModal').removeClass('on');
        }
    });
    $('body').on('click', '#appInfoModalClose', function(e){
        e.preventDefault();
        $('#appInfoModal').removeClass('on');
    });

    // Save in App modal UI Events listeners
    $('body').on('click', '#saveInAppModal', function(e){
        if (e.target == this)
        {
            e.preventDefault();
            $('#saveInAppModal').removeClass('on');

            if( $('#getRouteCode').length > 0 )
                $('#getRouteCode').remove();
        }
    });
    $('body').on('click', '#saveInAppModalClose', function(e){
        e.preventDefault();
        $('#saveInAppModal').removeClass('on');

        if( $('#getRouteCode').length > 0 )
            $('#getRouteCode').remove();
    });

    // Homepage filters UI Events
    $('body').on('click', '.category-filter-link', function(e){
        e.preventDefault();
        $(this).find('.category-filter-bullet-checked').toggleClass('unchecked');
        if( $('.category-filter-bullet-checked.unchecked').length == 5 )
        {
            $('#hideTopicsBtn').addClass('hidden');
            $('#clearFiltersBtn').removeClass('hidden');
        }
        if( $('.category-filter-bullet-checked.unchecked').length == 0 )
        {
            $('#hideTopicsBtn').removeClass('hidden');
            $('#clearFiltersBtn').addClass('hidden');
        }
        filterMapMarkers();
    });

    $('body').on('click', '#hideTopicsBtn', function(e){
        e.preventDefault();
        $('.category-filter-bullet-checked').addClass('unchecked');
        filterMapMarkers();
        $(this).addClass('hidden');
        $(this).next().removeClass('hidden');
    });

    $('body').on('click', '#clearFiltersBtn', function(e){
        e.preventDefault();
        $('.category-filter-bullet-checked').removeClass('unchecked');
        filterMapMarkers();
        $(this).addClass('hidden');
        $(this).prev().removeClass('hidden');
    });

    // Homepage featured UI events
    $('body').on('click', '.featured-card-town-list-toggle', function(e){
        e.preventDefault();
        var list = $(this).next()
        list.css({
            left: $(this).position().left + 30
        });
        $(this).next().toggleClass('visible');
    });
    $('body').on('mouseleave', '.featured-card-town-list', function(e){ $(this).removeClass('visible'); });

    // Town related slider
    $('body').on('click', '.town-related-nav', townRelatedSliderNavigation);
    $('#townRelatedSlider').on('transitionend', function(e){
        if(e.originalEvent.propertyName == 'left')
            $('body').on('click', '.town-related-nav', townRelatedSliderNavigation);
    });

    // Category filters UI Events
    $('#filterByCategorySelect').on('change', function(e){
        e.preventDefault();
        filterByCategory( $(this).val() );
    });
    $('#mainFilterInput').on('keyup', function(e){
        e.preventDefault();
        var searchString = $(this).val();

        filtered = {};

        var selectorString;
        $('.category-argument-card.filter-match').length > 0 ? selectorString = '.category-argument-card.filter-match' : selectorString = '.category-argument-card';

        if(searchString.length > 2)
        {
            $(selectorString).each(function(i){
                var id = $(this).data().id;

                var re = new RegExp(searchString, 'gi');

                var text = $(this).find('h5 a').text() + '. ' + $(this).find('p').text();
                text = text.replace(/['.,\/#!$%\^&\*;:{}=\-_`~()]/g, '').toLowerCase();

                if( text.search( re ) > -1 )
                {
                    var startPos = text.search( re );
                    var nextSpace = text.indexOf(' ', startPos);

                    if ( text[startPos-1] != ' ' && text[startPos-1] != "'" )
                        startPos = text.lastIndexOf( ' ', startPos ) + 1;

                    var suggestion;
                    if(nextSpace > -1)
                        suggestion = text.substring( startPos, nextSpace );
                    else
                        suggestion = text.substring( startPos );

                    var canUpdateSuggestion = true;
                    $('.filter-tag-link').each(function(i){
                        if( $(this).text() == suggestion )
                            canUpdateSuggestion = false;
                    });

                    if(canUpdateSuggestion)
                        updateSuggestions( suggestion );

                    if(!filtered[suggestion])
                        filtered[suggestion] = new Array();

                    filtered[suggestion].push( id );
                }
                else
                {
                    removeSuggestions( searchString );
                }
            });
        }

        if(searchString.length == 0)
        {
            $(this).val('');
            clearSuggestions();
        }
    });
    $('body').on('click', '.suggestion-link, .didactics-suggestion-link', function(e){
        e.preventDefault();
        addFilterTag( $(this).text() );
        filterCategoryCardsByTag( $(this).text() );
        clearSuggestions();
    });
    $('body').on('click', '.filter-tag-link', function(e){
        e.preventDefault();
        removeFilterTag( $(this).text(), e.target );
    });
    $('body').on('click', '#categoryFiltersResetFilters', function(e){
        e.preventDefault();
        resetFilters();
    });

    $('body').on('mouseenter', '.filter-tag-link', function(e){
        e.preventDefault();
        $(this).next().addClass('remove-tag');
    });
    $('body').on('mouseleave', '.filter-tag-link', function(e){
        e.preventDefault();
        $(this).next().removeClass('remove-tag');
    });

    // Routing UI Events
    $('body').on('click', 'a.argument-route-fixed-start, a.town-route-fixed-start', function(e){
        e.preventDefault();
        $(this).next().toggleClass('expanded');
    });

    $('body').on('click', 'a.route-toggle-steps', function(e){
        e.preventDefault();

        $('a.route-toggle-steps').each(function(i){
            $(this).next().removeClass('expanded');

            if( currentMap.getLayer(routeLayerBaseID + i) )
            {
                currentMap.setLayoutProperty(routeLayerBaseID + i, 'visibility', 'none');
            }
        });

        if( currentMap.getLayer(routeLayerBaseID + 'Custom') )
            currentMap.setLayoutProperty(routeLayerBaseID + 'Custom', 'visibility', 'none');

        $(this).next().toggleClass('expanded');

        if( currentMap.getLayer(routeLayerBaseID + $(this).data().id) )
        {
            currentMap.setLayoutProperty(routeLayerBaseID + $(this).data().id, 'visibility', 'visible');

            var coordinates = currentMap.getSource(routeLayerBaseID + $(this).data().id)._data.geometry.coordinates;

            var bounds = coordinates.reduce(function(bounds, coord) {
                return bounds.extend(coord);
            }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

            currentMap.fitBounds(bounds, {
                padding: 20
            });
        }
    });

    $('body').on('click', 'a.route-step-link', function(e){ e.preventDefault(); });

    $('input[name="argumentRouteCustomStart"], input[name="townRouteCustomStart"], input[name="routeInfoRouteCustomStart"]').on('focus', function(e){
        $(this).val('');

        $(this).next().next().addClass('expanded');

        $('.argument-route-container, town-route-container, route-info-route-container').removeClass('expanded');
        // $('a.route-toggle-steps').each(function(i){
        //     $(this).next().removeClass('expanded');

        //     if( currentMap.getLayer(routeLayerBaseID + i) )
        //     {
        //         currentMap.setLayoutProperty(routeLayerBaseID + i, 'visibility', 'none');
        //     }
        // });

        // if( currentMap.getLayer(routeLayerBaseID + 'Custom') )
        //     currentMap.setLayoutProperty(routeLayerBaseID + 'Custom', 'visibility', 'none');

        // currentMap.flyTo({
        //     center: mapConfig.center, 
        //     zoom: mapConfig.zoom
        // });
    });

    $('input[name="argumentRouteCustomStart"], input[name="townRouteCustomStart"], input[name="routeInfoRouteCustomStart"]').on('keyup', function(e){
        if( $(this).val().length > 5 ) 
        {
            $('#argumentRouteCustomStartHints, #townRouteCustomStartHints, #routeInfoRouteCustomStartHints').show();

            $.when( 
                $.ajax({
                    url: 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + $(this).val() + '.json',
                    data: {
                        language: 'it',
                        limit: 5,
                        access_token: mapboxgl.accessToken
                    }
                })
            )
            .then( 
                function(data) {
                    console.log(routeLayerBaseID);
                    var html = '';
                    for(var i = 0; i < data.features.length; i++)
                    {
                        html += '<li><a class="geocoding-hint-link" data-index="' + i + '" href="#">' + data.features[i].place_name + '</a></li>';
                    }

                    $('#' + routeLayerBaseID + 'CustomStartHintsContainer').html( html )
                }, 
                function(err) {
                    console.log(err)
                }
            );
        }
    });

    $('body').on('click', 'a.geocoding-hint-link', function(e){
        e.preventDefault();
        var index = $(this).data().index;

        $('#' + routeLayerBaseID + 'CustomStartHints').hide();

        $.when( 
            $.ajax({
                url: 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + $('input[name="' + routeLayerBaseID + 'CustomStart"]').val() + '.json',
                data: {
                    language: 'it',
                    limit: 5,
                    access_token: mapboxgl.accessToken
                }
            })
        )
        .then( 
            function(data) {
                var lng = data.features[ index ].center[0];
                var lat = data.features[ index ].center[1];
                var startLocationName = data.features[ index ].text;
                var destLocationName = $('#argumentInfos').find('h1').text() || $('#townInfo').find('h1').text() || $('.route-step').first().find('a.route-step-link').text();

                var html = '';
                html += '<li class="route" id="' + routeLayerBaseID + 'Custom" data-start-lat="" data-start-lng="">';
                    html += '<a data-id="Custom" class="route-toggle-steps" href="#">' + startLocationName + ' > ' + destLocationName + '</a>';
                    html += '<ul class="route-steps expanded" id="' + routeLayerBaseID + 'StepsCustom"></ul>';
                html += '</li>';

                $('#argumentRouteContainerCustom, #townRouteContainerCustom, #routeInfoRouteContainerCustom').html( html );
                $('#argumentRouteContainerCustom, #townRouteContainerCustom, #routeInfoRouteContainerCustom').parent().addClass('expanded');

                var containers = {
                    routeDataContainer: $('#' + routeLayerBaseID + 'Custom'),
                    routeStepsContainer: $('#' + routeLayerBaseID + 'StepsCustom')
                }

                fetchRoutePageData(data.features[ index ].center, [$('.route-step').first().data().lng, $('.route-step').first().data().lat], routeLayerBaseID + 'Custom', containers);
            }, 
            function(err) {
                console.log(err)
            }
        );
    });

    // Personal Route UI Events
    $('body').on('click', '.add-to-route-link, .argument-add-to-route, .town-argument-add-to-route-link, .category-add-to-route-link', function(e){
        e.preventDefault();

        if( $('#routeEditor, header').hasClass('expanded') )
            $('#routeEditorToggle a').trigger('click');

        setNewWaypoint( $(this) );
        toggleRoutesSelector( e, $('body') );
    });

    $('body').on('click', '#addAllToRouteLink', function(e){
        e.preventDefault();
        toggleRoutesSelectorAll( e, $('body') );
    });

    $('body').on('click', '.route-selector-new-route', function(e){
        e.preventDefault();
        $(this).hide();
        $('.route-selector-new-route-input-container').show()
        $('input[name="routeSelectorNewRouteInput"]').focus();
    });

    $('body').on('click', '.route-selector-new-route-input-submit', function(e){ 
        e.preventDefault();
        saveNewRoute(); 
    });

    $('body').on('click', '.route-selector-link', function(e){
        e.preventDefault();
        addWaypointToRoute( $(this).parent().index() );
        $(this).addClass('added');

        var delay = setTimeout(function(){
            $('.route-selector').remove();
            clearTimeout( delay );
        }, 1000);
    });
    $('body').on('click', '.route-selector-link-all', function(e){
        e.preventDefault();
        // addWaypointToRoute( $(this).parent().index() );
        addAllToRoute( $(this).parent().index() );
        $(this).addClass('added');

        var delay = setTimeout(function(){
            $('.route-selector').remove();
            clearTimeout( delay );
        }, 1000);
    });
    $('body').on('keydown', 'input[name="routeSelectorNewRouteInput"]', function(e){
        if( e.keyCode === 13 )
            saveNewRoute();
    });

    // Route Editor UI Events
    $('body').on('click', '.route-editor-switch-route-link', function(e){
        e.preventDefault();
        switchActiveRoute( $(this).parent().index() );
    });

    $('body').on('click', '#routeEditorEditRouteName', function(e){
        e.preventDefault();
        $(this).fadeOut(100, function(){
            $('#routeEditorSaveRouteName').fadeIn();
            $('input[name="routeName"]').prop("disabled", false).focus();
        });
    });

    $('body').on('click', '#routeEditorSaveRouteName', function(e){
        e.preventDefault();
        saveRouteName();
        $(this).fadeOut(100, function(){
            $('#routeEditorEditRouteName').fadeIn();
            $('input[name="routeName"]').prop("disabled", true).blur();
        });
    });
    $('body').on('keydown', 'input[name="routeName"]', function(e){
        if( e.keyCode === 13 )
        {
            saveRouteName();
            $('#routeEditorSaveRouteName').fadeOut(100, function(){
                $('#routeEditorEditRouteName').fadeIn();
                $('input[name="routeName"]').prop("disabled", true).blur();
            });
        }
    });

    $('body').on('click', '#routeEditorDeleteRoute', function(e){
        e.preventDefault();

        var r = confirm( confirmDeleteRoute );
        if(r)
            deleteActiveRoute();
    });

    $('body').on('click', '#routeEditorCreateNewRoute', function(e){
        e.preventDefault();
        $(this).hide();
        $('#routeEditorNewRouteInputContainer').show();
        // var html = '<input type="text" name="routeEditorNewRoute">';
        // html += '<a class="btn btn-default route-editor-submit-new-route-name"><span></span></a>'
        // $(this).after( html );
        $('input[name="routeEditorNewRoute"]').val( newRouteDefaultName ).focus();
        // $(this).addClass('disabled');
    });

    $('body').on('blur', 'input[name="routeEditorNewRoute"]', function(e){
        // $('#routeEditorNewRouteInputContainer').hide();
        // $('#routeEditorCreateNewRoute').show();
        // console.log(e.target);
    });

    $('body').on('keydown', 'input[name="routeEditorNewRoute"]', function(e){
        if( e.keyCode === 13 )
            createEmptyRoute();
    });
    $('body').on('click', 'a.route-editor-submit-new-route-name', function(e){
        e.preventDefault();
        createEmptyRoute();
    });

    $('body').on('click', '#routeEditorListSaveInAppLink', function(e){
        e.preventDefault();

        $('#saveInAppModalPreloader').hide();
        $('#saveInAppModalDownloadPdf').attr('disabled', 'disabled');
        $('#getRouteCode').attr('disabled', 'disabled');
        grecaptcha.reset();

        $('.save-in-app-modal-col#form').show();
        $('.save-in-app-modal-col#info').hide();

        $('#saveInAppModal').addClass('on');
    });

    $('body').on('click', '#getRouteCode', function(e){
        e.preventDefault();
        saveRouteInApp( $('#g-recaptcha-response').val() );
    });

    $('body').on('click', '.remove-route-editor-step', function(e){ removeWaypointFromRoute( $(this).parent().index() ); });

    $('#routeEditorDestMail').on('keydown', function(e){
        if(e.keyCode == 13)
        {
            e.preventDefault();
            $('#saveInAppModalSendByEmail').trigger('click');
        }
    });

    $('body').on('click', '#saveInAppModalSendByEmail', function(e){
        e.preventDefault();
        var btn = $(this);
        if( $('#routeEditorDestMail').val() && $('#routeEditorDestMail').val() != '' && $('#routeEditorDestMail').val().indexOf('@') > -1 )
        {
            var formData = new FormData( document.getElementById('routeEditorSendEmailForm') );

            var endpoint = $(this).parent().parent().find('.save-in-app-share-btn').data().routeurl;
                
            formData.delete('routeFileName');
            formData.append('routeFileName', $(this).data().routeid + '.pdf');
            formData.append('routeDestMail', formData.get('routeEditorDestMail'));
            formData.append('routeCode', $('#saveInAppModalRouteID').text());
            formData.append('routeName', $('#routeNameHeading').text());

            btn.addClass('spinning');
            
            $.ajax({
                url: endpoint,
                type: 'post',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    // console.info( data );
                    
                    $('#saveInAppModalDownloadApp').addClass('collapsed');
                    btn.removeClass('spinning');
                    $('#routeEditorDestMail').blur();

                    if( data.statusCode == 400 )
                    {
                        $('#routeEditorFeedbackContainer')
                            .addClass('alert-danger')
                            .removeClass('hidden')
                            .removeClass('alert-warning')
                            .removeClass('alert-success')
                            .text( feedbackEmailError );
                    }
                    else
                    {
                        $('#routeEditorFeedbackContainer')
                            .addClass('alert-success')
                            .removeClass('hidden')
                            .removeClass('alert-warning')
                            .removeClass('alert-danger')
                            .text( feedbackEmailSuccessMailSent );
                    }
                },
                error: function (err) {
                    console.log( err );

                    btn.removeClass('spinning');

                    $('#routeEditorFeedbackContainer')
                        .addClass('alert-danger')
                        .removeClass('hidden')
                        .removeClass('alert-warning')
                        .removeClass('alert-success')
                        .text( feedbackEmailError );
                }
            });
        }
        else
        {
            btn.removeClass('btn-route-page-spinner');
            
            $('#routeEditorFeedbackContainer')
                .addClass('alert-warning')
                .removeClass('hidden')
                .removeClass('alert-success')
                .removeClass('alert-danger')
                .text( feedbackEmailWarningNoEmail );
        }
    });

    // Route Page UI Events
    $('body').on('click', '#routeSendEmail', function(e){
        e.preventDefault();
        var btn = $(this);
        if( $('#routeDestMail').val() && $('#routeDestMail').val() != '' && $('#routeDestMail').val().indexOf('@') > -1 )
        {
            var formData = new FormData( document.getElementById('routeSendEmailForm') );

            formData.append('routeCode', $('#routePageRouteAppCode').text());
            formData.append('routeName', $('#routePageRouteName').text());

            btn.addClass('btn-route-page-spinner');

            $.ajax({
                url: window.location.href,
                type: 'post',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.info( data );
                    btn.removeClass('btn-route-page-spinner');

                    if( data.statusCode == 400 )
                    {
                        $('#feedbackContainer')
                            .addClass('alert-danger')
                            .removeClass('hidden')
                            .removeClass('alert-warning')
                            .removeClass('alert-success')
                            .text( feedbackEmailError );
                    }
                    else
                    {
                        $('#feedbackContainer')
                            .addClass('alert-success')
                            .removeClass('hidden')
                            .removeClass('alert-warning')
                            .removeClass('alert-danger')
                            .text( feedbackEmailSuccessMailSent );
                    }
                },
                error: function (err) {
                    console.log( err );

                    btn.removeClass('btn-route-page-spinner');

                    $('#feedbackContainer')
                        .addClass('alert-danger')
                        .removeClass('hidden')
                        .removeClass('alert-warning')
                        .removeClass('alert-success')
                        .text( feedbackEmailError );
                }
            });
        }
        else
        {
            btn.removeClass('btn-route-page-spinner');
            
            $('#feedbackContainer')
                .addClass('alert-warning')
                .removeClass('hidden')
                .removeClass('alert-success')
                .removeClass('alert-danger')
                .text( feedbackEmailWarningNoEmail );
        }
    });

    // Featured Routes UI Events
    $('.featured-route-steps-container').on('show.bs.collapse', function () {
        currentMap = featuredRoutesMaps[ $(this).data().index ];
    });

    $('.featured-route-steps-container').on('shown.bs.collapse', function () {
        currentMap.resize();
        var timeout = setTimeout(function() {
            $( currentMap.getCanvas() ).addClass('visible');
            $('.featured-routes-marker').addClass('visible');
            clearTimeout( timeout );
        }, 100);
    });

    $('body').on('click', '.featured-route-step-number', function(e){
        flyToConfig.zoom = 12;
        flyToConfig.center = [ $(this).data().lng, $(this).data().lat ];
        currentMap.flyTo( flyToConfig );
    });

    $('body').on('click', '.featured-route-step-social-icon', function(e){ 
        e.preventDefault();
    });

    $('body').on('click', '.featured-route-step-tools-use-route-btn', function(e){
        e.preventDefault();
        var form = $('#saveInAppModal').find('#form');
        var info = $('#saveInAppModal').find('#info');
        form.hide();
        info.show();

        var routeTitle = $(this).parent().parent().parent().prev().find('.featured-route-step-title-title-container h2 a').text();
        $('#routeNameHeading').text( routeTitle );

        var routeData = $(this).parent().parent().parent().parent().data();
        $('#saveInAppModalRouteID').text( routeData.code );

        var routeUrl;
        currentLanguage == 'it' ? routeUrl = '/it/itinerario/' + routeData.slug : routeUrl = '/en/route/' + routeData.slug;
        var pdfUrl = '/pdf/' + routeData.slug + '.pdf';
        
        $('#saveInAppModalDownloadPdf')
            .attr('data-routeurl', routeUrl)
            .attr('href', pdfUrl)
            .removeAttr('disabled');

        $('#saveInAppModalViewRoutePage').attr('href', routeUrl).prop("disabled", false).removeAttr('disabled');

        $('#saveInAppModalSendByEmail').attr( 'data-routeid', routeData.slug );

        $.get( routeUrl, function( data ) {
            $('#saveInAppModal').addClass('on');
        });
    });

    // Didactics page UI Events 
    $('body').on('click', '.didactics-town-list-toggle', function(e){
        e.preventDefault();
        $(this).next().css({top: $(this).position().top + 30 + 'px'});
        $(this).next().toggleClass('visible');
    });

    $('#didacticsFilterInput').on('keyup', function(e){
        e.preventDefault();
        var searchString = $(this).val();

        filtered = {};

        var selectorString;
        $('.didactics-argument-card.filter-match').length > 0 ? selectorString = '.didactics-argument-card.filter-match' : selectorString = '.didactics-argument-card';

        if(searchString.length > 2)
        {
            $(selectorString).each(function(i){
                var id = $(this).data().id;

                var re = new RegExp(searchString, 'gi');

                var text = $(this).find('h5 a').text() + '. ' + $(this).find('p').text();
                text = text.replace(/['.,\/#!$%\^&\*;:{}=\-_`~()]/g, '').toLowerCase();

                if( text.search( re ) > -1 )
                {
                    var startPos = text.search( re );
                    var nextSpace = text.indexOf(' ', startPos);

                    if ( text[startPos-1] != ' ' && text[startPos-1] != "'" )
                        startPos = text.lastIndexOf( ' ', startPos ) + 1;

                    var suggestion;
                    if(nextSpace > -1)
                        suggestion = text.substring( startPos, nextSpace );
                    else
                        suggestion = text.substring( startPos );

                    var canUpdateSuggestion = true;
                    $('.filter-tag-link').each(function(i){
                        if( $(this).text() == suggestion )
                            canUpdateSuggestion = false;
                    });

                    if(canUpdateSuggestion)
                        updateDidacticsSuggestions( suggestion );

                    if(!filtered[suggestion])
                        filtered[suggestion] = new Array();

                    filtered[suggestion].push( id );
                }
                else
                {
                    removeDidacticsSuggestions( searchString );
                }
            });
        }

        if(searchString.length == 0)
        {
            $(this).val('');
            clearSuggestions();
        }
    });

    $('body').on('change', '#didacticsFilterByCategorySelect', function(e){
        e.preventDefault();
        resetDidacticsFilters();

        $('#didacticsFilterByTownSelect').find('option').first().val('didacticsFilterByTown').text( didacticsFilterByTown );
        $('#didacticsFilterByTownSelect').val('didacticsFilterByTown');

        var searchString = $(this).val();

        if (searchString == 'reset') {
            $('.didactics-argument-card').each(function(i){ $(this).fadeIn() });

            $(this).find('option').first().val('didacticsFilterByCategory').text( didacticsFilterByCategory );
        }
        else
        {
            $('.didactics-argument-card').each(function(i){
                if( $(this).hasClass( searchString ) )
                    $(this).fadeIn();
                else
                    $(this).fadeOut();
            });

            $(this).find('option').first().val('reset').text( didacticsRemoveCategoryFilter );
        }
    });

    $('body').on('change', '#didacticsFilterByTownSelect', function(e){
        e.preventDefault();
        resetDidacticsFilters();

        $('#didacticsFilterByCategorySelect').find('option').first().val('didacticsFilterByCategory').text( didacticsFilterByCategory );
        $('#didacticsFilterByCategorySelect').val('didacticsFilterByCategory');

        var searchString = $(this).val();

        if (searchString == 'reset') {
            $('.didactics-argument-card').each(function(i){ $(this).fadeIn() });

            $(this).find('option').first().val('didacticsFilterByTown').text( didacticsFilterByTown );
        }
        else
        {
            $('.didactics-argument-card').each(function(i){
                if( $(this).find('.didactics-town-link').length > 0 )
                {
                    if( $(this).find('.didactics-town-link').data().slug == searchString )
                        $(this).fadeIn();
                    else
                        $(this).fadeOut();
                }
                else
                {
                    $(this).fadeOut();
                }
            });

            $(this).find('option').first().val('reset').text( didacticsRemoveTownFilter );
        }
    });

    $('body').on('click', '.didactics-argument-contribution-link', function(e){
        e.preventDefault();
        $('.didactics-argument-contribs-container').not( $(this).next() ).removeClass('visible');
        $(this).next().toggleClass('visible');
    });

    $('body').on('click', '.didactics-argument-download-PDF', function(e){
        e.preventDefault();
        var endpoint = $(this).parent().parent().find('a').first().attr('href');
        var slug = $(this).data().slug;

        $.get( endpoint, function( data ) {
            if(data)
            {
                window.location.href = '/pdf/' + slug + '.pdf';
            }
        });
    });

    // Footer search behavior
    // var searchSiteDefault = $('input[name="searchSite"]').val();
    // $('input[name="searchSite"]').on('focus', function(e){ $(this).val(''); });
    // $('input[name="searchSite"]').on('blur', function(e){
    //     if( $(this).val() == '' )
    //         $(this).val(searchSiteDefault);
    // });

    // Window resize listener
    $(window).on('resize', function(e){
        if( $('.featured-card').length > 0 ) { initHomeFeaturedSlider(); }
        if( $('.video-gallery-card').length > 0 ) { initHomeVideoGallery(); }
        if( $('.argument-featured-card').length > 0 ) { initArgumentFeaturedSlider(); }
        if( $('.town-related-card').length > 0 ) { initTownRelatedSlider(); }

        if( $('#townDescription').length > 0 ) { initTownDescription(); }
        if( $('.didactics-argument-contribs-container').length > 0 ) { initContribsContainers() };

        $('.route-selector').remove();
    });

    // Window scroll listener
    $(window).on('scroll', function(e){
        if( $(window).scrollTop() > 1 )
            $('header').addClass('sticky');
        else
            $('header').removeClass('sticky');

        // $('.route-selector').remove();
    });
});