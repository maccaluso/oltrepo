// Routing

function fetchRouteData(startLocation, routeId, containers) {
    console.log(routeId);
    if( mapConfig.center[0] == '' || mapConfig.center[1] == '') { return; }
    
    var center = mapConfig.center;
    $.when( 
        $.ajax({
            url: 'https://api.mapbox.com/directions/v5/mapbox/driving-traffic/' + startLocation[0] + '%2C' + startLocation[1] + '%3B' + center[0] + '%2C' + center[1] + '.json',
            data: {
                geometries: 'geojson',
                alternatives: false,
                steps: true,
                overview: 'full',
                language: 'it',
                access_token: mapboxgl.accessToken
            }
        })
    )
    .then( 
        function(route) {
            renderRouteLayer( routeId, route );
            if(containers)
            {
                renderRouteData( route.routes[0], containers.routeDataContainer );
                renderRouteSteps( route.routes[0].legs[0].steps, containers.routeStepsContainer );
            }

            if(routeId.indexOf('Custom') > -1) {
                currentMap.setLayoutProperty(routeId, 'visibility', 'visible');

                var coordinates = currentMap.getSource(routeId)._data.geometry.coordinates;

                var bounds = coordinates.reduce(function(bounds, coord) {
                    return bounds.extend(coord);
                }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

                currentMap.fitBounds(bounds, {
                    padding: 20
                });
            }
        }, 
        function(err) {
            console.log(err)
        }
    );
}

function fetchEditorRoute(wayPoints, routeId) {
    if(wayPoints.length <= 20)
    {
        var baseEndpoint = 'https://api.mapbox.com/directions/v5/mapbox/driving/';
        var coordinates = '';
        for(var i in wayPoints)
        {
            coordinates += wayPoints[i].lng + '%2C' + wayPoints[i].lat + '%3B';
        }
        coordinates = coordinates.slice(0, -3);
        coordinates += '.json';

        $.when( 
            $.ajax({
                url: baseEndpoint + coordinates,
                data: {
                    geometries: 'geojson',
                    alternatives: false,
                    steps: false,
                    overview: 'full',
                    language: 'it',
                    access_token: mapboxgl.accessToken
                }
            })
        )
        .then( 
            function(route) {
                renderRouteLayer( routeId, route );
                var km = route.routes[0].distance / 1000;
                $('.route-editor-personal-route-duration').text( km.toFixed(2) + ' Km.' );

                if(routeId.indexOf('Custom') > -1) {
                    currentMap.setLayoutProperty(routeId, 'visibility', 'visible');

                    var coordinates = currentMap.getSource(routeId)._data.geometry.coordinates;

                    var bounds = coordinates.reduce(function(bounds, coord) {
                        return bounds.extend(coord);
                    }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

                    currentMap.fitBounds(bounds, {
                        padding: 20
                    });
                }
            }, 
            function(err) {
                console.log(err.responseText)
            }
        );
    }
    else
    {
        console.log('Non più di 20 waypoints', wayPoints.length);
    }
}

function fetchRoutePageData(startLocation, endLocation, routeId, containers) {
    $.when( 
        $.ajax({
            url: 'https://api.mapbox.com/directions/v5/mapbox/driving-traffic/' + startLocation[0] + '%2C' + startLocation[1] + '%3B' + endLocation[0] + '%2C' + endLocation[1] + '.json',
            data: {
                geometries: 'geojson',
                alternatives: false,
                steps: true,
                overview: 'full',
                language: 'it',
                access_token: mapboxgl.accessToken
            }
        })
    )
    .then( 
        function(route) {
            renderRouteLayer( routeId, route );
            if(containers)
            {
                renderRouteData( route.routes[0], containers.routeDataContainer );
                renderRouteSteps( route.routes[0].legs[0].steps, containers.routeStepsContainer );
            }

            if(routeId.indexOf('Custom') > -1) {
                currentMap.setLayoutProperty(routeId, 'visibility', 'visible');

                var coordinates = currentMap.getSource(routeId)._data.geometry.coordinates;

                var bounds = coordinates.reduce(function(bounds, coord) {
                    return bounds.extend(coord);
                }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

                currentMap.fitBounds(bounds, {
                    padding: 20
                });
            }
        }, 
        function(err) {
            console.log(err)
        }
    );
}

function renderRouteLayer(routeId, route) {
    if( currentMap.getLayer( routeId ) )
        currentMap.removeLayer( routeId );

    if( currentMap.getSource( routeId ) )
        currentMap.removeSource( routeId );

    currentMap.addLayer({
        "id": routeId,
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": route.routes[0].geometry
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#888",
            "line-width": 4
        }
    });

    currentMap.setLayoutProperty(routeId, 'visibility', 'none');
}

function renderRouteData(route, container) {
    var duration, routeDuration;
    var hours, minutes, seconds;

    duration = route.duration;

    if( duration < 3600 )
    {
        minutes = Math.floor(duration / 60);
        seconds = Math.floor(duration % 60);

        routeDuration = str_pad_left( minutes, '0', 2 ) + ' min';
    }
    else
    {
        hours = Math.floor(duration / 3600);
        duration %= 3600;
        minutes = Math.floor(duration / 60);
        routeDuration = str_pad_left( hours, '0', 2 ) + ' h, ' + str_pad_left( minutes, '0', 2 ) + ' min';
    }

    var routeDistance = route.distance / 1000;

    var html = '<br>';
    html += '<span>';
        html += routeDuration;
        html += ' - (' + routeDistance.toFixed(2) + ' km)';
    html += '</span>';

    container.find('a.route-toggle-steps').append( html );
}

function renderRouteSteps(steps, container) {
    for(var i = 0; i < steps.length; i++) {
        var instruction = steps[i].maneuver.instruction;
        
        var typeClass = '';
        if(steps[i].maneuver.type)
            typeClass = steps[i].maneuver.type.replace(/ /g, '_');
        
        var modifierClass = '';
        if(steps[i].maneuver.modifier)
            modifierClass = '_' + steps[i].maneuver.modifier.replace(/ /g, '_')

        var minutes = Math.floor(steps[i].duration / 60);
        var seconds = Math.floor(steps[i].duration % 60);
        var stepDuration = str_pad_left( minutes, '0', 2 ) + ':' + str_pad_left( seconds, '0', 2 );

        var stepDistance = steps[i].distance;

        var html = '';
        html += '<li class="route-step">';
            html += '<span class="route-step-icon ' + typeClass + modifierClass + '"></span>';
            html += '<a class="route-step-link" href="#"><strong>' + instruction + '</strong>';
                html += ' <span class="route-step-duration"> - ' + stepDuration + ' min.</span> - ';
                html += ' <span class="route-step-distance">(' + stepDistance.toFixed(2) + ' mt)</span>';
            html += '</a>';
        html += '</li>';

        container.append( html );
    }
}

function translateInstruction(instruction) {}