// Category map Markers

function addMarkersToCategoryMap() {
    $('.category-argument-card').each(function(i) {
        var id = $(this).data().id;

        var location = {
            lat: $(this).data().lat,
            lng: $(this).data().lng
        }

        var el = document.createElement('div');
        var elInner = document.createElement('div');
        elInner.className = 'marker-inner';
        elInner.style.backgroundColor = $(this).data().color;
        
        el.append( elInner );
        el.className = 'marker argument-marker';
        el.id = 'marker' + id;

        el.addEventListener('click', function(e) {
            resetFilters();

            flyToConfig.center = location;
            flyToConfig.zoom = 14;
            categoryMap.flyTo( flyToConfig );

            filterCategoryCardsByID( id );
        });

        var DOMel = $('<div>')
            .addClass('popup-container category-popup-container')
            .text( $(this).find('h5 a').text() );

        var popup = new mapboxgl.Popup( { offset: 7.5 } );
        popup.setDOMContent( DOMel[0] );
        popups.push( popup );

        var marker = new mapboxgl.Marker(el, { offset: [-15, -15] });
        marker.setLngLat( location );
        marker.setPopup( popup );
        marker.addTo( categoryMap );
    });
}

// Category navigation

function filterByCategory(slug) {
    var currentLocation = window.location.href;
    var parts = currentLocation.split('/');

    if(slug == 'removeFilter')
    {
        parts.pop();
        window.location.href = parts.join('/')
    }
    else
    {

        if( parts.length > 5 )
            parts.pop();
        
        window.location.href = parts.join('/') + '/' + slug;
    }
}

// Category filters

function filterCategoryCards(slug) {
    $('.category-argument-card').each(function(i) {
        if( !$(this).hasClass(slug) )
            $(this).remove();
        else
            $(this).show();
    });
}

function addFilterTag(tag) {
    for (var i in filtered)
    {
        if(i == tag)
        {
            for (var j = 0; j < filtered[i].length; j++)
            {
                $('.category-argument-card[data-id="' + filtered[i][j] + '"]').addClass('filter-match filter-match-' + tag);
                $('.didactics-argument-card[data-id="' + filtered[i][j] + '"]').addClass('filter-match filter-match-' + tag);
            }
        }
    }

    var html = '';
    var color = $('#filterTags').data().color;
    if (color == '#ffffff')
        color = 'rgba(48, 48, 48, 1)';

    html += '<span class="filter-tag">';
        html += '<a class="filter-tag-link enabled" href="#" style="color: ' + color + ';" data-color="' + color + '">' + tag + '</a>';
        html += '<span class="filter-tag-icon enabled"></span>';
        html += '<span class="filter-tag-separator"></span>';
    html += '</span>';

    $('.filter-tag-link, .filter-tag-icon').css({color: '#666666'}).removeClass('enabled');
    $('#filterTags').append( html );
    $('#mainFilterInput, #didacticsFilterInput').val('');
}
function removeFilterTag(tag, target) {
    $(target).parent().fadeOut(300, function(e){
        var prevLink = $(target).parent().prev().find('.filter-tag-link');
        var prevLinkColor;
        prevLink.data() ? prevLinkColor = prevLink.data().color : prevLinkColor = '#666666';
        prevLink.css({color: prevLinkColor}).addClass('enabled');

        $(target).parent().prev().find('.filter-tag-icon').addClass('enabled');

        $(target).parent().remove();

        for (var i in filtered)
        {
            if(i == tag)
            {
                for (var j = 0; j < filtered[i].length; j++)
                {
                    $('.category-argument-card[data-id="' + filtered[i][j] + '"]').removeClass('filter-match filter-match-' + tag).fadeIn();
                    $('.didactics-argument-card[data-id="' + filtered[i][j] + '"]').removeClass('filter-match filter-match-' + tag).fadeIn();
                }
            }
        }

        toggleCategoryCards();
    });
}
function resetFilters() {
    $('.filter-tag').fadeOut(500, function(){ $(this).remove(); });

    $('.category-argument-card, .didactics-argument-card').removeClass('filter-match');
    $('.category-argument-card, .didactics-argument-card').removeClass(function (index, className) {
        return (className.match (/(^|\s)filter-match-\S+/g) || []).join(' ');
    });
    $('.category-argument-card, .didactics-argument-card').fadeIn();

    if(categoryMap)
    {
        flyToConfig.center = mapConfig.center;
        flyToConfig.zoom = mapConfig.zoom;
        categoryMap.flyTo( flyToConfig );

        for(var i = 0; i < popups.length; i++)
            popups[i].remove();

        $('.marker').fadeIn();
    }

    $('#didacticsFilterByCategorySelect').find('option').first().val('didacticsFilterByCategory').text( didacticsFilterByCategory );
    $('#didacticsFilterByCategorySelect').val('didacticsFilterByCategory');
    $('#didacticsFilterByTownSelect').find('option').first().val('didacticsFilterByTown').text( didacticsFilterByTown );
    $('#didacticsFilterByTownSelect').val('didacticsFilterByTown');
}
function resetDidacticsFilters() {
    $('.filter-tag').fadeOut(500, function(){ $(this).remove(); });

    $('.didactics-argument-card').removeClass('filter-match');
    $('.didactics-argument-card').removeClass(function (index, className) {
        return (className.match (/(^|\s)filter-match-\S+/g) || []).join(' ');
    });
}
function toggleCategoryCards() {
    var lastTag = $('.filter-tag-link').last().text();
    if(lastTag.length == 0)
    {
        $('.category-argument-card, .didactics-argument-card, .marker').fadeIn();
    }
    else
    {
        $('.category-argument-card, .didactics-argument-card').each(function(i){
            if ( $(this).hasClass('filter-match-' + lastTag) ) {
                $(this).fadeIn();
                $('#marker' + $(this).data().id).fadeIn();
            }
        });
    }
}
function filterCategoryCardsByTag(tag) {
    $('.category-argument-card, .didactics-argument-card').each(function(i){
        if ( !$(this).hasClass('filter-match-' + tag) ) {
            $(this).fadeOut();
            $('#marker' + $(this).data().id).fadeOut();
        }
    });
}
function filterCategoryCardsByID(id) {
    $('.marker').hide();
    $('#marker' + id).show();

    $('.category-argument-card').hide();
    $('.category-argument-card[data-id="' + id + '"]').fadeIn();
}

function updateSuggestions(suggestion) {
    var canAppend = true;
    var link = $('<a href="#" class="suggestion-link">' + suggestion + '</a>');
    $('.suggestion-link').each(function(i){
        if( $(this).text() == suggestion )
            canAppend = false;
    });

    if(canAppend)
        $('#suggestionsContainer').append(link);

    $('#suggestionsContainer').fadeIn();
}
function removeSuggestions(suggestion) {
    $('.suggestion-link').each(function(i){
        if( $(this).text().search(suggestion) == -1 )
            $(this).remove();
    });

    if( $('.suggestion-link').length == 0 )
        clearSuggestions();
}

function updateDidacticsSuggestions(suggestion) {
    var canAppend = true;
    var link = $('<a href="#" class="didactics-suggestion-link">' + suggestion + '</a>');
    $('.didactics-suggestion-link').each(function(i){
        if( $(this).text() == suggestion )
            canAppend = false;
    });

    if(canAppend)
        $('#suggestionsContainer').append(link);

    $('#suggestionsContainer').fadeIn();
}
function removeDidacticsSuggestions(suggestion) {
    $('.didactics-suggestion-link').each(function(i){
        if( $(this).text().search(suggestion) == -1 )
            $(this).remove();
    });

    if( $('.didactics-suggestion-link').length == 0 )
        clearSuggestions();
}

function clearSuggestions() { $('#suggestionsContainer').html('').fadeOut(); }

function addfeaturedRouteMarkers( map ) {
	var mapContainer = $( map.getContainer() );

	var steps = mapContainer.parent().find('.featured-route-step');

	steps.each(function(i){
		// var lng = $(this).find('.featured-route-step-number').data().lng;
		// var lat = $(this).find('.featured-route-step-number').data().lat;

		var markerNumber = $(this).find('.featured-route-step-number').text();
		var location = {
            lat: $(this).find('.featured-route-step-number').data().lat,
            lng: $(this).find('.featured-route-step-number').data().lng
        }

        var el = document.createElement('div');
        el.className = 'marker featured-routes-marker';
        el.innerHTML = markerNumber;

        var marker = new mapboxgl.Marker(el, { offset: [-12.5 / 2, -12.5 / 2] });
        marker.setLngLat( location );
        marker.addTo( map );
	});
}

function initSocialShare() { 
	$('.featured-route-step-social-icon').share();
}
WebFont.load({
    google: {
        families: ['Montserrat:400,500,600,700,800', 'PT Serif:400,700']
    }
});

$.cloudinary.config({ cloud_name: 'itinerarioltrepomantovano'});

mapboxgl.accessToken = 'pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6IjNLLVM5a0UifQ.fQiOn9nvARyZ80kya3yfjA';
// mapboxgl.accessToken = 'pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6ImNqNXJmNWs4ejByZXMycG16dXRreW54eGoifQ.FrVMLoCLXY17aZaNSWcIXQ';

var currentLanguage, townBasePath, topicBasePath, topicsBasePath;
var confirmDeleteRoute;
var closeText, yourRouteText, addToRouteText, newRouteText, newRouteDefaultName, noSavedRoutesText;
var feedbackEmailWarningNoEmail, feedbackEmailSuccessMailSent, feedbackEmailError;
var didacticsFilterByCategory, didacticsFilterByTown, didacticsRemoveCategoryFilter, didacticsRemoveTownFilter;
var getRouteCode;

var mapConfig = {
    style: 'mapbox://styles/maccaluso/cj5wksxrn7jau2rmkmryvi5co',
    center: [11.07486879208193,45.00679160929593],
    zoom: 10,
    attributionControl: false
};
var flyToConfig = {
    bearing: 0,
    speed: 3.5,
    curve: 1,
    easing: function (t) {
        return t<.5 ? 2*t*t : -1+(4-2*t)*t;
    }
};
var mapBounds = new mapboxgl.LngLatBounds(
  new mapboxgl.LngLat( 10.40489574463723, 44.72322879639489 ),
  new mapboxgl.LngLat( 11.659509644121414, 45.29442275231085 )
);
var popups = [];

var map, routeEditorMap, townSideMap, argumentSideMap, categoryMap, routePageMap, currentMap;
var featuredRoutesMaps = [];
var routeLayerBaseID;

var filtered;

var newWaypoint;

$(document).ready(function(){
    currentLanguage = $('header').data().lang;

    switch (currentLanguage)
    {
        case 'it':
            townBasePath = 'comune';
            topicBasePath = 'argomento';
            topicsBasePath = 'argomenti';
            closeText = 'CHIUDI';
            confirmDeleteRoute = 'Sei sicuro di voler eliminare questo itinerario?';
            cantDeleteTooltip = 'Non puoi eliminare l\'itinerario di default';
            yourRouteText = 'Il tuo itinerario';
            addToRouteText = 'Aggiungi all\'itinerario';
            newRouteText = 'Crea nuovo itinerario';
            newRouteDefaultName = 'Nuovo itinerario';
            noSavedRoutesText = 'Nessun itinerario salvato';
            feedbackEmailWarningNoEmail = 'Devi inserire un indirizzo email valido';
            feedbackEmailSuccessMailSent = 'La email è stata inviata correttamente';
            feedbackEmailError = 'Ops! Qualcosa è andato storto...';
            didacticsFilterByCategory = '-- Filtra per categoria --';
            didacticsFilterByTown = '-- Filtra per comune --';
            didacticsRemoveCategoryFilter = '-- Tutte le categorie --';
            didacticsRemoveTownFilter = '-- Tutti i comuni --';
            getRouteCode = 'Ottieni il codice dell\'itinerario attivo';
            break;
        case 'en':
            townBasePath = 'town';
            topicBasePath = 'topic';
            topicsBasePath = 'topics';
            confirmDeleteRoute = 'Are you sure you want to delete this route?';
            cantDeleteTooltip = 'You can not delete the default route';
            closeText = 'CLOSE';
            yourRouteText = 'Your route';
            addToRouteText = 'Add to route';
            newRouteText = 'Create new route';
            newRouteDefaultName = 'New route';
            noSavedRoutesText = 'No saved routes';
            feedbackEmailWarningNoEmail = 'Please insert a valid email address';
            feedbackEmailSuccessMailSent = 'Email successfully sent';
            feedbackEmailError = 'Ops! Something went wrong...';
            didacticsFilterByCategory = '-- Filter by category --';
            didacticsFilterByTown = '-- Filter by town --';
            didacticsRemoveCategoryFilter = '-- All categories --';
            didacticsRemoveTownFilter = '-- All towns --';
            getRouteCode = 'Get active route code';
            break;
    }

    initDefaultRoute();
    // initRouteEditorModalSocialShare();
    $('.save-in-app-social-icon').share();

    if( $('#map').length > 0 ) { initMap(); }
    if( $('#townSideMap').length > 0 ) { initTownSideMap(); }
    if( $('#argumentSideMap').length > 0 ) { initArgumentSideMap(); }
    if( $('#routeEditorMap').length > 0 ) { initRouteEditorMap(); }
    if( $('#categoryMap').length > 0 ) { initCategoryMap(); }
    if( $('#routePageMap').length > 0 ) { 
        initRoutePageMap(); 
        initRoutePageSocialShare();
    }
    if( $('.featured-route-map').length > 0 ) { 
        initFeaturedRoutesMaps();
        initSocialShare();
    }
    if( $('#saveInAppModal').length > 0 )
    {
        $('.save-in-app-social-icon').share();
    }

    // UI Adjustments calls
    if( $('.featured-card').length > 0 ) { initHomeFeaturedSlider(); }
    if( $('.video-gallery-card').length > 0 ) { initHomeVideoGallery(); }
    if( $('#townDescription').length > 0 ) { initTownDescription(); }
    if( $('.town-argument-info').length > 0 ) { initTownArgumentInfo(); }
    if( $('.town-argument-category-link').length > 0 ) { 
        $('.town-argument-info').each(function(i){
            $(this).find('.town-argument-category-link').last().addClass('last');
        });
    }
    if( $('.town-related-card').length > 0 ) { initTownRelatedSlider(); }
    if( $('.argument-featured-card').length > 0 ) { initArgumentFeaturedSlider(); }
    if( $('.category-argument-card').length > 0 ) { 
        if ( $('#categoryArguments').hasClass('filtered') )
            filterCategoryCards( $('#categoryArguments').data().filter );
        else
            $('.category-argument-card').show();
    }
    if( $('#routeEditorSteps').length > 0 )
        initDraggables();
    if( $('.didactics-argument-contribs-container').length > 0 ) { initContribsContainers() };

    // UI Events listeners
    $('body').on('mouseenter', '.category-link', function(e){
        e.preventDefault();
        $(this).css({
            backgroundColor: $(this).data().color
        });
    });
    $('body').on('mouseleave', '.category-link', function(e){
        e.preventDefault();
        $(this).css({
            backgroundColor: 'transparent'
        });
    });

    $('body').on('click', '#routeEditorToggle a', function(e){
        e.preventDefault();
        $('#routeEditor, header').toggleClass('expanded');

        if( $('#routeEditor, header').hasClass('expanded') )
        {
            $('#routeEditorToggleText').text( closeText );
            $('#logo').addClass('mobile-logo-expanded-route-editor');
            initRouteEditor();
        }
        else
        {
            $('#routeEditorToggleText').text( yourRouteText );
            $('#logo').removeClass('mobile-logo-expanded-route-editor');

            if( $('#townSideMap').length > 0 ) { currentMap = townSideMap; }
            if( $('#argumentSideMap').length > 0 ) { currentMap = argumentSideMap; }
            if( $('#categoryMap').length > 0 ) { currentMap = categoryMap; }
        }
    });
    $('body').on('mouseleave', '#routeEditorToggle a', function(e){
        e.preventDefault();

        $('#routeEditorToggle a').blur();
    });

    $('body').on('click', '#routeEditorCloseIcon', function(e){
        $('#routeEditor, header').removeClass('expanded');
        $('#routeEditorToggleText').text( yourRouteText );
        $('#routeEditorSteps').html('');

        if( $('#townSideMap').length > 0 ) { currentMap = townSideMap; }
        if( $('#argumentSideMap').length > 0 ) { currentMap = argumentSideMap; }
        if( $('#categoryMap').length > 0 ) { currentMap = categoryMap; }
    });

    $('body').on('click', '#mobileNavToggle', function(e) {
        $('#categoryLinks').addClass('expanded');
    });
    $('body').on('click', '#closeMobileNav', function(e) {
        $('#categoryLinks').removeClass('expanded');
    });

    $('body').on('click', '.slider-nav-control', sliderNavigation);
    $('#featuredSlider').on('transitionend', function(e){
        if(e.originalEvent.propertyName == 'left')
            $('body').on('click', '.slider-nav-control', sliderNavigation);
    });

    $('body').on('click', '.video-gallery-nav-control', videoGalleryNavigation);
    $('#videoGallerySlider').on('transitionend', function(e){
        if(e.originalEvent.propertyName == 'left')
            $('body').on('click', '.video-gallery-nav-control', videoGalleryNavigation);
    });

    $('body').on('click', '#argumentTownListToggle', function(e){
        e.preventDefault();
        $('#argumentTownList').toggleClass('visible');
    });
    $('body').on('mouseleave', '#argumentTownList', function(e){ $('#argumentTownList').removeClass('visible'); });

    $('body').on('click', '.argument-featured-nav', argumentSliderNavigation);
    $('#argumentFeaturedSlider').on('transitionend', function(e){
        if(e.originalEvent.propertyName == 'left')
            $('body').on('click', '.argument-featured-nav', argumentSliderNavigation);
    });

    // App Info modal UI Events listeners
    $('body').on('click', '.app-info', function(e){
        e.preventDefault();
        $('#appInfoModal').addClass('on');
    });
    $('body').on('click', '#appInfoModal', function(e){
        if (e.target == this)
        {
            e.preventDefault();
            $('#appInfoModal').removeClass('on');
        }
    });
    $('body').on('click', '#appInfoModalClose', function(e){
        e.preventDefault();
        $('#appInfoModal').removeClass('on');
    });

    // Save in App modal UI Events listeners
    $('body').on('click', '#saveInAppModal', function(e){
        if (e.target == this)
        {
            e.preventDefault();
            $('#saveInAppModal').removeClass('on');

            if( $('#getRouteCode').length > 0 )
                $('#getRouteCode').remove();
        }
    });
    $('body').on('click', '#saveInAppModalClose', function(e){
        e.preventDefault();
        $('#saveInAppModal').removeClass('on');

        if( $('#getRouteCode').length > 0 )
            $('#getRouteCode').remove();
    });

    // Homepage filters UI Events
    $('body').on('click', '.category-filter-link', function(e){
        e.preventDefault();
        $(this).find('.category-filter-bullet-checked').toggleClass('unchecked');
        if( $('.category-filter-bullet-checked.unchecked').length == 5 )
        {
            $('#hideTopicsBtn').addClass('hidden');
            $('#clearFiltersBtn').removeClass('hidden');
        }
        if( $('.category-filter-bullet-checked.unchecked').length == 0 )
        {
            $('#hideTopicsBtn').removeClass('hidden');
            $('#clearFiltersBtn').addClass('hidden');
        }
        filterMapMarkers();
    });

    $('body').on('click', '#hideTopicsBtn', function(e){
        e.preventDefault();
        $('.category-filter-bullet-checked').addClass('unchecked');
        filterMapMarkers();
        $(this).addClass('hidden');
        $(this).next().removeClass('hidden');
    });

    $('body').on('click', '#clearFiltersBtn', function(e){
        e.preventDefault();
        $('.category-filter-bullet-checked').removeClass('unchecked');
        filterMapMarkers();
        $(this).addClass('hidden');
        $(this).prev().removeClass('hidden');
    });

    // Homepage featured UI events
    $('body').on('click', '.featured-card-town-list-toggle', function(e){
        e.preventDefault();
        var list = $(this).next()
        list.css({
            left: $(this).position().left + 30
        });
        $(this).next().toggleClass('visible');
    });
    $('body').on('mouseleave', '.featured-card-town-list', function(e){ $(this).removeClass('visible'); });

    // Town related slider
    $('body').on('click', '.town-related-nav', townRelatedSliderNavigation);
    $('#townRelatedSlider').on('transitionend', function(e){
        if(e.originalEvent.propertyName == 'left')
            $('body').on('click', '.town-related-nav', townRelatedSliderNavigation);
    });

    // Category filters UI Events
    $('#filterByCategorySelect').on('change', function(e){
        e.preventDefault();
        filterByCategory( $(this).val() );
    });
    $('#mainFilterInput').on('keyup', function(e){
        e.preventDefault();
        var searchString = $(this).val();

        filtered = {};

        var selectorString;
        $('.category-argument-card.filter-match').length > 0 ? selectorString = '.category-argument-card.filter-match' : selectorString = '.category-argument-card';

        if(searchString.length > 2)
        {
            $(selectorString).each(function(i){
                var id = $(this).data().id;

                var re = new RegExp(searchString, 'gi');

                var text = $(this).find('h5 a').text() + '. ' + $(this).find('p').text();
                text = text.replace(/['.,\/#!$%\^&\*;:{}=\-_`~()]/g, '').toLowerCase();

                if( text.search( re ) > -1 )
                {
                    var startPos = text.search( re );
                    var nextSpace = text.indexOf(' ', startPos);

                    if ( text[startPos-1] != ' ' && text[startPos-1] != "'" )
                        startPos = text.lastIndexOf( ' ', startPos ) + 1;

                    var suggestion;
                    if(nextSpace > -1)
                        suggestion = text.substring( startPos, nextSpace );
                    else
                        suggestion = text.substring( startPos );

                    var canUpdateSuggestion = true;
                    $('.filter-tag-link').each(function(i){
                        if( $(this).text() == suggestion )
                            canUpdateSuggestion = false;
                    });

                    if(canUpdateSuggestion)
                        updateSuggestions( suggestion );

                    if(!filtered[suggestion])
                        filtered[suggestion] = new Array();

                    filtered[suggestion].push( id );
                }
                else
                {
                    removeSuggestions( searchString );
                }
            });
        }

        if(searchString.length == 0)
        {
            $(this).val('');
            clearSuggestions();
        }
    });
    $('body').on('click', '.suggestion-link, .didactics-suggestion-link', function(e){
        e.preventDefault();
        addFilterTag( $(this).text() );
        filterCategoryCardsByTag( $(this).text() );
        clearSuggestions();
    });
    $('body').on('click', '.filter-tag-link', function(e){
        e.preventDefault();
        removeFilterTag( $(this).text(), e.target );
    });
    $('body').on('click', '#categoryFiltersResetFilters', function(e){
        e.preventDefault();
        resetFilters();
    });

    $('body').on('mouseenter', '.filter-tag-link', function(e){
        e.preventDefault();
        $(this).next().addClass('remove-tag');
    });
    $('body').on('mouseleave', '.filter-tag-link', function(e){
        e.preventDefault();
        $(this).next().removeClass('remove-tag');
    });

    // Routing UI Events
    $('body').on('click', 'a.argument-route-fixed-start, a.town-route-fixed-start', function(e){
        e.preventDefault();
        $(this).next().toggleClass('expanded');
    });

    $('body').on('click', 'a.route-toggle-steps', function(e){
        e.preventDefault();

        $('a.route-toggle-steps').each(function(i){
            $(this).next().removeClass('expanded');

            if( currentMap.getLayer(routeLayerBaseID + i) )
            {
                currentMap.setLayoutProperty(routeLayerBaseID + i, 'visibility', 'none');
            }
        });

        if( currentMap.getLayer(routeLayerBaseID + 'Custom') )
            currentMap.setLayoutProperty(routeLayerBaseID + 'Custom', 'visibility', 'none');

        $(this).next().toggleClass('expanded');

        if( currentMap.getLayer(routeLayerBaseID + $(this).data().id) )
        {
            currentMap.setLayoutProperty(routeLayerBaseID + $(this).data().id, 'visibility', 'visible');

            var coordinates = currentMap.getSource(routeLayerBaseID + $(this).data().id)._data.geometry.coordinates;

            var bounds = coordinates.reduce(function(bounds, coord) {
                return bounds.extend(coord);
            }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

            currentMap.fitBounds(bounds, {
                padding: 20
            });
        }
    });

    $('body').on('click', 'a.route-step-link', function(e){ e.preventDefault(); });

    $('input[name="argumentRouteCustomStart"], input[name="townRouteCustomStart"], input[name="routeInfoRouteCustomStart"]').on('focus', function(e){
        $(this).val('');

        $(this).next().next().addClass('expanded');

        $('.argument-route-container, town-route-container, route-info-route-container').removeClass('expanded');
        // $('a.route-toggle-steps').each(function(i){
        //     $(this).next().removeClass('expanded');

        //     if( currentMap.getLayer(routeLayerBaseID + i) )
        //     {
        //         currentMap.setLayoutProperty(routeLayerBaseID + i, 'visibility', 'none');
        //     }
        // });

        // if( currentMap.getLayer(routeLayerBaseID + 'Custom') )
        //     currentMap.setLayoutProperty(routeLayerBaseID + 'Custom', 'visibility', 'none');

        // currentMap.flyTo({
        //     center: mapConfig.center, 
        //     zoom: mapConfig.zoom
        // });
    });

    $('input[name="argumentRouteCustomStart"], input[name="townRouteCustomStart"], input[name="routeInfoRouteCustomStart"]').on('keyup', function(e){
        if( $(this).val().length > 5 ) 
        {
            $('#argumentRouteCustomStartHints, #townRouteCustomStartHints, #routeInfoRouteCustomStartHints').show();

            $.when( 
                $.ajax({
                    url: 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + $(this).val() + '.json',
                    data: {
                        language: 'it',
                        limit: 5,
                        access_token: mapboxgl.accessToken
                    }
                })
            )
            .then( 
                function(data) {
                    console.log(routeLayerBaseID);
                    var html = '';
                    for(var i = 0; i < data.features.length; i++)
                    {
                        html += '<li><a class="geocoding-hint-link" data-index="' + i + '" href="#">' + data.features[i].place_name + '</a></li>';
                    }

                    $('#' + routeLayerBaseID + 'CustomStartHintsContainer').html( html )
                }, 
                function(err) {
                    console.log(err)
                }
            );
        }
    });

    $('body').on('click', 'a.geocoding-hint-link', function(e){
        e.preventDefault();
        var index = $(this).data().index;

        $('#' + routeLayerBaseID + 'CustomStartHints').hide();

        $.when( 
            $.ajax({
                url: 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + $('input[name="' + routeLayerBaseID + 'CustomStart"]').val() + '.json',
                data: {
                    language: 'it',
                    limit: 5,
                    access_token: mapboxgl.accessToken
                }
            })
        )
        .then( 
            function(data) {
                var lng = data.features[ index ].center[0];
                var lat = data.features[ index ].center[1];
                var startLocationName = data.features[ index ].text;
                var destLocationName = $('#argumentInfos').find('h1').text() || $('#townInfo').find('h1').text() || $('.route-step').first().find('a.route-step-link').text();

                var html = '';
                html += '<li class="route" id="' + routeLayerBaseID + 'Custom" data-start-lat="" data-start-lng="">';
                    html += '<a data-id="Custom" class="route-toggle-steps" href="#">' + startLocationName + ' > ' + destLocationName + '</a>';
                    html += '<ul class="route-steps expanded" id="' + routeLayerBaseID + 'StepsCustom"></ul>';
                html += '</li>';

                $('#argumentRouteContainerCustom, #townRouteContainerCustom, #routeInfoRouteContainerCustom').html( html );
                $('#argumentRouteContainerCustom, #townRouteContainerCustom, #routeInfoRouteContainerCustom').parent().addClass('expanded');

                var containers = {
                    routeDataContainer: $('#' + routeLayerBaseID + 'Custom'),
                    routeStepsContainer: $('#' + routeLayerBaseID + 'StepsCustom')
                }

                fetchRoutePageData(data.features[ index ].center, [$('.route-step').first().data().lng, $('.route-step').first().data().lat], routeLayerBaseID + 'Custom', containers);
            }, 
            function(err) {
                console.log(err)
            }
        );
    });

    // Personal Route UI Events
    $('body').on('click', '.add-to-route-link, .argument-add-to-route, .town-argument-add-to-route-link, .category-add-to-route-link', function(e){
        e.preventDefault();

        if( $('#routeEditor, header').hasClass('expanded') )
            $('#routeEditorToggle a').trigger('click');

        setNewWaypoint( $(this) );
        toggleRoutesSelector( e, $('body') );
    });

    $('body').on('click', '#addAllToRouteLink', function(e){
        e.preventDefault();
        toggleRoutesSelectorAll( e, $('body') );
    });

    $('body').on('click', '.route-selector-new-route', function(e){
        e.preventDefault();
        $(this).hide();
        $('.route-selector-new-route-input-container').show()
        $('input[name="routeSelectorNewRouteInput"]').focus();
    });

    $('body').on('click', '.route-selector-new-route-input-submit', function(e){ 
        e.preventDefault();
        saveNewRoute(); 
    });

    $('body').on('click', '.route-selector-link', function(e){
        e.preventDefault();
        addWaypointToRoute( $(this).parent().index() );
        $(this).addClass('added');

        var delay = setTimeout(function(){
            $('.route-selector').remove();
            clearTimeout( delay );
        }, 1000);
    });
    $('body').on('click', '.route-selector-link-all', function(e){
        e.preventDefault();
        // addWaypointToRoute( $(this).parent().index() );
        addAllToRoute( $(this).parent().index() );
        $(this).addClass('added');

        var delay = setTimeout(function(){
            $('.route-selector').remove();
            clearTimeout( delay );
        }, 1000);
    });
    $('body').on('keydown', 'input[name="routeSelectorNewRouteInput"]', function(e){
        if( e.keyCode === 13 )
            saveNewRoute();
    });

    // Route Editor UI Events
    $('body').on('click', '.route-editor-switch-route-link', function(e){
        e.preventDefault();
        switchActiveRoute( $(this).parent().index() );
    });

    $('body').on('click', '#routeEditorEditRouteName', function(e){
        e.preventDefault();
        $(this).fadeOut(100, function(){
            $('#routeEditorSaveRouteName').fadeIn();
            $('input[name="routeName"]').prop("disabled", false).focus();
        });
    });

    $('body').on('click', '#routeEditorSaveRouteName', function(e){
        e.preventDefault();
        saveRouteName();
        $(this).fadeOut(100, function(){
            $('#routeEditorEditRouteName').fadeIn();
            $('input[name="routeName"]').prop("disabled", true).blur();
        });
    });
    $('body').on('keydown', 'input[name="routeName"]', function(e){
        if( e.keyCode === 13 )
        {
            saveRouteName();
            $('#routeEditorSaveRouteName').fadeOut(100, function(){
                $('#routeEditorEditRouteName').fadeIn();
                $('input[name="routeName"]').prop("disabled", true).blur();
            });
        }
    });

    $('body').on('click', '#routeEditorDeleteRoute', function(e){
        e.preventDefault();

        var r = confirm( confirmDeleteRoute );
        if(r)
            deleteActiveRoute();
    });

    $('body').on('click', '#routeEditorCreateNewRoute', function(e){
        e.preventDefault();
        $(this).hide();
        $('#routeEditorNewRouteInputContainer').show();
        // var html = '<input type="text" name="routeEditorNewRoute">';
        // html += '<a class="btn btn-default route-editor-submit-new-route-name"><span></span></a>'
        // $(this).after( html );
        $('input[name="routeEditorNewRoute"]').val( newRouteDefaultName ).focus();
        // $(this).addClass('disabled');
    });

    $('body').on('blur', 'input[name="routeEditorNewRoute"]', function(e){
        // $('#routeEditorNewRouteInputContainer').hide();
        // $('#routeEditorCreateNewRoute').show();
        // console.log(e.target);
    });

    $('body').on('keydown', 'input[name="routeEditorNewRoute"]', function(e){
        if( e.keyCode === 13 )
            createEmptyRoute();
    });
    $('body').on('click', 'a.route-editor-submit-new-route-name', function(e){
        e.preventDefault();
        createEmptyRoute();
    });

    $('body').on('click', '#routeEditorListSaveInAppLink', function(e){
        e.preventDefault();

        $('#saveInAppModalPreloader').hide();
        $('#saveInAppModalDownloadPdf').attr('disabled', 'disabled');
        $('#getRouteCode').attr('disabled', 'disabled');
        grecaptcha.reset();

        $('.save-in-app-modal-col#form').show();
        $('.save-in-app-modal-col#info').hide();

        $('#saveInAppModal').addClass('on');
    });

    $('body').on('click', '#getRouteCode', function(e){
        e.preventDefault();
        saveRouteInApp( $('#g-recaptcha-response').val() );
    });

    $('body').on('click', '.remove-route-editor-step', function(e){ removeWaypointFromRoute( $(this).parent().index() ); });

    $('#routeEditorDestMail').on('keydown', function(e){
        if(e.keyCode == 13)
        {
            e.preventDefault();
            $('#saveInAppModalSendByEmail').trigger('click');
        }
    });

    $('body').on('click', '#saveInAppModalSendByEmail', function(e){
        e.preventDefault();
        var btn = $(this);
        if( $('#routeEditorDestMail').val() && $('#routeEditorDestMail').val() != '' && $('#routeEditorDestMail').val().indexOf('@') > -1 )
        {
            var formData = new FormData( document.getElementById('routeEditorSendEmailForm') );

            var endpoint = $(this).parent().parent().find('.save-in-app-share-btn').data().routeurl;
                
            formData.delete('routeFileName');
            formData.append('routeFileName', $(this).data().routeid + '.pdf');
            formData.append('routeDestMail', formData.get('routeEditorDestMail'));
            formData.append('routeCode', $('#saveInAppModalRouteID').text());
            formData.append('routeName', $('#routeNameHeading').text());

            btn.addClass('spinning');
            
            $.ajax({
                url: endpoint,
                type: 'post',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    // console.info( data );
                    
                    $('#saveInAppModalDownloadApp').addClass('collapsed');
                    btn.removeClass('spinning');
                    $('#routeEditorDestMail').blur();

                    if( data.statusCode == 400 )
                    {
                        $('#routeEditorFeedbackContainer')
                            .addClass('alert-danger')
                            .removeClass('hidden')
                            .removeClass('alert-warning')
                            .removeClass('alert-success')
                            .text( feedbackEmailError );
                    }
                    else
                    {
                        $('#routeEditorFeedbackContainer')
                            .addClass('alert-success')
                            .removeClass('hidden')
                            .removeClass('alert-warning')
                            .removeClass('alert-danger')
                            .text( feedbackEmailSuccessMailSent );
                    }
                },
                error: function (err) {
                    console.log( err );

                    btn.removeClass('spinning');

                    $('#routeEditorFeedbackContainer')
                        .addClass('alert-danger')
                        .removeClass('hidden')
                        .removeClass('alert-warning')
                        .removeClass('alert-success')
                        .text( feedbackEmailError );
                }
            });
        }
        else
        {
            btn.removeClass('btn-route-page-spinner');
            
            $('#routeEditorFeedbackContainer')
                .addClass('alert-warning')
                .removeClass('hidden')
                .removeClass('alert-success')
                .removeClass('alert-danger')
                .text( feedbackEmailWarningNoEmail );
        }
    });

    // Route Page UI Events
    $('body').on('click', '#routeSendEmail', function(e){
        e.preventDefault();
        var btn = $(this);
        if( $('#routeDestMail').val() && $('#routeDestMail').val() != '' && $('#routeDestMail').val().indexOf('@') > -1 )
        {
            var formData = new FormData( document.getElementById('routeSendEmailForm') );

            formData.append('routeCode', $('#routePageRouteAppCode').text());
            formData.append('routeName', $('#routePageRouteName').text());

            btn.addClass('btn-route-page-spinner');

            $.ajax({
                url: window.location.href,
                type: 'post',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.info( data );
                    btn.removeClass('btn-route-page-spinner');

                    if( data.statusCode == 400 )
                    {
                        $('#feedbackContainer')
                            .addClass('alert-danger')
                            .removeClass('hidden')
                            .removeClass('alert-warning')
                            .removeClass('alert-success')
                            .text( feedbackEmailError );
                    }
                    else
                    {
                        $('#feedbackContainer')
                            .addClass('alert-success')
                            .removeClass('hidden')
                            .removeClass('alert-warning')
                            .removeClass('alert-danger')
                            .text( feedbackEmailSuccessMailSent );
                    }
                },
                error: function (err) {
                    console.log( err );

                    btn.removeClass('btn-route-page-spinner');

                    $('#feedbackContainer')
                        .addClass('alert-danger')
                        .removeClass('hidden')
                        .removeClass('alert-warning')
                        .removeClass('alert-success')
                        .text( feedbackEmailError );
                }
            });
        }
        else
        {
            btn.removeClass('btn-route-page-spinner');
            
            $('#feedbackContainer')
                .addClass('alert-warning')
                .removeClass('hidden')
                .removeClass('alert-success')
                .removeClass('alert-danger')
                .text( feedbackEmailWarningNoEmail );
        }
    });

    // Featured Routes UI Events
    $('.featured-route-steps-container').on('show.bs.collapse', function () {
        currentMap = featuredRoutesMaps[ $(this).data().index ];
    });

    $('.featured-route-steps-container').on('shown.bs.collapse', function () {
        currentMap.resize();
        var timeout = setTimeout(function() {
            $( currentMap.getCanvas() ).addClass('visible');
            $('.featured-routes-marker').addClass('visible');
            clearTimeout( timeout );
        }, 100);
    });

    $('body').on('click', '.featured-route-step-number', function(e){
        flyToConfig.zoom = 12;
        flyToConfig.center = [ $(this).data().lng, $(this).data().lat ];
        currentMap.flyTo( flyToConfig );
    });

    $('body').on('click', '.featured-route-step-social-icon', function(e){ 
        e.preventDefault();
    });

    $('body').on('click', '.featured-route-step-tools-use-route-btn', function(e){
        e.preventDefault();
        var form = $('#saveInAppModal').find('#form');
        var info = $('#saveInAppModal').find('#info');
        form.hide();
        info.show();

        var routeTitle = $(this).parent().parent().parent().prev().find('.featured-route-step-title-title-container h2 a').text();
        $('#routeNameHeading').text( routeTitle );

        var routeData = $(this).parent().parent().parent().parent().data();
        $('#saveInAppModalRouteID').text( routeData.code );

        var routeUrl;
        currentLanguage == 'it' ? routeUrl = '/it/itinerario/' + routeData.slug : routeUrl = '/en/route/' + routeData.slug;
        var pdfUrl = '/pdf/' + routeData.slug + '.pdf';
        
        $('#saveInAppModalDownloadPdf')
            .attr('data-routeurl', routeUrl)
            .attr('href', pdfUrl)
            .removeAttr('disabled');

        $('#saveInAppModalViewRoutePage').attr('href', routeUrl).prop("disabled", false).removeAttr('disabled');

        $('#saveInAppModalSendByEmail').attr( 'data-routeid', routeData.slug );

        $.get( routeUrl, function( data ) {
            $('#saveInAppModal').addClass('on');
        });
    });

    // Didactics page UI Events 
    $('body').on('click', '.didactics-town-list-toggle', function(e){
        e.preventDefault();
        $(this).next().css({top: $(this).position().top + 30 + 'px'});
        $(this).next().toggleClass('visible');
    });

    $('#didacticsFilterInput').on('keyup', function(e){
        e.preventDefault();
        var searchString = $(this).val();

        filtered = {};

        var selectorString;
        $('.didactics-argument-card.filter-match').length > 0 ? selectorString = '.didactics-argument-card.filter-match' : selectorString = '.didactics-argument-card';

        if(searchString.length > 2)
        {
            $(selectorString).each(function(i){
                var id = $(this).data().id;

                var re = new RegExp(searchString, 'gi');

                var text = $(this).find('h5 a').text() + '. ' + $(this).find('p').text();
                text = text.replace(/['.,\/#!$%\^&\*;:{}=\-_`~()]/g, '').toLowerCase();

                if( text.search( re ) > -1 )
                {
                    var startPos = text.search( re );
                    var nextSpace = text.indexOf(' ', startPos);

                    if ( text[startPos-1] != ' ' && text[startPos-1] != "'" )
                        startPos = text.lastIndexOf( ' ', startPos ) + 1;

                    var suggestion;
                    if(nextSpace > -1)
                        suggestion = text.substring( startPos, nextSpace );
                    else
                        suggestion = text.substring( startPos );

                    var canUpdateSuggestion = true;
                    $('.filter-tag-link').each(function(i){
                        if( $(this).text() == suggestion )
                            canUpdateSuggestion = false;
                    });

                    if(canUpdateSuggestion)
                        updateDidacticsSuggestions( suggestion );

                    if(!filtered[suggestion])
                        filtered[suggestion] = new Array();

                    filtered[suggestion].push( id );
                }
                else
                {
                    removeDidacticsSuggestions( searchString );
                }
            });
        }

        if(searchString.length == 0)
        {
            $(this).val('');
            clearSuggestions();
        }
    });

    $('body').on('change', '#didacticsFilterByCategorySelect', function(e){
        e.preventDefault();
        resetDidacticsFilters();

        $('#didacticsFilterByTownSelect').find('option').first().val('didacticsFilterByTown').text( didacticsFilterByTown );
        $('#didacticsFilterByTownSelect').val('didacticsFilterByTown');

        var searchString = $(this).val();

        if (searchString == 'reset') {
            $('.didactics-argument-card').each(function(i){ $(this).fadeIn() });

            $(this).find('option').first().val('didacticsFilterByCategory').text( didacticsFilterByCategory );
        }
        else
        {
            $('.didactics-argument-card').each(function(i){
                if( $(this).hasClass( searchString ) )
                    $(this).fadeIn();
                else
                    $(this).fadeOut();
            });

            $(this).find('option').first().val('reset').text( didacticsRemoveCategoryFilter );
        }
    });

    $('body').on('change', '#didacticsFilterByTownSelect', function(e){
        e.preventDefault();
        resetDidacticsFilters();

        $('#didacticsFilterByCategorySelect').find('option').first().val('didacticsFilterByCategory').text( didacticsFilterByCategory );
        $('#didacticsFilterByCategorySelect').val('didacticsFilterByCategory');

        var searchString = $(this).val();

        if (searchString == 'reset') {
            $('.didactics-argument-card').each(function(i){ $(this).fadeIn() });

            $(this).find('option').first().val('didacticsFilterByTown').text( didacticsFilterByTown );
        }
        else
        {
            $('.didactics-argument-card').each(function(i){
                if( $(this).find('.didactics-town-link').length > 0 )
                {
                    if( $(this).find('.didactics-town-link').data().slug == searchString )
                        $(this).fadeIn();
                    else
                        $(this).fadeOut();
                }
                else
                {
                    $(this).fadeOut();
                }
            });

            $(this).find('option').first().val('reset').text( didacticsRemoveTownFilter );
        }
    });

    $('body').on('click', '.didactics-argument-contribution-link', function(e){
        e.preventDefault();
        $('.didactics-argument-contribs-container').not( $(this).next() ).removeClass('visible');
        $(this).next().toggleClass('visible');
    });

    $('body').on('click', '.didactics-argument-download-PDF', function(e){
        e.preventDefault();
        var endpoint = $(this).parent().parent().find('a').first().attr('href');
        var slug = $(this).data().slug;

        $.get( endpoint, function( data ) {
            if(data)
            {
                window.location.href = '/pdf/' + slug + '.pdf';
            }
        });
    });

    // Footer search behavior
    // var searchSiteDefault = $('input[name="searchSite"]').val();
    // $('input[name="searchSite"]').on('focus', function(e){ $(this).val(''); });
    // $('input[name="searchSite"]').on('blur', function(e){
    //     if( $(this).val() == '' )
    //         $(this).val(searchSiteDefault);
    // });

    // Window resize listener
    $(window).on('resize', function(e){
        if( $('.featured-card').length > 0 ) { initHomeFeaturedSlider(); }
        if( $('.video-gallery-card').length > 0 ) { initHomeVideoGallery(); }
        if( $('.argument-featured-card').length > 0 ) { initArgumentFeaturedSlider(); }
        if( $('.town-related-card').length > 0 ) { initTownRelatedSlider(); }

        if( $('#townDescription').length > 0 ) { initTownDescription(); }
        if( $('.didactics-argument-contribs-container').length > 0 ) { initContribsContainers() };

        $('.route-selector').remove();
    });

    // Window scroll listener
    $(window).on('scroll', function(e){
        if( $(window).scrollTop() > 1 )
            $('header').addClass('sticky');
        else
            $('header').removeClass('sticky');

        // $('.route-selector').remove();
    });
});
var directions = {
    'N': 'north',
    'NE': 'northeast',
    'E': 'east',
    'SE': 'southeast',
    'S': 'south',
    'SW': 'southwest',
    'W': 'west',
    'NW': 'northwest'
};

var maneuvers = {
    '1': 'continue',
    '2': 'bear right',
    '3': 'turn right',
    '4': 'sharp right',
    '5': 'u-turn',
    '6': 'sharp left',
    '7': 'turn left',
    '8': 'bear left',
    '9': 'waypoint',
    '10': 'depart',
    '11': 'enter roundabout',
    '15': 'arrive'
};

var travelMode = {
    'mapbox.driving': ['unaccessible', 'driving', 'ferry', 'movable bridge'],
    'mapbox.walking': ['unaccessible', 'walking', 'ferry'],
    'mapbox.cycling': ['unaccessible', 'cycling', 'walking', 'ferry', 'train', 'moveable bridge']
};

var textInstructions = {
    '1': 'Continue[ on {way_name}]',
    '2': 'Bear right[ onto {way_name}]',
    '3': 'Turn right[ onto {way_name}]',
    '4': 'Make a sharp right[ onto {way_name}]',
    '5': 'Make a U-turn[ onto {way_name}]',
    '6': 'Make a sharp left[ onto {way_name}]',
    '7': 'Turn left[ onto {way_name}]',
    '8': 'Bear left[ onto {way_name}]',
    '9': 'Reach waypoint[ and continue on {way_name}]',
    '10': 'Head {direction}[ on {way_name}]',
    '11': 'Enter the roundabout[ and take the exit onto {way_name}]',
    '15': 'You have arrived at your destination'
};
// Main map Markers

function addTowns(d) {

    d.forEach(function(town){
        var el = document.createElement('div');
        el.className = 'marker town-marker';

        var DOMel = $('<div>')
            .addClass('popup-container town-popup-container');

        var head = $('<div>')
            .addClass('popup-header');

        var img;
        if(town.coverImg.src)
        {
            var pid = town.coverImg.src.public_id;
            img = $.cloudinary.image(pid, { width: 300, height: 190, crop: 'fill' });
        }
        else
        {
            img = $('<img>')
                .attr('src', 'images/defaultPopupImg.svg')
                .attr('width', '300')
                .attr('height', '190');
        }

        img.addClass('img-responsive');
        head.append( img );
        head.append('<div class="popup-img-overlay"></div>');

        head.append('<h5><a href="/' + currentLanguage + '/' + townBasePath + '/' + town.key + '">' + town.name + '</a></h5>');

        DOMel.append( head );

        if(town.arguments)
        {
            var argumentList = $('<ul>')
            town.arguments.forEach(function(argument){
                var argumentElement = $('<li>')
                var argumentLink = $('<a>')
                    .attr('href', '/' + currentLanguage + '/' + topicBasePath + '/' + getArgumentSlug(argument))
                    .addClass('argument-link')
                    .text( getArgumentName(argument) );
                argumentElement.append( argumentLink );

                var categoryList = $('<span>')
                    .addClass('category-link-list');

                argument.categories.forEach(function(id){
                    var category = $('.category-link[data-id="' + id._id + '"]');
                    var catCol = category.data().color || '#ffffff';
                    var categoryLink = $('<a>')
                        .attr('href', topicsBasePath + '/' + category.data().slug)
                        .addClass('popup-category-link')
                        .css({ color: catCol })
                        .text( category.text() );
                    categoryList.append( categoryLink );
                })
                argumentElement.append( categoryList );

                argumentElement.append( '<a class="add-to-route-link" href="#" title="' + addToRouteText + '"></div>' );

                argumentElement.append( '<div style="clear: both;"></div>' );

                argumentList.append( argumentElement );
            });

            DOMel.append( argumentList );
        }

        var popup = new mapboxgl.Popup( { offset: 15 } );
        popup.setDOMContent( DOMel[0] );

        popup.on('close', function(e){
            $('.route-selector').remove();
        });

        if(town.location)
        {
            var marker = new mapboxgl.Marker(el, { offset: [-25 / 2, -40 / 2] })
            marker.setLngLat(town.location.location.geo);
            marker.setPopup( popup );
            marker.addTo(map);
        }
    });
}

function addArgumentMarkers(d) {
    d.forEach(function(arg){

        var el = document.createElement('div');
        var elInner = document.createElement('div');
        elInner.className = 'marker-inner';
        
        if(arg.categories[0])
        {
            elInner.style.backgroundColor = arg.categories[0].color;
            
            var categoryClass;
            currentLanguage == 'it' ? categoryClass = arg.categories[0].key : categoryClass = arg.categories[0].slugEN;
        }
        else
        {
            elInner.style.backgroundColor = '#ffffff';
            
            var categoryClass;
            currentLanguage == 'it' ? categoryClass = 'senza-categoria' : categoryClass = 'uncategorized';
        }

        el.append( elInner );
        el.className = 'marker argument-marker ' + categoryClass;

        var DOMel = $('<div>')
            .addClass('popup-container argument-popup-container');

        var head = $('<div>')
            .addClass('popup-header');

        if(arg.coverImg)
        {
            var img;
            if(arg.coverImg.src)
            {
                var pid = arg.coverImg.src.public_id;
                img = $.cloudinary.image(pid, { width: 300, height: 190, crop: 'fill' });
            }
            else
            {
                img = $('<img>')
                    .attr('src', '/images/defaultPopupImg.svg')
                    .attr('width', '300')
                    .attr('height', '190');
            }

            img.addClass('img-responsive');
            head.append( img );
            head.append('<div class="popup-img-overlay"></div>');
        }

        head.append('<h5><a href="/' + currentLanguage + '/' + topicBasePath + '/' + getArgumentSlug(arg) + '">' + getArgumentName(arg) + '</a></h5>');

        var categoryList = $('<div>')
            .addClass('category-link-list');

        arg.categories.forEach(function(cat){
            var slug;
            currentLanguage == 'it' ? slug = cat.key : slug = cat.slugEN;
            var categoryLink = $('<a>')
                .attr('href', topicsBasePath + '/' + slug)
                .addClass('popup-category-link')
                .css({ color: cat.color })
                .text( getCategoryName(cat) );

            categoryList.append(categoryLink);
        });

        head.append(categoryList);

        DOMel.append( head );

        var addToRoute = $('<div>')
            .addClass('add-to-route-argument-popup-link')

        var addToRouteLink = $('<a>')
            .addClass('add-to-route-link')
            .attr('href', '#')
            .attr('data-id', arg._id)
            .text( addToRouteText );

        if(arg.location && arg.location.location.geo)
        {
            addToRouteLink
                .attr('data-lng', arg.location.location.geo[0])
                .attr('data-lat', arg.location.location.geo[1])
        }

        addToRoute.append( addToRouteLink );

        DOMel.append(addToRoute);

        var popup = new mapboxgl.Popup( { offset: 15 } );
        popup.setDOMContent( DOMel[0] );

        popup.on('close', function(e){
            $('.route-selector').remove();
        });

        if(arg.location)
        {
            if(arg.location.location.geo)
            {
                var marker = new mapboxgl.Marker(el, { offset: [-15, -15] });
                marker.setLngLat(arg.location.location.geo);
                marker.setPopup( popup );
                marker.addTo(map);
            }
        }
    })
}

// Main map Filters

function filterMapMarkers() {
    $('.category-filter-container').each(function(i){
        var slug = $(this).data().slug;
        if( $(this).find('.category-filter-bullet-checked').hasClass('unchecked') )
        {
            $('.argument-marker.' + slug).addClass('faded');
        }
        else
        {
            $('.argument-marker.' + slug).removeClass('faded');
        }
    });
}
// Maps initialization

function initMap() {
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/maccaluso/cj5wksxrn7jau2rmkmryvi5co',
        center: [11.07486879208193,45.00679160929593],
        zoom: 10,
        attributionControl: false
    });

    map.addControl( new mapboxgl.AttributionControl({ compact: true }));
    map.addControl( new mapboxgl.NavigationControl(), 'top-right' );

    map.on('load', function () {
        $.when( 
            $.ajax("/api/towns"), 
            $.ajax("/api/arguments") 
        )
        .then( 
            function(towns, arguments) {
                addTowns( towns[0] );
                addArgumentMarkers( arguments[0] );
            }, 
            function(err) {
                console.log(err)
            }
        );
    });

    map.on('movestart', function() { $('.route-selector').remove(); });

    map.scrollZoom.disable();
    map.setMaxBounds( mapBounds );
}

function initTownSideMap() {
    var lat = $('#townSideMap').data().lat;
    var lng = $('#townSideMap').data().lng;

    var el = document.createElement('div');
        el.className = 'marker town-marker';

    mapConfig.container = 'townSideMap';
    mapConfig.center = [lng,lat];
    mapConfig.zoom = 7;

    townSideMap = new mapboxgl.Map(mapConfig);
    currentMap = townSideMap;
    routeLayerBaseID = 'townRoute';

    townSideMap.addControl( new mapboxgl.AttributionControl({ compact: true }));
    townSideMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );
    townSideMap.scrollZoom.disable();
    // townSideMap.setMaxBounds( mapBounds );

    townSideMap.on('load', function () {
        var marker = new mapboxgl.Marker(el, { offset: [-25 / 2, -25 / 2] })
        marker.setLngLat([lng,lat]);
        marker.addTo(townSideMap);

        $('.route').each(function(i){
            var startLocation = [ $(this).data().startLng, $(this).data().startLat ];
            
            var el = document.createElement('div');
            el.className = 'marker toll-marker';

            var marker = new mapboxgl.Marker(el, { offset: [-25 / 2, -25 / 2] })
            marker.setLngLat( startLocation );
            marker.addTo(townSideMap);

            // el.addEventListener('click', function(e) {});

            var routeId = routeLayerBaseID + i;

            var containers = {
                routeDataContainer: $('#' + routeLayerBaseID + i),
                routeStepsContainer: $('#' + routeLayerBaseID + 'Steps' + i)
            }
        
            fetchRouteData(startLocation, routeId, containers);
        });
    });
}

function initArgumentSideMap() {
    var lat = $('#argumentSideMap').data().lat;
    var lng = $('#argumentSideMap').data().lng;
    var color = $('#argumentSideMap').data().color;

    var el = document.createElement('div');
    el.className = 'marker argument-marker';
    var elInner = document.createElement('div');
        elInner.className = 'marker-inner';

    if(color)
        elInner.style.backgroundColor = color;
    else
        elInner.style.backgroundColor = '#ffffff';

    el.append( elInner );

    mapConfig.container = 'argumentSideMap';
    mapConfig.center = [lng,lat];
    mapConfig.zoom = 7;

    argumentSideMap = new mapboxgl.Map(mapConfig);
    currentMap = argumentSideMap;
    routeLayerBaseID = 'argumentRoute';

    argumentSideMap.addControl( new mapboxgl.AttributionControl({ compact: true }));
    argumentSideMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );
    argumentSideMap.scrollZoom.disable();
    // argumentSideMap.setMaxBounds( mapBounds );

    argumentSideMap.on('load', function () {
        var marker = new mapboxgl.Marker(el, { offset: [-25 / 2, -25 / 2] })
        marker.setLngLat([lng,lat]);
        marker.addTo(argumentSideMap);

        $('.route').each(function(i){
            var startLocation = [ $(this).data().startLng, $(this).data().startLat ];
            
            var el = document.createElement('div');
            el.className = 'marker toll-marker';

            var marker = new mapboxgl.Marker(el, { offset: [-25 / 2, -25 / 2] })
            marker.setLngLat( startLocation );
            marker.addTo(argumentSideMap);

            // el.addEventListener('click', function(e) {});

            var routeId = routeLayerBaseID + i;

            var containers = {
                routeDataContainer: $('#' + routeLayerBaseID + i),
                routeStepsContainer: $('#' + routeLayerBaseID + 'Steps' + i)
            }
        
            fetchRouteData(startLocation, routeId, containers);
        });
    });
}

function initRouteEditorMap() {
    mapConfig.container = 'routeEditorMap';
    mapConfig.zoom = 8;

    routeEditorMap = new mapboxgl.Map(mapConfig);

    routeEditorMap.addControl( new mapboxgl.AttributionControl({ compact: true }));

    routeEditorMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );

    routeEditorMap.scrollZoom.disable();
}

function initCategoryMap() {
    mapConfig.container = 'categoryMap';
    mapConfig.zoom = 8.5;
    categoryMap = new mapboxgl.Map(mapConfig);
    currentMap = categoryMap;

    categoryMap.addControl( new mapboxgl.AttributionControl({ compact: true }));
    categoryMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );

    categoryMap.on('load', function() { addMarkersToCategoryMap(); });

    categoryMap.scrollZoom.disable();

    categoryMap.setMaxBounds( mapBounds );
}

function initRoutePageMap() {
    mapConfig.container = 'routePageMap';
    mapConfig.zoom = 9;
    routePageMap = new mapboxgl.Map(mapConfig);
    currentMap = routePageMap;
    routeLayerBaseID = 'routeInfoRoute';

    routePageMap.addControl( new mapboxgl.AttributionControl({ compact: true }));
    routePageMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );

    routePageMap.on('load', function() { addMarkersToRoutePageMap(); });

    routePageMap.scrollZoom.disable();

    // routePageMap.setMaxBounds( mapBounds );
}

function initFeaturedRoutesMaps() {
    $('.featured-route-map').each(function(i){
        mapConfig.container = $(this).attr('id');
        mapConfig.zoom = 9;

        var featuredRouteMap = new mapboxgl.Map(mapConfig);

        featuredRouteMap.addControl( new mapboxgl.AttributionControl({ compact: true }));
        featuredRouteMap.addControl( new mapboxgl.NavigationControl(), 'top-right' );

        featuredRouteMap.on('load', function(e) { addfeaturedRouteMarkers( e.target ) });

        featuredRouteMap.scrollZoom.disable();

        featuredRouteMap.setMaxBounds( mapBounds );

        featuredRoutesMaps.push( featuredRouteMap );
    });
}
// Personal routes

function toggleRoutesSelector(e, container) {
    var routes = store.get('oltrepoRoutes');

    var html = '';
    html += '<div class="route-selector" style="left: ' + (e.pageX - 15) +'px; top: ' + e.pageY + 'px;">';
        
        html += '<ul class="route-selector-routes">';
            if(routes)
            {
                if(routes.length > 0)
                {
                    for(var i = 0; i < routes.length; i++)
                    {
                        var canAdd = true;
                        for(var j = 0; j < routes[i].wayPoints.length; j++)
                        {
                            if( routes[i].wayPoints[j].id == newWaypoint.id )
                                canAdd = false;
                        }

                        if (canAdd)
                            html += '<li><a class="route-selector-link" href="#">' + routes[i].name + '</a></li>';
                        else
                            html += '<li><a class="route-selector-link disabled" href="#">' + routes[i].name + '</a></li>';
                    }
                }
            }
            else
            {
                html += '<li><a href="#">' + noSavedRoutesText + '</a></li>';
            }
        html += '</ul>';
    
        html += '<a class="route-selector-new-route" href="#">' + newRouteText + '</a>';

        html += '<div class="route-selector-new-route-input-container clearfix">';
            html += '<input type="text" name="routeSelectorNewRouteInput">';
            html += '<span class="route-selector-new-route-input-submit"></span>';
        html += '</div>';

    html += '</div>';

    if( container.find('.route-selector').length == 0 )
    {
        container.append( html );
        var delay;
        delay = setTimeout(function(){
            container.find('.route-selector').addClass('expanded');
            clearTimeout( delay );
        }, 100);
    }
    else
    {
        container.find('.route-selector').remove();
    }
}

function toggleRoutesSelectorAll(e, container) {
    var routes = store.get('oltrepoRoutes');

    var html = '';
    html += '<div class="route-selector" style="left: ' + (e.pageX - 15) +'px; top: ' + e.pageY + 'px;">';
        
        html += '<ul class="route-selector-routes">';
            if(routes)
            {
                if(routes.length > 0)
                {
                    for(var i = 0; i < routes.length; i++)
                    {
                        html += '<li><a class="route-selector-link-all" href="#">' + routes[i].name + '</a></li>';
                    }
                }
            }
            else
            {
                html += '<li><a href="#">' + noSavedRoutesText + '</a></li>';
            }
        html += '</ul>';
    
        html += '<a class="route-selector-new-route" href="#">' + newRouteText + '</a>';

        html += '<div class="route-selector-new-route-input-container clearfix">';
            html += '<input type="text" name="routeSelectorNewRouteInputAll">';
            html += '<span class="route-selector-new-route-input-submit-all"></span>';
        html += '</div>';

    html += '</div>';

    if( container.find('.route-selector').length == 0 )
    {
        container.append( html );
        var delay;
        delay = setTimeout(function(){
            container.find('.route-selector').addClass('expanded');
            clearTimeout( delay );
        }, 100);
    }
    else
    {
        container.find('.route-selector').remove();
    }
}

function updateRouteSelector() {
    var routes = store.get( 'oltrepoRoutes' );
    $('ul.route-selector-routes').find('li').remove();
    for(var i = 0; i < routes.length; i++)
    {
        var canAdd = true;
        for(var j = 0; j < routes[i].wayPoints.length; j++)
        {
            if( routes[i].wayPoints[j].id == newWaypoint.id )
                canAdd = false;
        }
        var linkClass;
        canAdd ? linkClass = 'route-select-link' : linkClass = 'route-select-link disabled';

        $('ul.route-selector-routes').append( '<li><a class="' + linkClass + '" href="#">' + routes[i].name + '</a></li>' );
    }

    $('.route-selector-new-route-input-container').hide();
    $('.route-selector-new-route').show();
}

function saveNewRoute() {
    var routes = store.get( 'oltrepoRoutes' );
    if( routes )
    {
        for(var i = 0; i < routes.length; i++)
            routes[i].selected = false;

        routes.push(
            {
                name: $('input[name="routeSelectorNewRouteInput"]').val(),
                slug: 'test',
                description: '',
                selected: true,
                wayPoints: [newWaypoint]
            }
        );

        store.set( 'oltrepoRoutes', routes);
    }
    else
    {
        store.set( 'oltrepoRoutes',  [
            {
                name: $('input[name="routeSelectorNewRouteInput"]').val(),
                slug: 'test',
                description: '',
                selected: true,
                wayPoints: [newWaypoint]
            }
        ]);
    }

    updateRouteSelector();
}

function addWaypointToRoute(index) {
    var routes = store.get('oltrepoRoutes');
    
    for(var i = 0; i < routes.length; i++)
        routes[i].selected = false;
    
    routes[index].selected = true;
    
    routes[index].wayPoints.push( newWaypoint );
    $('#routeStepsCounter').text( routes[index].wayPoints.length );
    
    store.set('oltrepoRoutes', routes);
}

function setNewWaypoint(caller) {
    newWaypoint = caller.data();

    $.when( 
        $.ajax("/api/argument/" + caller.data().id) 
    )
    .then( 
        function(argument) {
            newWaypoint.name = {
                it: argument.name,
                en: argument.nameEN
            };
            newWaypoint.slug = {
                it: argument.key,
                en: argument.slugEN
            };

            newWaypoint.categories = argument.categories;
        }, 
        function(err) {
            console.log(err);
        }
    );
}

function addAllToRoute(routeIndex) {
    $('.category-add-to-route-link').each(function(i){
        var data = $(this).data();
        $.when( 
            $.ajax( "/api/argument/" + data.id ) 
        )
        .then( 
            function(argument) {
                newWaypoint = data;

                if( currentLanguage == 'it' ) 
                {
                    newWaypoint.name = argument.name;
                    newWaypoint.slug = argument.key;
                }
                else
                {
                    newWaypoint.name = argument.nameEN;
                    newWaypoint.slug = argument.slugEN;
                }

                newWaypoint.categories = argument.categories;

                addWaypointToRoute( routeIndex );
            }, 
            function(err) {
                console.log(err);
            }
        );
    });
}
// Route Editor

function initDefaultRoute() {
    var routes = store.get('oltrepoRoutes');

    if(routes)
    {
        console.log('routes present, skipping', routes);
        if (routes.length == 1) {
            if(routes[0].default)
                $('#routeEditorDeleteRoute').addClass('disabled');
        }
    }
    else
    {
        console.log('no routes', routes);

        routes = [{
            name: yourRouteText,
            description: '',
            selected: true,
            default: true,
            wayPoints: []
        }]

        store.set('oltrepoRoutes', routes);

        $('input[name="routeName"]').val( yourRouteText + ' (default)' );
        $('#routeEditorListSelectRoute').append('<li><a class="route-editor-switch-route-link" href="#">' + yourRouteText + '</a></li>');
        $('#routeEditorDeleteRoute').addClass('disabled');

        console.log('nuovo default creato', routes);
    }

    for(var i in routes) {
        if( routes[i].selected )
            $('#routeStepsCounter').addClass('visible').text( routes[i].wayPoints.length );
    }
}

function initRouteEditor() {
    currentMap = routeEditorMap;

    $('#routeEditorListSelectRoute').html('');

    var routes = store.get('oltrepoRoutes');
    
    if(routes)
    {
        for(var i in routes)
        {
            var routeName = routes[i].name;

            if(routes[i].selected) {
                
                initWaypointsList(i);

                if( routes[i].default )
                {
                    routeName = routes[i].name + ' (default)' ;

                    $('#routeEditorDeleteRoute')
                        .addClass('disabled')
                        .tooltip({
                            title: cantDeleteTooltip
                        });
                }

                $('input[name="routeName"]').val( routeName );
            }

            $('#routeEditorListSelectRoute').append('<li><a class="route-editor-switch-route-link" href="#">' + routeName + '</a></li>');
        }

        $('#routeEditorListNoRoutes').addClass('hidden');
        $('#routeEditorListContainer, #routeEditorListAddStep a').removeClass('hidden');
    }
    else
    {
        // showNoRoutes();
    }
}

function switchActiveRoute(index) {
    var routes = store.get('oltrepoRoutes');

    for(var i in routes)
        routes[i].selected = false;

    routes[index].selected = true;

    if( routes[index].default )
    {
        $('#routeEditorDeleteRoute')
            .addClass('disabled')
            .tooltip({
                title: cantDeleteTooltip
            });
    }
    else
    {
        $('#routeEditorDeleteRoute')
            .removeClass('disabled')
            .tooltip('destroy');
    }


    $('input[name="routeName"]').val( routes[ index ].name );
    if( routes[ index ].default )
    {
        if($('input[name="routeName"]').val().indexOf('(default)') < 0)
            $('input[name="routeName"]').val( routes[ index ].name + ' (default)' );
    }

    store.set('oltrepoRoutes', routes);

    initWaypointsList(index);
}

function initWaypointsList(index) {
    var route = store.get('oltrepoRoutes')[index];
    $('#routeEditorSteps').html('');
    $('#routeEditorMap').find('.marker').remove();
    $('#routeStepsCounter').text( route.wayPoints.length );

    var counter = 1;

    for( var i in route.wayPoints )
    {
        var html = '';
        html += '<li class="route-editor-step">';
            html += '<span class="route-editor-step-tool route-editor-step-number">' + counter + '</span>';
            html += '<span class="route-editor-step-tool reorder-route-editor-step"></span>';
            html += '<span class="route-editor-step-info">';
                
                var argumentName = route.wayPoints[i].name[currentLanguage];
                var argumentSlug = route.wayPoints[i].slug[currentLanguage];

                html += '<a class="route-editor-argument-link" href="/' + currentLanguage + '/' + topicBasePath + '/' + argumentSlug + '">' + argumentName + '</a>';
                html += '<br>';
                for( var j in route.wayPoints[i].categories )
                {
                    var catName, catSlug, color;
                    currentLanguage == 'it' ? catName = route.wayPoints[i].categories[j].name : catName = route.wayPoints[i].categories[j].nameEN;
                    currentLanguage == 'it' ? catSlug = route.wayPoints[i].categories[j].key : catSlug = route.wayPoints[i].categories[j].slugEN;
                    color = route.wayPoints[i].categories[j].color;
                    html += '<a class="route-editor-category-link" style="color: ' + color + ';" href="/' + currentLanguage + '/'  + topicsBasePath + '/' + catSlug + '">';
                        html += catName;
                    html += '</a>'
                }
            html += '</span>';
            html += '<span class="route-editor-step-tool remove-route-editor-step"></span>';
        html += '</li>';

        $('#routeEditorSteps').append( html );

        var el = document.createElement('div');
        
        var elInner = document.createElement('div');
        elInner.className = 'marker-inner';
        elInner.innerHTML = counter;

        if(route.wayPoints[i].categories[0])
        {
            elInner.style.backgroundColor = route.wayPoints[i].categories[0].color;
            
            var categoryClass;
            currentLanguage == 'it' ? categoryClass = route.wayPoints[i].categories[0].key : categoryClass = route.wayPoints[i].categories[0].slugEN;
        }
        else
        {
            elInner.style.backgroundColor = '#ffffff';
            
            var categoryClass;
            currentLanguage == 'it' ? categoryClass = 'senza-categoria' : categoryClass = 'uncategorized';
        }

        el.className = 'marker argument-marker ' + categoryClass;
        el.append( elInner );

        var marker = new mapboxgl.Marker(el, { offset: [-12.5 / 2, -12.5 / 2] });
        marker.setLngLat([ route.wayPoints[i].lng, route.wayPoints[i].lat ]);
        marker.addTo(routeEditorMap);

        counter++;
    }

    if( route.wayPoints.length <= 1 )
    {
        $('#routeEditorMapInfo').fadeOut();

        var layers = currentMap.getStyle().layers;
        for(var i in layers) {
            if(layers[i].id.indexOf('Custom') > -1)
            {
                currentMap.removeLayer( layers[i].id );
                currentMap.removeSource( layers[i].source );
            }
        }

        if( route.wayPoints.length == 0 )
            flyToConfig.center = mapConfig.center;

        if( route.wayPoints.length == 1 )
            flyToConfig.center = [ route.wayPoints[0].lng, route.wayPoints[0].lat ];

        flyToConfig.zoom = mapConfig.zoom;
        routeEditorMap.flyTo( flyToConfig );
    }
    else
    {
        for(var z in currentMap.getStyle().layers)
        {
            if( currentMap.getStyle().layers[z].id.indexOf('Custom') > -1 )
                currentMap.setLayoutProperty(currentMap.getStyle().layers[z].id, 'visibility', 'none');
        }

        fetchEditorRoute(route.wayPoints, 'routeEditorCustomRoute' + index);

        var gmapEndpoint = 'https://www.google.com/maps/dir/?api=1';
            gmapEndpoint += '&origin=' + route.wayPoints[0].lat + ',' + route.wayPoints[0].lng;
            gmapEndpoint += '&destination=' + route.wayPoints[ route.wayPoints.length - 1 ].lat + ',' + route.wayPoints[ route.wayPoints.length - 1 ].lng;
            gmapEndpoint += '&travelmode=driving';
            gmapEndpoint += '&waypoints=';

            for(var h in route.wayPoints)
            {
                gmapEndpoint += route.wayPoints[h].lat + ',' + route.wayPoints[h].lng + '|';
            }

        $('#routeEditorMapInfo').find('a')
            .attr('href', gmapEndpoint)
            .attr('target', 'blank');

        $('.route-editor-personal-route-start').text( route.wayPoints[0].name[currentLanguage] );

        $('#routeEditorMapInfo').fadeIn();

        // if( !routeEditorMap.getLayer('routeEditorCustomRoute' + index) )
        // {
        //     fetchEditorRoute(route.wayPoints, 'routeEditorCustomRoute' + index);
        // }
        // else
        // {
        //     currentMap.setLayoutProperty('routeEditorCustomRoute' + index, 'visibility', 'visible');

        //     var coordinates = currentMap.getSource('routeEditorCustomRoute' + index)._data.geometry.coordinates;

        //     var bounds = coordinates.reduce(function(bounds, coord) {
        //         return bounds.extend(coord);
        //     }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

        //     currentMap.fitBounds(bounds, {
        //         padding: 20
        //     });
        // }
    }
}

function saveRouteName() {
    var routes = store.get('oltrepoRoutes');

    // var currentRouteIndex = 0;
    for(var i in routes)
    {
        if( routes[i].selected ) {
            routes[i].name = $('input[name="routeName"]').val().replace(' (default)', '');
            // currentRouteIndex = i;
        }
    }

    store.set('oltrepoRoutes', routes);
    initRouteEditor();
    // initWaypointsList( currentRouteIndex );
}

function createEmptyRoute() {
    var newRouteName = $('input[name="routeEditorNewRoute"]').val();

    if(newRouteName != '')
    {
        var routes = store.get('oltrepoRoutes');
        console.log(routes);

        var emptyRoute = {
            name: newRouteName,
            slug: 'test',
            description: '',
            selected: true,
            wayPoints: []
        }

        routes.push( emptyRoute );
        console.log(routes);

        store.set('oltrepoRoutes', routes);
        // switchActiveRoute( routes.length - 1 );
        initRouteEditor();

        $('#routeEditorNewRouteInputContainer').hide();
        $('#routeEditorCreateNewRoute').show();
    }
    else
    {
        console.log('no name no route');
    }
}

function removeWaypointFromRoute(index) {
    var routes = store.get('oltrepoRoutes');
    for(var i = 0; i < routes.length; i++)
    {
        if( routes[i].selected )
        {
            routes[i].wayPoints.splice(index, 1);
        }
    }

    store.set( 'oltrepoRoutes', routes);

    initRouteEditor();
}

function deleteActiveRoute() {
    var routes = store.get('oltrepoRoutes');
    
    var delIndex, nextIndex;
    for(var i = 0; i < routes.length; i++)
    {
        if( routes[i].selected )
            delIndex = i;
    }

    routes.splice(delIndex, 1);

    if(routes.length > 0)
    {
        store.set('oltrepoRoutes', routes);

        initRouteEditor();

        routes[0].selected = true;
        switchActiveRoute(0);
    }
    else
    {
        store.remove('oltrepoRoutes');

        $('#routeEditorMap').find('.marker').remove();

        var layers = routeEditorMap.getStyle().layers;
        for(var i in layers) {
            if(layers[i].id.indexOf('Custom') > -1)
            {
                routeEditorMap.removeLayer( layers[i].id );
                routeEditorMap.removeSource( layers[i].source );
            }
        }

        flyToConfig.center = mapConfig.center;
        flyToConfig.zoom = mapConfig.zoom;
        routeEditorMap.flyTo( flyToConfig );

        showNoRoutes();
    }
}

function hideNoRoutes() {
    $('#routeEditorListNoRoutes').addClass('hidden');
    $('#routeEditorListContainer, #routeEditorListAddStep a').removeClass('hidden');
}

function showNoRoutes() {
    $('input[name="routeEditorNewRoute"], a.route-editor-submit-new-route-name').remove();
    $('#routeEditorListNoRoutes').removeClass('hidden');
    $('#routeEditorCreateNewRoute').removeClass('disabled');
    $('#routeEditorListContainer, #routeEditorListAddStep a').addClass('hidden');
}

function initDraggables() {
    var el = document.getElementById( 'routeEditorSteps' );
    var sortable = Sortable.create(el, {
        handle: '.reorder-route-editor-step',
        onEnd: function(e){
            var routes = store.get( 'oltrepoRoutes' );

            if( routes )
            {
                for(var i in routes)
                {
                    if(routes[i].selected)
                    {
                        arraymove( routes[i].wayPoints, e.oldIndex, e.newIndex );
                        store.set( 'oltrepoRoutes', routes );
                        initWaypointsList(i);
                    }
                }
            }
        }
    });
}

function saveRouteInApp(token) {
    // console.log( token );
    $('#saveInAppModalPreloader').show();
    $('#routeEditorFeedbackContainer').addClass('hidden');

    console.log( 'send post request with route data and recaptcha token' );

    var data = { token: token }

    var routes = store.get('oltrepoRoutes');

    for(var i in routes)
    {
        if( routes[i].selected )
        {
            data.route = routes[i];
        }
    }
    
    $.ajax({
        method: "POST",
        url: "/api/new-route",
        data: data
    })
    .done(function( data ) {
        console.log( "Data Saved" );

        var routePath = '/itinerario/id/';
        if (currentLanguage == 'en')
            routePath = '/route/id/';

        var shareUrl = window.location.protocol + '//' + window.location.host + '/' + currentLanguage + routePath + data._id ;

        $('#saveInAppModalPreloader').hide();
        $('#getRouteCode').attr('disabled', 'disabled');
        grecaptcha.reset();

        initSaveInAppInfo( data );

        $('.save-in-app-social-icon').attr('data-url', shareUrl);

        $('#saveInAppModalDownloadPdf').attr('data-routeurl', '/' + currentLanguage + routePath + data._id);
        $('#saveInAppModalDownloadPdf').attr('href', '/pdf/' + data._id + '.pdf');

        $('#saveInAppModalViewRoutePage').attr('href', shareUrl).prop("disabled", false).removeAttr('disabled');

        prebuildPDF();

        $('.save-in-app-modal-col#form').hide();
        $('.save-in-app-modal-col#info').fadeIn();
    });
}

function saveInAppReCaptchaCallBack(msg) { 
    // console.log( msg );
    if( $('#getRouteCode').length > 0 )
        $('#getRouteCode').remove();
    
    var html = '';
    html += '<button type="submit" class="btn btn-default" id="getRouteCode">';
        html += getRouteCode;
    html += '</button>';

    $('#form').find('form').find('.form-group').after( html );
}

function initSaveInAppInfo( data ) {
    $('#saveInAppModalSendByEmail').attr('data-routeID', data._id);
    $('#routeNameHeading').text( data.name );
    $('#saveInAppModalRouteID').text( data.appCode );
}

// function initRouteEditorModalSocialShare() {
//     $('.save-in-app-social-icon').share();
// }

function prebuildPDF() {
    console.log( 'prebuildPDF' );
    $.get( $('#saveInAppModalDownloadPdf').data().routeurl, function( data ) {
        if(data)
            $('#saveInAppModalDownloadPdf').removeAttr('disabled');
    });
}
function initRoutePageSocialShare() { 
    $('.route-page-social-icon').share();
}

function addMarkersToRoutePageMap() {
    $('.route-step ').each(function(i) {
        var location = {
            lat: $(this).data().lat,
            lng: $(this).data().lng
        }

        var el = document.createElement('div');
        el.className = 'marker route-page-marker';
        el.style.backgroundColor = $(this).data().color;
        el.innerHTML = i+1;

        var marker = new mapboxgl.Marker(el, { offset: [-12.5 / 2, -12.5 / 2] });
        marker.setLngLat( location );
        marker.addTo( routePageMap );
    });
    // $('.category-argument-card').each(function(i) {
    //     var id = $(this).data().id;

    //     var location = {
    //         lat: $(this).data().lat,
    //         lng: $(this).data().lng
    //     }

    //     var el = document.createElement('div');
    //     el.className = 'marker argument-marker';
    //     el.id = 'marker' + id;
    //     el.style.backgroundColor = $(this).data().color;

    //     el.addEventListener('click', function(e) {
    //         resetFilters();

    //         flyToConfig.center = location;
    //         flyToConfig.zoom = 14;
    //         categoryMap.flyTo( flyToConfig );

    //         filterCategoryCardsByID( id );
    //     });

    //     var DOMel = $('<div>')
    //         .addClass('popup-container category-popup-container')
    //         .text( $(this).find('h5 a').text() );

    //     var popup = new mapboxgl.Popup( { offset: 7.5 } );
    //     popup.setDOMContent( DOMel[0] );
    //     popups.push( popup );

    //     var marker = new mapboxgl.Marker(el, { offset: [-12.5 / 2, -12.5 / 2] });
    //     marker.setLngLat( location );
    //     marker.setPopup( popup );
    //     marker.addTo( categoryMap );
    // });
}
// Routing

function fetchRouteData(startLocation, routeId, containers) {
    console.log(routeId);
    if( mapConfig.center[0] == '' || mapConfig.center[1] == '') { return; }
    
    var center = mapConfig.center;
    $.when( 
        $.ajax({
            url: 'https://api.mapbox.com/directions/v5/mapbox/driving-traffic/' + startLocation[0] + '%2C' + startLocation[1] + '%3B' + center[0] + '%2C' + center[1] + '.json',
            data: {
                geometries: 'geojson',
                alternatives: false,
                steps: true,
                overview: 'full',
                language: 'it',
                access_token: mapboxgl.accessToken
            }
        })
    )
    .then( 
        function(route) {
            renderRouteLayer( routeId, route );
            if(containers)
            {
                renderRouteData( route.routes[0], containers.routeDataContainer );
                renderRouteSteps( route.routes[0].legs[0].steps, containers.routeStepsContainer );
            }

            if(routeId.indexOf('Custom') > -1) {
                currentMap.setLayoutProperty(routeId, 'visibility', 'visible');

                var coordinates = currentMap.getSource(routeId)._data.geometry.coordinates;

                var bounds = coordinates.reduce(function(bounds, coord) {
                    return bounds.extend(coord);
                }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

                currentMap.fitBounds(bounds, {
                    padding: 20
                });
            }
        }, 
        function(err) {
            console.log(err)
        }
    );
}

function fetchEditorRoute(wayPoints, routeId) {
    if(wayPoints.length <= 20)
    {
        var baseEndpoint = 'https://api.mapbox.com/directions/v5/mapbox/driving/';
        var coordinates = '';
        for(var i in wayPoints)
        {
            coordinates += wayPoints[i].lng + '%2C' + wayPoints[i].lat + '%3B';
        }
        coordinates = coordinates.slice(0, -3);
        coordinates += '.json';

        $.when( 
            $.ajax({
                url: baseEndpoint + coordinates,
                data: {
                    geometries: 'geojson',
                    alternatives: false,
                    steps: false,
                    overview: 'full',
                    language: 'it',
                    access_token: mapboxgl.accessToken
                }
            })
        )
        .then( 
            function(route) {
                renderRouteLayer( routeId, route );
                var km = route.routes[0].distance / 1000;
                $('.route-editor-personal-route-duration').text( km.toFixed(2) + ' Km.' );

                if(routeId.indexOf('Custom') > -1) {
                    currentMap.setLayoutProperty(routeId, 'visibility', 'visible');

                    var coordinates = currentMap.getSource(routeId)._data.geometry.coordinates;

                    var bounds = coordinates.reduce(function(bounds, coord) {
                        return bounds.extend(coord);
                    }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

                    currentMap.fitBounds(bounds, {
                        padding: 20
                    });
                }
            }, 
            function(err) {
                console.log(err.responseText)
            }
        );
    }
    else
    {
        console.log('Non più di 20 waypoints', wayPoints.length);
    }
}

function fetchRoutePageData(startLocation, endLocation, routeId, containers) {
    $.when( 
        $.ajax({
            url: 'https://api.mapbox.com/directions/v5/mapbox/driving-traffic/' + startLocation[0] + '%2C' + startLocation[1] + '%3B' + endLocation[0] + '%2C' + endLocation[1] + '.json',
            data: {
                geometries: 'geojson',
                alternatives: false,
                steps: true,
                overview: 'full',
                language: 'it',
                access_token: mapboxgl.accessToken
            }
        })
    )
    .then( 
        function(route) {
            renderRouteLayer( routeId, route );
            if(containers)
            {
                renderRouteData( route.routes[0], containers.routeDataContainer );
                renderRouteSteps( route.routes[0].legs[0].steps, containers.routeStepsContainer );
            }

            if(routeId.indexOf('Custom') > -1) {
                currentMap.setLayoutProperty(routeId, 'visibility', 'visible');

                var coordinates = currentMap.getSource(routeId)._data.geometry.coordinates;

                var bounds = coordinates.reduce(function(bounds, coord) {
                    return bounds.extend(coord);
                }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

                currentMap.fitBounds(bounds, {
                    padding: 20
                });
            }
        }, 
        function(err) {
            console.log(err)
        }
    );
}

function renderRouteLayer(routeId, route) {
    if( currentMap.getLayer( routeId ) )
        currentMap.removeLayer( routeId );

    if( currentMap.getSource( routeId ) )
        currentMap.removeSource( routeId );

    currentMap.addLayer({
        "id": routeId,
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": route.routes[0].geometry
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#888",
            "line-width": 4
        }
    });

    currentMap.setLayoutProperty(routeId, 'visibility', 'none');
}

function renderRouteData(route, container) {
    var duration, routeDuration;
    var hours, minutes, seconds;

    duration = route.duration;

    if( duration < 3600 )
    {
        minutes = Math.floor(duration / 60);
        seconds = Math.floor(duration % 60);

        routeDuration = str_pad_left( minutes, '0', 2 ) + ' min';
    }
    else
    {
        hours = Math.floor(duration / 3600);
        duration %= 3600;
        minutes = Math.floor(duration / 60);
        routeDuration = str_pad_left( hours, '0', 2 ) + ' h, ' + str_pad_left( minutes, '0', 2 ) + ' min';
    }

    var routeDistance = route.distance / 1000;

    var html = '<br>';
    html += '<span>';
        html += routeDuration;
        html += ' - (' + routeDistance.toFixed(2) + ' km)';
    html += '</span>';

    container.find('a.route-toggle-steps').append( html );
}

function renderRouteSteps(steps, container) {
    for(var i = 0; i < steps.length; i++) {
        var instruction = steps[i].maneuver.instruction;
        
        var typeClass = '';
        if(steps[i].maneuver.type)
            typeClass = steps[i].maneuver.type.replace(/ /g, '_');
        
        var modifierClass = '';
        if(steps[i].maneuver.modifier)
            modifierClass = '_' + steps[i].maneuver.modifier.replace(/ /g, '_')

        var minutes = Math.floor(steps[i].duration / 60);
        var seconds = Math.floor(steps[i].duration % 60);
        var stepDuration = str_pad_left( minutes, '0', 2 ) + ':' + str_pad_left( seconds, '0', 2 );

        var stepDistance = steps[i].distance;

        var html = '';
        html += '<li class="route-step">';
            html += '<span class="route-step-icon ' + typeClass + modifierClass + '"></span>';
            html += '<a class="route-step-link" href="#"><strong>' + instruction + '</strong>';
                html += ' <span class="route-step-duration"> - ' + stepDuration + ' min.</span> - ';
                html += ' <span class="route-step-distance">(' + stepDistance.toFixed(2) + ' mt)</span>';
            html += '</a>';
        html += '</li>';

        container.append( html );
    }
}

function translateInstruction(instruction) {}
// Sliders

function sliderNavigation() {
    $('body').off('click', '.slider-nav-control', sliderNavigation);

    var dir;
    $(this).hasClass('slide-right') ? dir = -1 : dir = 1;

    var cardW = $('.featured-card').width();
    var destPos = $('#featuredSlider').position().left + ( cardW * dir );

    var maxPos = cardW * ($('.featured-card').length - 3)

    $('#featuredSlider').css({ left: destPos + 'px' });

    if(destPos < 0) { $('.slide-left').show(); }
    if(destPos > -maxPos) { $('.slide-right').show(); }

    if(destPos == -maxPos) { $('.slide-right').hide(); }
    if(destPos == 0) { $('.slide-left').hide(); }
}

function videoGalleryNavigation() {
    $('body').off('click', '.video-gallery-nav-control', sliderNavigation);

    var dir;
    $(this).hasClass('video-gallery-slide-right') ? dir = -1 : dir = 1;

    var cardW = $('.video-gallery-card').width();
    var destPos = $('#videoGallerySlider').position().left + ( cardW * dir );

    var maxPos = cardW * ($('.video-gallery-card').length - 1)

    $('#videoGallerySlider').css({ left: destPos + 'px' });

    if(destPos < 0) { $('.video-gallery-slide-left').show(); }
    if(destPos > -maxPos) { $('.video-gallery-slide-right').show(); }

    if(destPos == -maxPos) { $('.video-gallery-slide-right').hide(); }
    if(destPos == 0) { $('.video-gallery-slide-left').hide(); }
}

function argumentSliderNavigation() {
    $('body').off('click', '.argument-featured-nav', argumentSliderNavigation);

    var dir;
    $(this).attr('id').indexOf('Right') > -1 ? dir = -1 : dir = 1;

    var cardW = $('.argument-featured-card').width() + 15;
    var destPos = ( $('#argumentFeaturedSlider').position().left - 15 ) + ( cardW * dir );

    var maxPos = cardW * ( $('.argument-featured-card').length - 4 );

    $('#argumentFeaturedSlider').css({ left: destPos + 'px' });

    if(destPos < 0) { $('#argumentFeaturedSliderNavLeft').show(); }
    if(destPos > -maxPos) { $('#argumentFeaturedSliderNavRight').show(); }

    if(destPos == -maxPos) { $('#argumentFeaturedSliderNavRight').hide(); }
    if(destPos == 0) { $('#argumentFeaturedSliderNavLeft').hide(); }
}

function townRelatedSliderNavigation() {
    $('body').off('click', '.town-related-nav', argumentSliderNavigation);

    var dir;
    $(this).attr('id').indexOf('Right') > -1 ? dir = -1 : dir = 1;

    var cardW = $('.town-related-card').width() + 15;
    var destPos = ( $('#townRelatedSlider').position().left - 15 ) + ( cardW * dir );

    var maxPos = cardW * ( $('.town-related-card').length - 4 );

    $('#townRelatedSlider').css({ left: destPos + 'px' });

    if(destPos < 0) { $('#townRelatedSliderNavLeft').show(); }
    if(destPos > -maxPos) { $('#townRelatedSliderNavRight').show(); }

    if(destPos == -maxPos) { $('#townRelatedSliderNavRight').hide(); }
    if(destPos == 0) { $('#townRelatedSliderNavLeft').hide(); }
}
// UI Adjustments

function setVisibleCards(type) {
    var numVisibleCards;
    var w = $(window).width();

    switch(type) {
        case 'home-featured':
            w <= 768 ? numVisibleCards = 1 : numVisibleCards = 3;
            break;
        case 'home-video-gallery':
            numVisibleCards = 1;
            break;
        case 'town-related':
            if( w <= 320 )
                numVisibleCards = 1;
            if( w > 320 && w <= 768 )
                numVisibleCards = 2;
            if( w > 768 )
                numVisibleCards = 4;
            break;
        case 'argument-featured':
            if( w <= 320 )
                numVisibleCards = 1;
            if( w > 320 && w <= 768 )
                numVisibleCards = 2;
            if( w > 768 )
                numVisibleCards = 4;
            break;
    }

    return numVisibleCards;
}

function initHomeFeaturedSlider() {
    var numVisibleCards = setVisibleCards('home-featured');

    $('#featuredContainer').css({ opacity: 0 });
    $('#featuredSlider').css({ left: 0 });
    $('.slide-left').hide();

    var containerW = $('#featuredContainer').width();
    var cardW = containerW / numVisibleCards;
    var sliderW = cardW * $('.featured-card').length;

    $('.featured-card').width( cardW );
    $('#featuredSlider').width( sliderW );

    if( $('.featured-card').length <= numVisibleCards )
        $('.slider-nav-control').hide();

    $('.slider-nav-control').css({
        top: - ( $('#featuredContainer').height() / 2 + $('.slider-nav-control').height() / 2 ) + 'px'
    })

    $('#featuredContainer').css({ opacity: 1 });
}

function initHomeVideoGallery() {
    var numVisibleCards = setVisibleCards('home-video-gallery');

    $('#videoGalleryContainer').css({ opacity: 0 });
    $('#videoGallerySlider').css({ left: 0 });
    $('.video-gallery-slide-left').hide();

    var containerW = $('#videoGalleryContainer').width();
    var cardW = containerW / numVisibleCards;
    var sliderW = cardW * $('.video-gallery-card').length;

    $('.video-gallery-card').width( cardW );
    $('#videoGallerySlider').width( sliderW );

    if( $('.video-gallery-card').length <= numVisibleCards )
        $('.video-gallery-nav-control').hide();

    $('.video-gallery-nav-control').css({
        top: - ( $('#videoGalleryContainer').height() / 2 + $('.video-gallery-nav-control').height() / 2 ) + 'px'
    })

    $('#videoGalleryContainer').css({ opacity: 1 });
}

function initTownDescription() {
    // if( $('#townDescription').hasClass('town-video-cover-description') )
    // {
    //     $('#townDescription').css({
    //         top: - $('#townDescription').outerHeight() + 'px'
    //     });
    // }
    // else
    // {
        $('#townDescription').css({
            // width: $('#townArguments').width() + 15 + 'px'
            width: $('#townArguments').width() + 'px'
        });
        $('#townDescription').css({
            marginTop: - $('#townDescription').outerHeight() + 'px',
        });
    // }
    $('#townDescription').fadeTo(500, 1);
}

function initTownArgumentInfo() {
    $('.town-argument-info').css( 'height', $('.town-argument-img').height() + 'px' );
}

function initArgumentFeaturedSlider() {
    var numVisibleCards = setVisibleCards('argument-featured');

    $('#argumentFeaturedSliderContainer').css({ opacity: 0 });
    $('#argumentFeaturedSlider').css({ left: 0 });
    $('#argumentFeaturedSliderNavLeft').hide();

    var containerW = $('#argumentFeaturedSliderContainer').width();
    var cardW = containerW / numVisibleCards - 15;
    var sliderW = cardW * $('.argument-featured-card').length + (15 * $('.argument-featured-card').length);

    $('.argument-featured-card').width( cardW );
    $('#argumentFeaturedSlider').width( sliderW );

    if( $('.argument-featured-card').length <= numVisibleCards )
        $('.argument-featured-nav').hide();

    $('#argumentFeaturedSliderContainer').css({ opacity: 1 });
}

function initTownRelatedSlider() {
    var numVisibleCards = setVisibleCards('town-related');

    $('#townRelatedSliderContainer').css({ opacity: 0 });
    $('#townRelatedSlider').css({ left: 0 });
    $('#townRelatedSliderNavLeft').hide();

    var containerW = $('#townRelatedSliderContainer').width();
    var cardW = containerW / numVisibleCards - 15;
    var sliderW = cardW * $('.town-related-card').length + (15 * $('.town-related-card').length);

    $('.town-related-card').width( cardW );
    $('#townRelatedSlider').width( sliderW );

    if( $('.town-related-card').length <= numVisibleCards )
        // console.log( $('.town-related-card').length )
        $('.town-related-nav').hide();

    $('#townRelatedSliderContainer').css({ opacity: 1 });
}

function initContribsContainers() {
    var cardW = $('.didactics-argument-card-inner').width();
    $('.didactics-argument-contribs-container').outerWidth( cardW - 30 );
}
// Utils

function getArgumentName(arg) {
    var name;
    currentLanguage == 'it' ? name = arg.name : name = arg.nameEN;
    return name;
}
function getArgumentSlug(arg) {
    var slug;
    currentLanguage == 'it' ? slug = arg.key : slug = arg.slugEN;
    return slug;
}

function getCategoryName(cat) {
    var name;
    currentLanguage == 'it' ? name = cat.name : name = cat.nameEN;
    return name;
}
function getCategorySlug(cat) {
    var slug;
    currentLanguage == 'it' ? slug = cat.key : slug = cat.slugEN;
    return slug;
}

function str_pad_left(string,pad,length) { return (new Array(length+1).join(pad)+string).slice(-length); }

function arraymove(arr, fromIndex, toIndex) {
    var element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
}