/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

var Handlebars = require('handlebars');

// Common Middleware
keystone.pre('routes', middleware.initErrorHandlers);
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Handle 404 errors
keystone.set('404', function(req, res, next) {
    res.notfound('404', 'Sorry, the page you requested can\'t be found.');
});

// Handle other errors
keystone.set('500', function(err, req, res, next) {
    var title, message;
    if (err instanceof Error) {
        message = err.message;
        err = err.stack;
    }
    res.err(err, title, message);
});

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
};

// Setup Route Bindings
exports = module.exports = function (app) {

	Handlebars.registerHelper('localize', function(obj, lang) { 
		return new Handlebars.SafeString( obj[lang] ); 
	});

	Handlebars.registerHelper('translate', function(obj, lang) { 
		return new Handlebars.SafeString( obj[lang] ); 
	});

	Handlebars.registerHelper('hasDeepeningsClassName', function(obj) { 
		if(obj.deepenings.length > 0)
			return 'col-md-7 col-md-offset-1';
		else
			return 'col-md-8 col-md-offset-2'
	});

	Handlebars.registerHelper("inc", function(value, options) {
	    return parseInt(value) + 1;
	});

	// Home
	app.get('/', middleware.setLocalizedRoot, routes.views.index);
	app.get('/app', function(req, res){ res.redirect('/it/app'); });
	app.get('/:lang', routes.views.index);

	// Town
	app.get('/:lang/comune/:slug', routes.views.town);
	app.get('/:lang/town/:slug', routes.views.town);

	// Topic
	app.get('/:lang/argomento/:slug', routes.views.argument);
	app.get('/:lang/topic/:slug', routes.views.argument);
	
	// Topics
	app.get('/:lang/argomenti', routes.views.arguments);
	app.get('/:lang/topics', routes.views.arguments);
	app.get('/:lang/argomenti/:cat', routes.views.arguments);
	app.get('/:lang/topics/:cat', routes.views.arguments);

	// Route
	app.all('/:lang/itinerario/:slug', routes.views.route);
	app.all('/:lang/route/:slug', routes.views.route);
	app.all('/:lang/itinerario/id/:id', routes.views.route);
	app.all('/:lang/route/id/:id', routes.views.route);
	app.get('/:lang/itinerario/:slug/:nowebgl', routes.views.route);
	app.get('/:lang/route/:slug/:nowebgl', routes.views.route);
	app.all('/:lang/itinerario/id/:id/:nowebgl', routes.views.route);
	app.all('/:lang/route/id/:id/:nowebgl', routes.views.route);

	// Routes
	app.get('/:lang/itinerari', routes.views.routes);
	app.get('/:lang/routes', routes.views.routes);
	app.get('/:lang/itinerari/:filter', routes.views.routes);
	app.get('/:lang/routes/:filter', routes.views.routes);

	// Learning section
	app.get('/:lang/didattica', routes.views.didactics);
	app.get('/:lang/didactics', routes.views.didactics);
	app.get('/:lang/didattica/argomento/:slug', routes.views.argument);
	app.get('/:lang/didactics/topic/:slug', routes.views.argument);
	app.get('/:lang/didattica/argomento/:slug/:nowebgl', routes.views.argument);
	app.get('/:lang/didactics/topic/:slug/:nowebgl', routes.views.argument);
	
	// Statics
	app.get('/:lang/about', routes.views.about);
	app.get('/:lang/app', routes.views.app);

	// API
	app.get('/api/towns', keystone.middleware.cors, routes.views.townsAPI);
	app.get('/api/arguments', keystone.middleware.cors, routes.views.argumentsAPI);
	app.get('/api/argument/:id', keystone.middleware.cors, routes.views.argumentAPI);
	app.get('/api/customroute/:appCode', keystone.middleware.cors, routes.views.customRouteAPI);

	app.post('/api/new-route', routes.views.newRouteAPI);

	// app.all('/api*', keystone.middleware.cors);
	// app.options('/api*', function(req, res) {
	// 	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-XSRF-TOKEN');
	// 	res.sendStatus(200);
	// });
	// app.all('/:lang/contact', routes.views.contact);

	// app.get('/.well-known/acme-challenge/:content', function(req, res) {
	// 	res.send('EaaI5nSrYBbSJTAi2Ki-Jywh1RuVgf6v_KIITg5EB9s.Af7IBC8MwWSHq9RT89yFJS12W8zKEsFgo02IZq04JF8')
	// })

	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);

};
