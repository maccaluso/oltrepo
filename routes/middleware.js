	/**
	 * This file contains the common middleware used by your routes.
	 *
	 * Extend or replace these functions as your application requires.
	 *
	 * This structure is not enforced, and just a starting point. If
	 * you have more middleware you may want to group it as separate
	 * modules in your project's /lib directory.
	 */
	var _ = require('lodash');

	var keystone = require('keystone');
	var Category = keystone.list('Category');

	var translations = require('./translations');

	/**
		Initialises the standard view locals

		The included layout depends on the navLinks array to generate
		the navigation in the header, you may wish to change this array
		or replace it with your own templates / logic.
	*/
	exports.initLocals = function (req, res, next) {
		// res.locals.navLinks = [
		// 	{ label: 'Home', key: 'home', href: '/' },
		// 	{ label: 'Gallery', key: 'gallery', href: '/gallery' },
		// 	{ label: 'Contact', key: 'contact', href: '/contact' },
		// ];
		res.locals.prodEnv = false;
		if( process.env.NODE_ENV == 'production' )
			res.locals.prodEnv = true

		var q = Category.model.find();

		q.exec(function (err, result) {
			// console.log('query ran...')
			res.locals.user = req.user;
			res.locals.categories = result;
			res.locals.translations = translations;

			res.locals.isDidactics = false;
			if( req.path.indexOf('didattica') > 0 || req.path.indexOf('didactics') > 0 )
				res.locals.isDidactics = true;
			// console.log( JSON.stringify( result, null, 2 ) );
			next();
		});
	};

	/**
	    Inits the error handler functions into `res`
	*/
	exports.initErrorHandlers = function(req, res, next) {
	    
	    res.err = function(err, title, message) {
	        res.status(500).render('errors/500', {
	            err: err,
	            errorTitle: title,
	            errorMsg: message
	        });
	    }
	    
	    res.notfound = function(title, message) {
	        res.status(404).render('errors/404', {
	            errorTitle: title,
	            errorMsg: message
	        });
	    }
	    
	    next();
	    
	};


	/**
		Fetches and clears the flashMessages before a view is rendered
	*/
	exports.flashMessages = function (req, res, next) {
		var flashMessages = {
			info: req.flash('info'),
			success: req.flash('success'),
			warning: req.flash('warning'),
			error: req.flash('error'),
		};
		res.locals.messages = _.some(flashMessages, function (msgs) { return msgs.length; }) ? flashMessages : false;
		next();
	};


	/**
		Prevents people from accessing protected pages when they're not signed in
	 */
	exports.requireUser = function (req, res, next) {
		if (!req.user) {
			req.flash('error', 'Please sign in to access this page.');
			res.redirect('/keystone/signin');
		} else {
			next();
		}
	};


	/**
		Language based root redirect
	 */
	exports.setLocalizedRoot = function (req, res, next) {
		// console.log(res.cookies.language);
		res.redirect('/it');
		//next();
		// if (!req.user) {
		// 	req.flash('error', 'Please sign in to access this page.');
		// 	res.redirect('/keystone/signin');
		// } else {
		// 	next();
		// }
	};

	// exports.isDidactics = function (req, res, next) {
	// 	if( req.path.indexOf('didattica') > 0 )
	// 		res.locals.isDidactics = true;
	// 	next();
	// }

	// exports.checkENV = function(req, res, next) {
	// 	console.log('checkENV');
	// 	next();
	// };