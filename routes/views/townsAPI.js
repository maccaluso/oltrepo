var keystone = require('keystone');
var Town = keystone.list('Town');

exports = module.exports = function (req, res) {

	var q = Town.model.find()
		.populate('location routingStartDefault arguments relatedTowns')
		.deepPopulate('arguments.categories');

	q.exec(function (err, result) {
		res.setHeader('Content-Type', 'application/json');
		res.send( JSON.stringify( result, null, 2 ) );
	});
};
