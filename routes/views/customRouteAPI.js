var keystone = require('keystone');
var Route = keystone.list('Route');

exports = module.exports = function (req, res) {
	var q = Route.model.findOne()
				.where({ 'appCode': req.params.appCode })
				.populate('arguments')
				.deepPopulate('arguments.location');

	q.exec(function (err, result) {
		res.setHeader('Content-Type', 'application/json');
		res.send( JSON.stringify( result, null, 2 ) );
	});
};
