var keystone = require('keystone');
var Town = keystone.list('Town');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'comune';
	locals.slug = req.params.slug;
	locals.lang = req.params.lang;
	locals.basePathIT = 'comune';
	locals.basePathEN = 'town';

	view.on('init', function (next) {

		var q = Town.model.findOne({
			key: locals.slug,
		}).populate('location routingStartDefault arguments relatedTowns')
		.deepPopulate('arguments.categories arguments.location');

		q.exec(function (err, result) {
			locals.slugIT = result.key;
			locals.slugEN = result.key;
			locals.town = result;

			locals.hasRelated = false;
			if(result.relatedTowns.length > 0)
				locals.hasRelated = true;

			next(err);
		});

	});

	// Render the view
	view.render('town');

};
