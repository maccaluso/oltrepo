var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.lang = req.params.lang;
	locals.slugIT = locals.slugEN = 'about';

	// Set locals
	locals.section = 'about';

	// Render the view
	view.render('about');

};
