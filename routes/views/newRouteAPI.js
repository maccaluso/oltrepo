var querystring = require('querystring');
var keystone = require('keystone');
var Route = keystone.list('Route');

// var requestify = require('requestify');
var axios = require('axios');

exports = module.exports = function (req, res) {

	var token = req.body.token;
	// var secretKey = '6LeulzoUAAAAABAMZ4aDAWllhsukTQqqIrsxRERh';
	var secretKey = '6Lc5EXoUAAAAACJ2eiBBrtoXQXA4kM7Qr4gRDNYW';
	var ip = req.connection.remoteAddress;

	// var endpoint = 'https://www.google.com/recaptcha/api/siteverify?secret=' + secretKey + '"&response=' + token + '&remoteip=' + ip;
	var endpoint = 'https://www.google.com/recaptcha/api/siteverify';

	axios.post(endpoint, querystring.stringify({
		secret: secretKey,
		response: token
	  }))
	  .then(function (response) {
		if( response.data.success )
		{
			var data = req.body.route;
			data.arguments = [];
			for(var i = 0; i < data.wayPoints.length; i++)
			{
				data.arguments.push( keystone.mongoose.Types.ObjectId( data.wayPoints[i].id ) );
			}

			var newRoute = new Route.model( data );

			newRoute.save(function(err, route) {
				if(err) {
					console.log('error', err);
				}
				else
				{
					res.send( route );
				}
			});
		}
		else
		{
			res.send('invalid recaptcha from server side', response);
		}
	  })
	  .catch(function (error) {
		res.send(error);
	  });
};
