var keystone = require('keystone');
var Route = keystone.list('Route');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'routes';
	locals.basePathEN = 'routes';
	locals.basePathIT = 'itinerari';
	locals.slugEN = 'featured';
	locals.slugIT = 'suggeriti';
	locals.lang = req.params.lang;
	locals.filter = req.params.filter;
	locals.shareUrlBase = req.protocol + '://' + req.get('host') + '/' + locals.lang;

	// Load the galleries by sortOrder
	// view.query('galleries', keystone.list('Gallery').model.find().sort('sortOrder'));
	view.query(
		'featuredVideos', 
		keystone.list('FeaturedVideo')
			.model.find()
			.where('showInHome', true)
			.sort('sortOrder')
			.populate('categories towns')
	);

	view.query(
		'featuredRoutes', 
		keystone.list('Route')
			.model.find()
			.where('isFeatured', true)
			.populate('arguments')
			.deepPopulate('arguments.location arguments.categories')
	);

	// view.on('init', function (next) {
	// 	var q;
	// 	// if(req.params.lang == 'it')
	// 	// {
	// 	// 	q = Route.model.findOne({
	// 	// 		key: locals.slug,
	// 	// 	}).populate('arguments')
	// 	// 	.deepPopulate('arguments.location arguments.categories');
	// 	// }

	// 	// if(req.params.lang == 'en')
	// 	// {
	// 	// 	q = Route.model.findOne({
	// 	// 		slugEN: locals.slug,
	// 	// 	}).populate('arguments')
	// 	// 	.deepPopulate('arguments.location arguments.categories');
	// 	// }

	// 	q = Route.model.find().populate('arguments')
	// 		.where({ isFeatured: true })
	// 		.deepPopulate('arguments.location arguments.categories');

	// 	q.exec(function (err, result) {
	// 		if(result)
	// 		{
	// 			// locals.slugIT = result.key;
	// 			// locals.slugEN = result.slugEN;
	// 			// locals.route = result;
	// 			locals.featuredRoutes = result;
	// 		}

	// 		next(err);
	// 	});

	// 	// console.log(req.message)
	// });

	// Render the view
	view.render('routes');

};
