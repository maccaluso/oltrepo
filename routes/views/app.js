var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.lang = req.params.lang;
	locals.slugIT = locals.slugEN = 'app';

	// Set locals
	locals.section = 'app';

	// Render the view
	view.render('app');

};
