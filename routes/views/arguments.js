var keystone = require('keystone');
var Argument = keystone.list('Argument');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.lang = req.params.lang;
	locals.isFiltered = false;

	if(req.params.cat)
	{
		locals.cat = req.params.cat;
		locals.isFiltered = true;
		locals.filterString = locals.cat;
	}

	locals.basePathIT = 'argomenti';
	locals.basePathEN = 'topics';

	if(locals.lang == 'it') {
		for(var i = 0; i < locals.categories.length; i++)
		{
			if(locals.categories[i].key == locals.cat)
			{
				locals.slugIT = locals.categories[i].key;
				locals.slugEN = locals.categories[i].slugEN;

				locals.catName = locals.categories[i].name;
				locals.catColor = locals.categories[i].color;
			}
		}
	}
	else
	{
		for(var j = 0; j < locals.categories.length; j++)
		{
			if(locals.categories[j].slugEN == locals.cat)
			{
				locals.slugIT = locals.categories[j].key;
				locals.slugEN = locals.categories[j].slugEN;

				locals.catName = locals.categories[j].nameEN;
				locals.catColor = locals.categories[j].color;
			}
		}
	}

	if(!locals.catColor) { locals.catColor = '#ffffff'; }

	view.on('init', function (next) {
		var q = Argument.model.find().populate('location routingStartDefault categories related towns deepenings links');

		q.exec(function (err, result) {
			locals.arguments = result;
			next(err);
		});
	});

	// Render the view
	view.render('arguments');
};
