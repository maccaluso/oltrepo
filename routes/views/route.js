var keystone = require('keystone');
var Route = keystone.list('Route');

var pdf = require('html-pdf');
var requestify = require('requestify');
var path = require('path');

var mailgun = require('mailgun-js')({
	apiKey: process.env.MAILGUN_API_KEY || 'key-36b178172aff120644ffbdccd26e3bcf', 
	domain: process.env.MAILGUN_DOMAIN || 'sandboxad50f20f1710432ea4da00fb3a2ed361.mailgun.org'
});
var handlebars = require('handlebars');
var fs = require('fs');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'route';
	locals.lang = req.params.lang;
	locals.slug = req.params.slug;
	locals.basePathIT = 'itinerario';
	locals.basePathEN = 'route';

	if( req.params.nowebgl == 'on' )
		locals.nowebgl = true

	locals.mapboxAccessToken = 'pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6IjNLLVM5a0UifQ.fQiOn9nvARyZ80kya3yfjA';
	locals.mapboxStaticDefault = 'https://api.mapbox.com/styles/v1/maccaluso/cj5wksxrn7jau2rmkmryvi5co/static/11.07486879208193,45.00679160929593,7,20/150x150@2x?access_token=pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6IjNLLVM5a0UifQ.fQiOn9nvARyZ80kya3yfjA';
	locals.mapboxStaticRoute = 'https://api.mapbox.com/styles/v1/maccaluso/cj5wksxrn7jau2rmkmryvi5co/static/11.07486879208193,45.00679160929593,9,20/750x430@2x?access_token=pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6IjNLLVM5a0UifQ.fQiOn9nvARyZ80kya3yfjA';
	locals.mapboxStaticBaseUrl = 'https://api.mapbox.com/styles/v1/maccaluso/cj5wksxrn7jau2rmkmryvi5co/static';
	locals.mapboxStaticPosAndSize = '11.07486879208193,45.00679160929593,9,20/750x430@2x';
	locals.markerUrl = encodeURIComponent('http://www.itinerarioltrepomantovano.it/styles/img/townMarker.png');

	view.on('init', function (next) {
		var q;
		if(req.params.lang == 'it')
		{
			q = Route.model.findOne({
				key: locals.slug,
			}).populate('arguments')
			.deepPopulate('arguments.location arguments.categories');
		}

		if(req.params.lang == 'en')
		{
			q = Route.model.findOne({
				slugEN: locals.slug,
			}).populate('arguments')
			.deepPopulate('arguments.location arguments.categories');
		}

		if(req.params.id) {
			q = Route.model.findById(req.params.id)
				.populate('arguments')
				.deepPopulate('arguments.location arguments.categories');
		}

		q.exec(function (err, result) {
			if(result)
			{
				if(!result.nameEN)
					result.nameEN = result.name;

				locals.slugIT = result.key;
				locals.slugEN = result.slugEN;

				if (req.params.id) {
					result.slugIT = result.slugEN = result.id;
					locals.slugIT = locals.slugEN = 'id/' + result.id;
				}

				locals.route = result;
				locals.shareImg = result.coverImg.src.secure_url || req.protocol + '://' + req.get('host') + '/images/custom-route-share-cover.jpg';
				if(locals.lang == 'it') 
				{ 
					locals.shareTitle = result.name;
					locals.shareDescription = result.description.it;
					locals.pdfUrl = '/pdf/' + result.key + '.pdf';
				} 
				else 
				{ 
					locals.shareTitle = result.nameEN;
					locals.shareDescription = result.description.en;
					locals.pdfUrl = '/pdf/' + result.slugEN + '.pdf';
				}
				if (req.params.id)
					locals.pdfUrl = '/pdf/' + result.id + '.pdf';

				locals.shareUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

				var staticMarkers = [];
				for( var i in result.arguments )
				{
					if( result.arguments[i].location )
					{
						staticMarkers.push( 'url-' + locals.markerUrl + '(' + result.arguments[i].location.location.geo + ')' );
					}
				}

				locals.staticMarkers = staticMarkers.join(',');

				if( req.method == 'GET' )
				{
					console.log( 'hit get request' );
					requestify.get(req.protocol + '://' + req.get('host') + req.originalUrl + '/on').then(function (response) {
						var html = response.getBody();

						var base = path.join( __dirname, '../../public/' );
						base = 'file://' + base;
						html = html.replace( '/images', base + 'images' );
						html = html.replace( '/styles', base + 'styles' );
						html = html.replace( '/js', base + 'js' );

						var config = { 
							format: 'A4',
							base: base,
							border: {
								top: '15mm',
								right: '0mm',
								bottom: '30mm',
								left: '0mm'
							}
						};

						var filename = result.key;

						if(locals.lang == 'en')
							filename = result.slugEN;

						if( req.params.id )
							filename = req.params.id;

						pdf.create(html, config).toFile('./public/pdf/' + filename + '.pdf', function (err, res) {
							if (err) 
							{
								console.log(err);
								return;
							}

							console.log(res);
						});
					});
				}
			}

			next(err);
		});
	});

	view.on('post', function (next) {

		var filepath = path.join(__dirname, '../../public/pdf/' + req.body.routeFileName);
		if( req.params.id )
			filepath = path.join(__dirname, '../../public/pdf/' + req.params.id + '.pdf');

		var template = fs.readFileSync( path.join(__dirname, '../../templates/emails/route-pdf.hbs') );

		var hbs = handlebars.compile( template.toString() );

		var mailFrom, mailSubject, mailTitle, mailHead, 
			routeIDLabel, mailText1, mailText2, appDownloadText, 
			gpImg, asImg, mailTnxLabel, mailDisclaimer1, mailDisclaimer2, mailDisclaimer3;

		if(locals.lang == 'it')
		{
			mailFrom = 'Itinerari oltrepò mantovano <no-reply@itinerarioltrepomantovano.it>';
			mailSubject = 'Itinerari oltrepò mantovano - Il tuo itinerario';
			mailTitle = 'Itinerari Oltrepo Mantovano';
			mailHead = 'Inserisci l\'ID Itinerario seguente nell\'App gratuita Qui Oltrepò!';
			mailIDLabel = 'ID itinerario';
			mailText1 = 'Potrai così portare con te l\'itinerario';
			mailText2 = 'che hai salvato nel website Itinerari Oltrepò Mantovano, senza nessuna iscrizione nè ulteriori operazioni.'
			appDownloadText = 'Puoi scaricare l\'App gratuita Qui Oltrepò';
			mailTnxLabel = 'Grazie per aver usato Itinerari Oltrepò Mantovano!';
			mailDisclaimer1 = 'Hai richiesto questo link mediante la funzione';
			mailDisclaimer2 = 'Invia l\'Itinerario al tuo telefono';
			mailDisclaimer3 = 'di Itinerari Oltrepò Mantovano';
			gpImg = req.protocol + '://' + req.get('host') + '/images/google-play-badge-it.png';
			asImg = req.protocol + '://' + req.get('host') + '/images/app-store-badge-it.png'
		}
		else
		{
			mailFrom = 'Oltrepò Mantovano Itineraries <no-reply@itinerarioltrepomantovano.it>';
			mailSubject = 'Oltrepò Mantovano Itineraries - Your route';
			mailTitle = 'Oltrepò Mantovano Itineraries';
			mailHead = 'Enter the Route ID in the free App Qui Oltrepò!';
			mailIDLabel = 'Route ID';
			mailText1 = 'You\'ll be able to receive the';
			mailText2 = 'route\'s information directly from your smartphone on site!'
			appDownloadText = 'Download the free App Qui Oltrepò from here';
			mailTnxLabel = 'Thank you for using the website Oltrepò Mantovano Itineraries!';
			mailDisclaimer1 = 'You have requested this link using the';
			mailDisclaimer2 = 'Send Route to Your Phone';
			mailDisclaimer3 = 'function in Oltrepò Mantovano Itineraries';
			gpImg = req.protocol + '://' + req.get('host') + '/images/google-play-badge-en.png';
			asImg = req.protocol + '://' + req.get('host') + '/images/app-store-badge-en.png'
		}

		var result = hbs({
			routeMailTitle: mailTitle,
			routeCode: req.body.routeCode,
			routeName: req.body.routeName,
			routeMailHead: mailHead,
			routeMailIDLabel: mailIDLabel,
			routeMailText1: mailText1,
			routeMailText2: mailText2,
			routeMailAppDownloadText: appDownloadText,
			googlePlayImg: gpImg,
			appStoreImg: asImg,
			routeMailTnxLabel: mailTnxLabel,
			routeMailDisclaimer1: mailDisclaimer1,
			routeMailDisclaimer2: mailDisclaimer2,
			routeMailDisclaimer3: mailDisclaimer3
		});

		var data = {
			from: mailFrom,
			to: req.body.routeDestMail,
			subject: mailSubject,
			html: result,
			attachment: filepath
		};

		mailgun.messages().send(data, function (error, body) {
			if(error)
				res.send( error );
			else
				res.send( body );
		});
	});

	// Render the view
	if(req.params.nowebgl == 'on')
		view.render('routePDF', { layout: 'pdf' });
	else
		view.render('route');
};
