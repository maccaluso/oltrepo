var keystone = require('keystone');
var Argument = keystone.list('Argument');

exports = module.exports = function (req, res) {

	var q = Argument.model.findOne()
				.where({ '_id': req.params.id })
				.populate('categories');

	q.exec(function (err, result) {
		res.setHeader('Content-Type', 'application/json');
		res.send( JSON.stringify( result, null, 2 ) );
	});
};
