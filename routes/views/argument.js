var keystone = require('keystone');
var Argument = keystone.list('Argument');

var pdf = require('html-pdf');
var requestify = require('requestify');
var path = require('path');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'argomento';
	locals.slug = req.params.slug;
	locals.lang = req.params.lang;
	locals.basePathIT = 'didattica/argomento';
	locals.basePathEN = 'didactics/topic';

	view.on('init', function (next) {

		var q;
		if(req.params.lang == 'it')
		{
			q = Argument.model.findOne({
				key: locals.slug,
			}).populate('location routingStartDefault links towns deepenings categories related')
			.deepPopulate('related.towns');
		}

		if(req.params.lang == 'en')
		{
			q = Argument.model.findOne({
				slugEN: locals.slug,
			}).populate('location routingStartDefault links towns deepenings categories related')
			.deepPopulate('related.towns');
		}

		q.exec(function (err, result) {
			// console.log(result.related[0].towns);
			locals.slugIT = result.key;
			locals.slugEN = result.slugEN;
			locals.argument = result;

			locals.hasDeepenings = false;
			if(result.deepenings.length > 0)
				locals.hasDeepenings = true;

			locals.hasRelated = false;
			if(result.related.length > 0)
				locals.hasRelated = true;

			var blocks = [];
			var blocks2 = [];
			var orderedBlocks = [];
			var unorderedBlocks = [];
			for(var i in result)
			{
				if(i.indexOf('block') > -1) {

					if(result[i].order)
					{
						orderedBlocks[ result[i].order ] = result[i];
					}
					else
					{
						unorderedBlocks.unshift( result[i] );
					}
				}
			}

			blocks = orderedBlocks.concat( unorderedBlocks );
			blocks2 = blocks.filter(function(block){
				if(block.text.it || block.text.en || block.img.src.secure_url || block.gallery.pics.length > 0 || block.video.src.secure_url || block.externalVideo.id)
				{
					return true;
				}
			});
			
			locals.blocks = blocks2;

			locals.hasLinks = false;
			locals.links = [
				{
					label: result.externalLinks.label1,
					url: result.externalLinks.url1
				},
				{
					label: result.externalLinks.label2,
					url: result.externalLinks.url2
				},
				{
					label: result.externalLinks.label3,
					url: result.externalLinks.url3
				},
				{
					label: result.externalLinks.label4,
					url: result.externalLinks.url4
				},
				{
					label: result.externalLinks.label5,
					url: result.externalLinks.url5
				},
				{
					label: result.externalLinks.label6,
					url: result.externalLinks.url6
				},
				{
					label: result.externalLinks.label7,
					url: result.externalLinks.url7
				},
				{
					label: result.externalLinks.label8,
					url: result.externalLinks.url8
				},
				{
					label: result.externalLinks.label9,
					url: result.externalLinks.url9
				},
				{
					label: result.externalLinks.label10,
					url: result.externalLinks.url10
				}
			];

			var numSlotsFilled = 0;
			for(var z = 0; z < locals.links.length; z++)
			{
				if( locals.links[z].url != '' && locals.links[z].url != undefined )
					numSlotsFilled++;
			}
			if(numSlotsFilled > 0)
				locals.hasLinks = true;

			locals.hasContributions = false;
			locals.contributions = [
				{
					label: result.contributions.label1,
					url: result.contributions.url1,
					caption: result.contributions.caption1
				},
				{
					label: result.contributions.label2,
					url: result.contributions.url2,
					caption: result.contributions.caption2
				},
				{
					label: result.contributions.label3,
					url: result.contributions.url3,
					caption: result.contributions.caption3
				},
				{
					label: result.contributions.label4,
					url: result.contributions.url4,
					caption: result.contributions.caption4
				},
				{
					label: result.contributions.label5,
					url: result.contributions.url5,
					caption: result.contributions.caption5
				},
				{
					label: result.contributions.label6,
					url: result.contributions.url6,
					caption: result.contributions.caption6
				},
				{
					label: result.contributions.label7,
					url: result.contributions.url7,
					caption: result.contributions.caption7
				},
				{
					label: result.contributions.label8,
					url: result.contributions.url8,
					caption: result.contributions.caption8
				},
				{
					label: result.contributions.label9,
					url: result.contributions.url9,
					caption: result.contributions.caption9
				},
				{
					label: result.contributions.label10,
					url: result.contributions.url10,
					caption: result.contributions.caption10
				},
				{
					label: result.contributions.label11,
					url: result.contributions.url11,
					caption: result.contributions.caption11
				},
				{
					label: result.contributions.label12,
					url: result.contributions.url12,
					caption: result.contributions.caption12
				},
				{
					label: result.contributions.label13,
					url: result.contributions.url13,
					caption: result.contributions.caption13
				},
				{
					label: result.contributions.label14,
					url: result.contributions.url14,
					caption: result.contributions.caption14
				},
				{
					label: result.contributions.label15,
					url: result.contributions.url15,
					caption: result.contributions.caption15
				},
				{
					label: result.contributions.label16,
					url: result.contributions.url16,
					caption: result.contributions.caption16
				},
				{
					label: result.contributions.label17,
					url: result.contributions.url17,
					caption: result.contributions.caption17
				},
				{
					label: result.contributions.label18,
					url: result.contributions.url18,
					caption: result.contributions.caption18
				},
				{
					label: result.contributions.label19,
					url: result.contributions.url19,
					caption: result.contributions.caption19
				},
				{
					label: result.contributions.label20,
					url: result.contributions.url20,
					caption: result.contributions.caption20
				}
			];

			var numSlotsFilled2 = 0;
			for(var zz = 0; zz < locals.contributions.length; zz++)
			{
				if( locals.contributions[zz].url != '' && locals.contributions[zz].url != undefined )
					numSlotsFilled2++;
			}
			if(numSlotsFilled2 > 0)
				locals.hasContributions = true;

			if( req.method == 'GET' )
			{
				console.log( 'hit get request' );
				requestify.get(req.protocol + '://' + req.get('host') + req.originalUrl + '/on').then(function (response) {
					console.log(response);
					var html = response.getBody();

					var base = path.join( __dirname, '../../public/' );
					base = 'file://' + base;
					html = html.replace( '/images', base + 'images' );
					html = html.replace( '/styles', base + 'styles' );
					html = html.replace( '/js', base + 'js' );

					var config = { 
						format: 'A4',
						base: base,
						border: {
							top: '15mm',
							right: '0mm',
							bottom: '30mm',
							left: '0mm'
						}
					};

					var filename = result.key;

					if(locals.lang == 'en')
						filename = result.slugEN;

					pdf.create(html, config).toFile('./public/pdf/' + filename + '.pdf', function (err, res) {
						if (err) 
						{
							console.log(err);
							return;
						}

						console.log(res);
					});
				});
			}

			next(err);
		});

	});

	// Render the view
	// view.render('argument');
	if(req.params.nowebgl == 'on')
	{
		view.render('argumentPDF', { layout: 'pdf' });
	}
	else
	{
		view.render('argument');
	}

};
