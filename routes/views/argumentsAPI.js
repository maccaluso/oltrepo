var keystone = require('keystone');
var Argument = keystone.list('Argument');

exports = module.exports = function (req, res) {

	var q = Argument.model.find()
		.populate('location routingStartDefault categories related towns deepenings links');

	q.exec(function (err, result) {
		res.setHeader('Content-Type', 'application/json');
		res.send( JSON.stringify( result, null, 2 ) );
	});
};
