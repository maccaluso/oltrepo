var keystone = require('keystone');
var Argument = keystone.list('Argument');
var Town = keystone.list('Town');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.lang = req.params.lang;

	locals.basePathIT = 'didattica';
	locals.basePathEN = 'didactics';

	view.on('init', function (next) {
		var q = Argument.model.find().populate('location routingStartDefault categories related towns deepenings links');
		q.exec(function (err, result) {
			for(var i in result) {

				result[i].hasContribs = false;
				result[i].contribs = [
					{
						label: result[i].contributions.label1,
						url: result[i].contributions.url1,
						caption: result[i].contributions.caption1
					},
					{
						label: result[i].contributions.label2,
						url: result[i].contributions.url2,
						caption: result[i].contributions.caption2
					},
					{
						label: result[i].contributions.label3,
						url: result[i].contributions.url3,
						caption: result[i].contributions.caption3
					},
					{
						label: result[i].contributions.label4,
						url: result[i].contributions.url4,
						caption: result[i].contributions.caption4
					},
					{
						label: result[i].contributions.label5,
						url: result[i].contributions.url5,
						caption: result[i].contributions.caption5
					},
					{
						label: result[i].contributions.label6,
						url: result[i].contributions.url6,
						caption: result[i].contributions.caption6
					},
					{
						label: result[i].contributions.label7,
						url: result[i].contributions.url7,
						caption: result[i].contributions.caption7
					},
					{
						label: result[i].contributions.label8,
						url: result[i].contributions.url8,
						caption: result[i].contributions.caption8
					},
					{
						label: result[i].contributions.label9,
						url: result[i].contributions.url9,
						caption: result[i].contributions.caption9
					},
					{
						label: result[i].contributions.label10,
						url: result[i].contributions.url10,
						caption: result[i].contributions.caption10
					},
					{
						label: result[i].contributions.label11,
						url: result[i].contributions.url11,
						caption: result[i].contributions.caption11
					},
					{
						label: result[i].contributions.label12,
						url: result[i].contributions.url12,
						caption: result[i].contributions.caption12
					},
					{
						label: result[i].contributions.label13,
						url: result[i].contributions.url13,
						caption: result[i].contributions.caption13
					},
					{
						label: result[i].contributions.label14,
						url: result[i].contributions.url14,
						caption: result[i].contributions.caption14
					},
					{
						label: result[i].contributions.label15,
						url: result[i].contributions.url15,
						caption: result[i].contributions.caption15
					},
					{
						label: result[i].contributions.label16,
						url: result[i].contributions.url16,
						caption: result[i].contributions.caption16
					},
					{
						label: result[i].contributions.label17,
						url: result[i].contributions.url17,
						caption: result[i].contributions.caption17
					},
					{
						label: result[i].contributions.label18,
						url: result[i].contributions.url18,
						caption: result[i].contributions.caption18
					},
					{
						label: result[i].contributions.label19,
						url: result[i].contributions.url19,
						caption: result[i].contributions.caption19
					},
					{
						label: result[i].contributions.label20,
						url: result[i].contributions.url20,
						caption: result[i].contributions.caption20
					}
				];

				var numSlotsFilled = 0;
				for(var z = 0; z < result[i].contribs.length; z++)
				{
					if( result[i].contribs[z].url != '' && result[i].contribs[z].url != undefined )
						numSlotsFilled++;
				}
				if(numSlotsFilled > 0)
					result[i].hasContribs = true;
			}

			locals.arguments = result;

			var townsQuery = Town.model.find();
			townsQuery.exec(function (err, result) {
				locals.towns = result;

				next(err);
			});
		});
	});

	// Render the view
	view.render('didactics');
};