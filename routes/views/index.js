var keystone = require('keystone');
var Argument = keystone.list('Argument');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';
	locals.lang = req.params.lang;

	view.query(
		'arguments', 
		keystone.list('Argument')
			.model.find()
			.where('isFeatured', true)
			.sort('-featuredOrder')
			.populate('categories location towns')
	);

	view.query(
		'featuredVideos', 
		keystone.list('FeaturedVideo')
			.model.find()
			.where('showInHome', true)
			.sort('sortOrder')
			.populate('categories towns')
	);

	// Render the view
	view.render('index');
};
