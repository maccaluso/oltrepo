var translations = {
	SEO: {
		siteTitle: {
			it: 'Itinerari di visita dell\'Oltrepò Mantovano',
			en: 'Sightseeing itineraries in Oltrepò Mantovano'
		},
		siteDescription: {
			it: 'Scopri gli itinerari enogastronomici più sorprendenti e le tappe storico culturali più affascinanti, e pianifica il tuo percorso di visita in questo territorio!',
			en: 'Check out the most unique wine & food tasting routes, and the most breathtaking sightseeing destinations, and organize your own visit of this surprising corner of the Po territory.'
		}
	},

	siteTitle: {
		it: 'Itinerari Oltrepò Mantovano',
		en: 'Oltrepò Mantovano Itineraries'
	},
	yourRoute: {
		it: 'Il tuo itinerario',
		en: 'Your route'
	},
	siteHeading: {
		it: 'Oltrepò mantovano',
		en: 'Oltrepò mantovano'
	},
	siteHeadline: {
		it: 'Scopri gli itinerari enogastronomici più sorprendenti e le tappe storico culturali più affascinanti, e pianifica il tuo percorso di visita in questo territorio!',
		en: 'Check out the most unique wine & food tasting routes, and the most breathtaking sightseeing destinations, and organize your own visit of this surprising corner of the Po territory.'
	},
	removeFilters: {
		it: 'Mostra tutti',
		en: 'Show all'
	},
	hideTopics: {
		it: 'Nascondi argomenti',
		en: 'Hide topics'
	},
	featuredArgumentsHeading: {
		it: 'Esplora le eccellenze del territorio',
		en: 'Explore the excellence of the territory'
	},
	videoGalleryHeading: {
		it: 'Oppure comincia da qui',
		en: 'Or start here'
	},

	readMore: {
		it: 'Continua a leggere...',
		en: 'Read more...'
	},
	addToRoute: {
		it: 'Aggiungi all\'itinerario',
		en: 'Add to route'
	},

	whereDoYouStart: {
		it: 'Da dove parti?',
		en: 'Departure?'
	},
	typeStartingPoint: {
		it: 'Digita il tuo punto di partenza',
		en: 'Type your starting point'
	},
	fromNearestToll: {
		it: 'Dal casello più vicino',
		en: 'From the nearest toll'
	},
	fromNearestStation: {
		it: 'Dalla stazione più vicina',
		en: 'From the nearest station'
	},
	editRouteName: {
		it: 'Modifica il nome del tuo itinerario',
		en: 'Edit the name of your route'
	},
	saveRouteName: {
		it: 'Salva il nuovo nome',
		en: 'Save the new name'
	},
	deleteRoute: {
		it: 'Elimina itinerario',
		en: 'Delete route'
	},
	noSavedRoute: {
		it: 'Nessun itinerario salvato',
		en: 'No saved routes'
	},
	createNewRoute: {
		it: 'Crea nuovo itinerario',
		en: 'Create new route'
	},
	downloadRouteAsPdf: {
		it: 'Scarica itinerario come PDF',
		en: 'Download route as PDF'
	},
	openInGMaps: {
		it: 'Apri in Google Maps',
		en: 'Open in Google Maps'
	},
	personalRouteStart: {
		it: 'Partenza da: ',
		en: 'Start from: '
	},
	addStepToRoute: {
		it: 'Aggiungi una destinazione',
		en: 'Add a destination'
	},
	bringRouteWithYou: {
		it: 'Porta con te questo itinerario',
		en: 'Take this route with you'
	},
	saveRouteInApp: {
		it: 'Salva l\'itinerario nell\'app',
		en: 'Save the route in app'
	},

	foundInTown: {
		it: 'In questo comune trovi',
		en: 'In this town you can find'
	},
	noArgumentsAvailable: {
		it: 'Nessun argomento disponibile',
		en: 'No topics available'
	},
	noRelatedArguments: {
		it: 'Non ci sono argomenti correlati',
		en: 'No related topics'
	},
	noRelatedTowns: {
		it: 'Non ci sono comuni correlati',
		en: 'No related towns'
	},

	exploreTopics: {
		it: 'Esplora gli altri argomenti',
		en: 'Explore other topics'
	},
	exploreTowns: {
		it: 'Esplora gli altri comuni',
		en: 'Explore other towns'
	},
	toKnowMore: {
		it: 'Per approfondire',
		en: 'To know more'
	},
	relatedTowns: {
		it: 'Comuni correlati',
		en: 'Related Towns'
	},
	previous: {
		it: 'Precedente',
		en: 'Previous'
	},
	next: {
		it: 'Successivo',
		en: 'Next'
	},

	searchSomethingSpecial: {
		it: 'Cerchi qualcosa di particolare?',
		en: 'Looking for something special?'
	},
	startTyping: {
		it: 'Inizia a digitare',
		en: 'Start typing'
	},
	flowArguments: {
		it: 'Scorri gli argomenti per localizzarli sulla mappa',
		en: 'Browse topics to locate them on the map'
	},
	addAllToRoute: {
		it: 'Aggiungi tutti',
		en: 'Add all'
	},
	filterByCategory: {
		it: 'Filtra per categoria',
		en: 'Filter by category'
	},
	filterByTown: {
		it: 'Filtra per comune',
		en: 'Filter by town'
	},
	removeFilter: {
		it: 'Azzera filtro',
		en: 'Remove filter'
	},
	allTopics: {
		it: 'Tutti gli argomenti',
		en: 'All topics'
	},

	exploreThroughWebsite: {
		it: 'Esplora attraverso il website',
		en: 'Explore through the website'
	},
	gotoHomepage: {
		it: 'Vai alla home page',
		en: 'Go to the home page'
	},

	routeCode: {
		it: 'Codice Itinerario',
		en: 'Route code'
	},
	routeAppHint: {
		it: 'Scarica la App "Qui Oltrepò" e inserisci questo codice per avere questo itinerario sempre con te.',
		en: 'Download the "Qui Oltrepò" App and enter this code to have this tour always with you.'
	},
	downloadRoutePDF: {
		it: 'Scarica itinerario in PDF',
		en: 'Download route in PDF'
	},
	sendRouteEmail: {
		it: 'Invia itinerario per email',
		en: 'Send route by email'
	},
	yourEmail: {
		it: 'La tua email',
		en: 'Your email'
	},
	yourEmailPlaceholder: {
		it: 'Invia queste informazioni alla tua email',
		en: 'Send this information to your email'
	},
	toReachRouteStartingPoint: {
		it: 'Per raggiungere la prima tappa',
		en: 'To reach the starting point'
	},

	featuredRoutesHeading: {
		it: "Itinerari suggeriti",
		en: "Featured routes"
	},
	showSteps: {
		it: "Scopri le destinazioni",
		en: "Show steps"
	},
	useThisRoute: {
		it: "Invia l'itinerario al tuo telefono",
		en: "Send this route to your phone"
	},
	share: {
		it: "Condividi",
		en: "Share"
	},

	composeRoute: {
		it: 'Componi il tuo itinerario',
		en: 'Build your route'
	},
	navigateMap: {
		it: 'Naviga la mappa e aggiungi le tue destinazioni oppure filtra i luoghi per categoria. Poi scarica l\'app <strong>Qui Oltrepò</strong> e porta con te il tuo itinerario.',
		en: 'Navigate the map and add your destinations or filter the places by category. Then download the <strong>Qui Oltrepò</strong> app and bring your itinerary with you.'
	},
	letInspire: {
		it: 'Non sai da dove cominciare? Lasciati ispirare dai nostri itinerari.',
		en: 'Don\'t know where to start? Let yourself be inspired by our itineraries.'
	},

	getRouteCode: {
		it: 'Ottieni il codice dell\'itinerario attivo',
		en: 'Get active route code'
	},
	routeNameLabel: {
		it: 'Nome itinerario',
		en: 'Route name'
	},
	routeIdLabel: {
		it: 'ID itinerario',
		en: 'Route ID'
	},
	downloadAppLabel: {
		it: 'Scarica l\'app',
		en: 'Download app'
	},
	downloadAppText: {
		it: 'Scarica l\'App, inserisci l\'ID e segui questo itinerario dal tuo smartphone!',
		en: 'Download the App, enter the ID and follow this route from your smartphone!'
	},
	orLabel: {
		it: 'Oppure',
		en: 'Or'
	},

	searchArchive: {
		it: 'Ricerca nell\'archivio',
		en: 'Search archive'
	},
	didacticsIntroHead: {
		it: 'Benvenuti nell\'area didattica di itinerari Oltrepò Mantovano',
		en: 'Welcome to the didactic area of Oltrepò Mantovano itineraries'
	},
	didacticsIntroBody: {
		it: 'Quest’area didattica nasce con l\'idea di valorizzare le caratteristiche della storia e del territorio locali anche come materiale a disposizione dell\'attività didattica locale tramite un archivio digitale facilmente consultabile, e con l\'obiettivo di costruire uno strumento che favorisca l\'uso di tale materiale tra insegnanti che lavorano o intendono lavorare sul territorio dell\'Oltrepò Mantovano.',
		en: 'This didactic area is born with the idea of enhancing the characteristics of local history and locality as a material available for local teaching activities through an easily archive digital archive, with the aim of building a tool that favors the use of this material among teachers who work or intend to work in the territory of Oltrepò Mantovano.'
	},
	backToDidactics: {
		it: 'Torna all\'area didattica',
		en: 'Back to didactics area'
	},
	argumentContributions: {
		it: 'Contributi per questo argomento',
		en: 'Contributions for this topic'
	},
	downloadPDF: {
		it: 'Scarica PDF',
		en: 'Download PDF'
	},
	contributionsLabel: {
		it: 'Contributi',
		en: 'Contributions'
	},

	viewRoutePage: {
		it: 'Visualizza itinerario',
		en: 'View route'
	}

}

module.exports = translations;