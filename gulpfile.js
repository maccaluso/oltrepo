var gulp = require('gulp');
var jshint = require('gulp-jshint');
var jshintReporter = require('jshint-stylish');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var shell = require('gulp-shell');
var bs = require('browser-sync').create();

var debug = require('gulp-debug');

var paths = {
    'src':['./models/**/*.js','./routes/**/*.js', 'keystone.js', 'package.json'],
    'style': {
        all: './public/styles/**/*.scss',
        output: './public/styles/'
    },
    'js': {
        src: './public/js/frontend/**/*.js',
        output: './public/js/'
    }
};

// gulp lint
gulp.task('lint', function(){
    gulp.src(paths.src)
        .pipe(jshint())
        .pipe(jshint.reporter(jshintReporter));
});

// gulp watcher for lint
gulp.task('watch:lint', function () {
    gulp.src(paths.src)
        .pipe(watch(paths.src))
        .pipe(jshint())
        .pipe(jshint.reporter(jshintReporter));
});

// compile & minify frontend javascript
gulp.task('frontend', function() {
    gulp.src(paths.js.src)
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(gulp.dest(paths.js.output))
        .pipe(rename('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.js.output))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(paths.js.output))
        .pipe(bs.stream());
});

// compile and minify sass
gulp.task('sass', function(){
    gulp.src(paths.style.all)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(paths.style.output))
        .pipe(bs.stream());
});

// watchers
gulp.task('watch:frontend', function () {
    gulp.watch(paths.js.src, ['frontend']);
});

gulp.task('watch:sass', function () {
    gulp.watch(paths.style.all, ['sass']);
});

gulp.task('browser-sync', function(){
    bs.init({
        proxy: 'http://localhost:3000',
        port: '4000'
    });
});

// run keystone
// gulp.task('runKeystone', shell.task('node keystone.js'));

gulp.task('watch', ['watch:frontend', 'watch:sass', 'watch:lint']);
// gulp.task('default', ['watch', 'runKeystone', 'browser-sync']);
gulp.task('default', ['watch', 'browser-sync']);